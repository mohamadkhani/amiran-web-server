import { IsArray, IsDefined, IsString, Length } from 'class-validator';
import Entity, { CUF } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

export class ItemSpec extends Entity {
	public _collectionName = 'ItemSpec';

	public _key: string;

	@IsDefined({ groups: CUF })
	@Length(2, 20, { groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	@Sortable()
	public title: string = null;

	@Length(2, 10)
	@IsString({ groups: CUF })
	@Filterable()
	public type: string = null;

	@IsDefined({ groups: CUF })
	@IsArray({ groups: CUF })
	@Filterable()
	public options: string[] = [];

	createdAt: number = null;

	createdBy: string = null;
	constructor() {
		super();
	}
}
