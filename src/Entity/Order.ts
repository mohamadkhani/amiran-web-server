import {
	IsArray,
	IsBoolean,
	IsDefined,
	IsEmpty,
	IsNumber,
	IsObject,
	IsOptional,
	IsString
} from 'class-validator';
import Entity, { F, CF, CUF, CU } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';
import { ItemType } from './ItemType';

export interface OrderItem {
	itemType: ItemType;
	packingOption: { title: string; capacity: number; _key: string };
	totalCount: number;
	totalPrice: number;
	discount: number;
}

export interface AbstractOrderItem {
	itemType: ItemType;
	packingOption: { title: string; capacity: number; _key: string };
	packageCount: number;
	totalCount: number;
	packages: string[];
	items: string[];
}

export type OrderStatus =
	| 'canceled'
	| 'suspended'
	| 'pendingAccountantVerify'
	| 'pendingAdminVerify'
	| 'accountantRejected'
	| 'adminRejected'
	| 'adminCanceled'
	| 'pendingWarehouseLoading'
	| 'loading'
	| 'loaded'
	| 'sent';
export class Order extends Entity {
	public _collectionName = 'Order';

	@IsEmpty()
	_key: string = null;

	@IsEmpty()
	_id: string = null;

	@IsDefined({ groups: CF })
	@IsString({ groups: CF })
	@Filterable()
	@Sortable()
	public customer: string = null;

	@IsOptional({ groups: CU })
	@IsString({ groups: CU })
	public description: string = null;

	@Filterable()
	@Sortable()
	public status: OrderStatus = null;

	@IsOptional({ groups: CUF })
	@IsNumber({}, { groups: CUF })
	@Filterable()
	@Sortable()
	public totalPrice: number = 0;

	@IsOptional({ groups: CUF })
	@IsBoolean({ groups: CUF })
	@Filterable()
	@Sortable()
	public urgent: boolean = false;

	@IsOptional({ groups: CUF })
	@IsBoolean({ groups: CUF })
	@Filterable()
	@Sortable()
	public isGuarantee: boolean = false;

	@Filterable()
	@Sortable()
	public adminRejectReason: string = null;

	@Filterable()
	@Sortable()
	public adminCancelReason: string = null;

	@Filterable()
	@Sortable()
	public accountantRejectReason: string = null;

	@Filterable()
	@Sortable()
	public ladingCode: string = null;

	@IsOptional({ groups: CUF })
	@IsObject()
	transportInfo: {
		company: string;
		driverFullname: string;
		driverMobile: string;
		ladingCode: string;
		transportType: string;
		vehiclePlate: string[];
	};

	@IsOptional({ groups: CU })
	@IsArray({ groups: CU })
	public items: OrderItem[] = [];

	@IsDefined({ groups: CUF })
	@IsArray({})
	public abstractItems: AbstractOrderItem[] = [];

	@Sortable()
	createdAt: number = null;

	createdBy: string = null;

	constructor() {
		super();
	}
}
