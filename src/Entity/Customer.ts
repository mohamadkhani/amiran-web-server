import {
	IsBoolean,
	IsDefined,
	IsNumber,
	IsNumberString,
	IsOptional,
	IsString
} from 'class-validator';
import Entity, { CF, CUF, F, U } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';
import { Updatable } from '../Model/CustomValidators/General/Updatable';

export class Customer extends Entity {
	_collectionName = 'Customer';

	@IsDefined({ groups: F })
	@IsString({ groups: F })
	@Filterable()
	@Sortable()
	_key: string = null;

	@IsDefined({ groups: CF })
	@IsString({ groups: CUF })
	@Sortable()
	@Updatable()
	fullname: string = null;

	@IsOptional({ groups: U })
	@IsString({ groups: CUF })
	@Updatable()
	company: string = null;

	@IsOptional({ groups: CUF })
	@IsNumberString({}, { groups: CUF })
	@Updatable()
	mobile: string = null;

	@IsOptional({ groups: CUF })
	@IsNumberString({}, { groups: CUF })
	@Updatable()
	phone: string = null;

	@IsOptional({ groups: CUF })
	@IsNumber({}, { groups: CUF })
	@Updatable()
	postCode: string = null;

	@IsOptional({ groups: CUF })
	@IsString({ groups: CUF })
	@Updatable()
	address: string = null;

	@IsOptional({ groups: CUF })
	@IsNumberString({}, { groups: CUF })
	@Sortable()
	@Updatable()
	province: string = null;

	@IsOptional({ groups: CUF })
	@IsNumber({}, { groups: CUF })
	@Sortable()
	@Updatable()
	city: string = null;

	@IsOptional({ groups: CUF })
	@IsBoolean({ groups: CUF })
	@Filterable()
	@Sortable()
	@Updatable()
	active: boolean = true;

	@Sortable()
	createdAt: number = null;

	createdBy: string = null;
}
