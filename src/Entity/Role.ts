import { IsDefined, IsEmpty, IsString, Length } from 'class-validator';
import Entity, { CUF } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

export type RoleIds =
	| 'Role/scanman'
	| 'Role/worker'
	| 'Role/productManager'
	| 'Role/salesperson'
	| 'Role/salesExpert'
	| 'Role/salesManager'
	| 'Role/accountant'
	| 'Role/accountingExpert'
	| 'Role/warehouseKeeper'
	| 'Role/manager'
	| 'Role/admin'
	| 'Role/supervisor';

export type RolesNames =
	| 'Worker'
	| 'Admin'
	| 'ProductionManager'
	| 'WarehouseKeeper';
export type RolesKeys =
	| 'worker'
	| 'admin'
	| 'productionManager'
	| 'warehouseKeeper';

export type Action = 'ProductionLine/create';

export type ZoneName =
	| 'Company'
	| 'ProductionLine'
	| 'Warehouse'
	| 'Shift'
	| 'Sold'
	| 'ShiftInterval'
	| 'Guarantee'
	| 'Trash'
	| 'ProductionLineHistory';

export type MenuItem =
	| 'Profile'
	| 'ProductionLine'
	| 'Packing'
	| 'Package List';

export type AccessList = Record<ZoneName, Access>;

export type Access = {
	actions: Action[];
	menuItems: MenuItem[];
	subZones: ZoneName[];
	ZoneId: string;
};

export class Role extends Entity {
	public _collectionName = 'Role';

	@IsEmpty()
	public _key: RolesKeys = null;
	@IsDefined({ groups: CUF })
	@Length(2, 20, { groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	@Sortable()
	public title: RolesNames = null;

	public accessList: AccessList;

	createdAt: number = null;

	createdBy: string = null;
}
