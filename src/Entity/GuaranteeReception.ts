import {
	IsArray,
	IsDefined,
	IsEAN,
	IsEmpty,
	IsNumber,
	IsOptional,
	IsString,
	Length
} from 'class-validator';
import Entity, { C, CFD, CU, F } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

export class GuaranteeReception extends Entity {
	public _collectionName = 'GuaranteeReception';

	@IsEmpty()
	@Filterable()
	@Sortable()
	_key: string;

	@IsDefined({ groups: C })
	@IsArray({ groups: C })
	items: { _key: string }[] = [];

	@IsOptional({ groups: CU })
	@IsString({ groups: CU })
	description: string = null;

	@IsDefined({ groups: C })
	@IsNumber({}, { groups: CU })
	@Filterable()
	@Sortable()
	sendDate: number = null;

	@IsDefined({ groups: C })
	@IsString({ groups: C })
	@Filterable()
	@Sortable()
	sender: string = null;

	@IsDefined({ groups: C })
	@IsNumber({}, { groups: C })
	@Filterable()
	@Sortable()
	packageCount: number = null;

	@IsOptional({ groups: CU })
	@IsNumber({}, { groups: CU })
	@Filterable()
	@Sortable()
	cargoPrice: number = null;

	@IsOptional({ groups: CU })
	@IsString({ groups: CU })
	@Filterable()
	@Sortable()
	originCargo: string = null;

	@IsOptional({ groups: CU })
	@IsString({ groups: CU })
	@Filterable()
	@Sortable()
	destinationCargo: string = null;

	@IsOptional({ groups: CU })
	@IsNumber({}, { groups: CU })
	@Filterable()
	@Sortable()
	receptionDate: string = null;

	@IsOptional({ groups: CU })
	@IsNumber({}, { groups: CU })
	@Filterable()
	@Sortable()
	warehouseDeliverDate: number = null;

	@Sortable()
	createdAt: number = null;

	createdBy: string = null;

	constructor() {
		super();
	}
}
