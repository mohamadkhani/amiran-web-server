import { ArangojsError } from 'arangojs/lib/request.node';
import {
	IsArray,
	IsDefined,
	IsNumber,
	IsString,
	Length
} from 'class-validator';
import Entity, { CU, CUF, db } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';
import { Updatable } from '../Model/CustomValidators/General/Updatable';
import { FilterMDL, SortMDL } from '../Model/Entity/ReadMDL';
import { ItemSpec } from './ItemSpec';
import { aql } from 'arangojs';
import _ from 'lodash';
import moment from 'moment-jalaali';
import { ActionAccess } from '../Base/Controller';

export class ItemType extends Entity {
	public _collectionName = 'ItemType';

	_key: string = null;

	@IsDefined({ groups: CUF })
	@Length(2, 20, { groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	@Sortable()
	@Updatable()
	public title: string = null;

	@IsDefined({ groups: CUF })
	@IsNumber({}, { groups: CUF })
	@Filterable()
	@Sortable()
	@Updatable()
	public price: number = 0;

	@IsDefined({ groups: CUF })
	@IsArray({ groups: CUF })
	@Filterable()
	@Updatable()
	public values: {
		itemSpec: ItemSpec;
		value: { key: string; title: string };
	}[] = [];

	@IsDefined({ groups: CU })
	@IsString({ groups: CU })
	@Updatable()
	public itemRegex: string = null;

	@IsDefined({ groups: CU })
	@IsString({ groups: CU })
	@Updatable()
	public packageRegex: string = null;

	@IsDefined({ groups: CU })
	@Updatable()
	public packingOptions: {
		title: string;
		size: string;
		capacity: number;
		itemInRow: number;
	}[] = [];

	createdAt: number = null;

	createdBy: string = null;
	constructor() {
		super();
	}

	@ActionAccess('ItemType/read', 'Company')
	async read(
		data: {
			pageSize?: number;
			page?: number;
			filter: FilterMDL<ItemType, string | number>[];
			sort?: SortMDL[];
		} = {
			pageSize: 10,
			page: 1,
			filter: [],
			sort: []
		}
	) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}

		if (typeof data === 'string') {
			let result;
			try {
				const cursor = await db.query(
					`RETURN DOCUMENT("` +
						this._collectionName +
						`/` +
						data +
						`")`
				);
				result = await cursor.reduce((acm, item) => item, undefined);
			} catch (e) {
				throw {
					message: (e as ArangojsError).message,
					name: (e as ArangojsError).name,
					stack: (e as ArangojsError).stack
				};
			}
			if (result === undefined || result === null) throw 'Not Found';
			else return result;
		} else {
			// LIST REQUESTED
			data = {
				...data,
				pageSize: data.pageSize || 10,
				page: data.page ? data.page - 1 : 0
			};

			try {
				const collection = db.collection(this._collectionName);
				const query1 = `
				FOR u in ${collection.name}
				${!!data?.sort?.length ? 'SORT' : ''}
				${
					!!data?.sort?.length
						? data?.sort?.reduce(
								(acm, sortItem) =>
									(acm ? acm + ', ' : '') +
									('u.' +
										sortItem.key +
										'' +
										(sortItem.desc ? ' DESC ' : '')),
								''
						  )
						: ''
				}
				FILTER ${
					data?.filter
						?.map(
							({ op, value, key }) =>
								'u.' + key + ' ' + op + ' "' + value + '"'
						)
						.reduce(
							(strFilters, strFilter) =>
								strFilters +
								(strFilters ? ' AND ' : '') +
								strFilter,
							''
						) || ' true '
				}
					LIMIT ${data.page * data.pageSize},${data.pageSize}
					let values = (FOR v in u.values
						return MERGE(v, {itemSpec: DOCUMENT('ItemSpec',v.itemSpec)}))
					RETURN MERGE(u, {values})
			`;

				const query2 = `
				FOR u in ${collection.name}
				${!!data?.sort?.length ? 'SORT' : ''}
				${
					!!data?.sort?.length
						? data?.sort?.reduce(
								(acm, sortItem) =>
									(acm ? acm + ', ' : '') +
									('u.' +
										sortItem.key +
										'' +
										(sortItem.desc ? ' DESC ' : '')),
								''
						  )
						: ''
				}
				FILTER ${
					data?.filter
						?.map(
							({ op, value, key }) =>
								'u.' + key + ' ' + op + ' "' + value + '"'
						)
						.reduce(
							(strFilters, strFilter) =>
								strFilters +
								(strFilters ? ' AND ' : '') +
								strFilter,
							''
						) || ' true '
				}
					LIMIT ${data.page * data.pageSize},${data.pageSize}
					RETURN u
			`;

				let cursor = await db.query(query1);
				const items = await cursor.all();
				cursor = await db.query(query2);
				const totalCount = await cursor.all();
				return {
					items,
					totalCount: totalCount.length
				};
			} catch (e) {
				return e;
			}
		}
	}

	@ActionAccess('ItemType/create', 'Company')
	async create(userInfo: { userKey: string }) {
		try {
			if (!this._collectionName) {
				throw 'Collection name is not specified.';
			}
			if (Object.keys(this).includes('createdBy'))
				this['createdBy'] = userInfo.userKey;
			if (Object.keys(this).includes('createdAt')) {
				this['createdAt'] = moment().valueOf();
			}
			let data: ItemType = {} as ItemType;
			Object.keys(this)
				.filter((item) => !item.startsWith('_'))
				.forEach((item) => (data[item] = this[item]));

			const qData = {
				...data,
				values: data.values.map((v) => ({
					...v,
					itemSpec: v.itemSpec._key
				}))
			};

			const collection = db.collection(this._collectionName);
			const inserted = await db.query(aql`
              INSERT ${qData}
              IN ${collection}
              RETURN NEW
            `);
			return (await inserted.map((doc) => doc)).reduce(
				(acm, item) => item,
				''
			);
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	@ActionAccess('ItemType/update', 'Company')
	async update({ key, data }: { key: string; data: ItemType }) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}
		try {
			const collection = db.collection(this._collectionName);
			const qData = {
				...data,
				values: data.values.map((v) => ({
					...v,
					itemSpec: v.itemSpec._key
				}))
			};

			const inserted = await db.query(aql`
            	UPDATE ${key} WITH ${qData}  
				IN ${collection}
				RETURN { before: OLD, after: NEW }
            `);
			return (await inserted.map((doc) => doc)).reduce(
				(acm, item) => item,
				''
			);
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}
}
