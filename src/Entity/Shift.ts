import {
	IsDefined,
	IsOptional,
	IsString,
	Length,
	IsArray,
	IsObject,
	IsDateString,
	IsISO8601,
	IsNumber,
	IsEmpty
} from 'class-validator';
import Entity, { C, CF, CU, CUF, F } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';
import { Updatable } from '../Model/CustomValidators/General/Updatable';

export type ShiftStatus = 'producing' | 'paused' | 'finished' | 'created';
export const shiftStatuses: ShiftStatus[] = [
	'producing',
	'paused',
	'finished',
	'created'
];

//TODO: valiations should become more accurate
export class Shift extends Entity {
	public _collectionName = 'Shift';

	public _key = null;

	@IsEmpty({ groups: C })
	@IsDefined({ groups: F })
	@IsString({ groups: F })
	@Updatable()
	@Filterable()
	public status: ShiftStatus = null;

	@IsDefined({ groups: C })
	@IsObject({ groups: C })
	public itemType: Record<string, any> = null;

	@IsDefined({ groups: C })
	@IsObject({ groups: C })
	// @Updatable()
	public packing: Record<string, any> = null;

	@IsDefined({ groups: CF })
	@IsNumber({}, { groups: CF })
	@Filterable()
	public guaranteeDate: number = null;

	@IsDefined({ groups: C })
	@IsNumber({}, { groups: C })
	public productionEstimation: number = null;

	@IsArray({ groups: C })
	public intervals: {
		started: number;
		zoneAccess: Record<string, string>;
	}[] = [];

	@IsOptional({ groups: CU })
	@IsString({ groups: CU })
	public report: string;

	@IsOptional({ groups: CU })
	public verifiedByQC: boolean;

	public zoneAccess: Record<string, string> = {};

	@Sortable()
	@Filterable()
	createdAt: number = null;

	createdBy: string = null;

	constructor() {
		super();
	}
}
