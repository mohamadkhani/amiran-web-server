import { IsDefined, IsOptional, IsString, Length } from 'class-validator';
import Entity, { CFD, F } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

type ItemStatusLogType = 'packed' | 'unpacked' | 'sold';
export type ItemStatusLog = {
	name: ItemStatusLogType;
	createdBy: string;
	createdAt: number;
};
export class Item extends Entity {
	public _collectionName = 'Item';

	@IsOptional({ groups: F })
	@Length(15, 15, { groups: CFD })
	@IsString({ groups: CFD })
	@Filterable()
	@Sortable()
	_key: string;

	@IsOptional({ groups: F })
	@Length(13, 13, { groups: F })
	@IsString({ groups: F })
	@Filterable()
	@Sortable()
	packageKey: string = null;

	@IsDefined({ groups: CFD })
	@IsString({ groups: CFD })
	@Filterable()
	@Sortable()
	shiftCode: string = null;

	createdAt: number = null;

	createdBy: string = null;
	// public async read(
	// 	data:
	// 		| string
	// 		| {
	// 				page: number;
	// 				index: number;
	// 				filters: {
	// 					op: '==' | '!=' | '<' | '<=' | '>' | '>=' | 'like';
	// 					value: string;
	// 					key: string;
	// 				}[];
	// 				order: { field: string; DESC: boolean };
	// 		  }
	// ): Promise<any> {
	// 	if (!this._collectionName) {
	// 		throw 'Collection name is not specified.';
	// 	}
	// 	if (typeof data === 'string') return super.read(data);
	//
	// 	// LIST REQUESTED
	// 	data = { ...data, limit: data.limit || 10, index: data.index || 0 };
	//
	// 	try {
	// 		const collection = this.db.collection(this._collectionName);
	// 		const cursor = await this.db.query(`
	//                 FOR u in ${collection.name}
	//                 FILTER ${
	// 					data?.filters
	// 						?.map(
	// 							({ op, value, key }) =>
	// 								'u.' + key + ' ' + op + ' "' + value + '"'
	// 						)
	// 						.reduce(
	// 							(strFilters, strFilter) =>
	// 								strFilters +
	// 								(strFilters ? ' AND ' : '') +
	// 								strFilter,
	// 							''
	// 						) || ' true '
	// 				}
	//                     LIMIT ${data.index},${data.limit}
	//                     RETURN MERGE(u, {box: DOCUMENT("Box", u.box)})
	//             `);
	// 		const items = await cursor.all();
	// 		return {
	// 			items,
	// 			totalCount: (
	// 				await this.db.collection(this._collectionName).count()
	// 			).count
	// 		};
	// 	} catch (e) {
	// 		return e;
	// 	}
	// }

	constructor() {
		super();
	}
}
