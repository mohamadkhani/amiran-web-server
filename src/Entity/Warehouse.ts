import Entity from '../Base/Entity';

export class Warehouse extends Entity {
	_collectionName = 'Warehouse';

	_key: string = null;

	details: string = null;

	createdAt: number = null;

	createdBy: string = null;

	workers: string[] = [];
}
