import {
	IsArray,
	IsDate,
	IsEmpty,
	IsObject,
	IsOptional,
	IsString,
	Length
} from 'class-validator';
import Entity, { F, CF, CUF } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

type PackageLogType =
	| 'filling'
	| 'filled'
	| 'labeled'
	| 'stored'
	| 'stored-opened'
	| 'ordered'
	| 'loaded'
	| 'sold';
export type PackageLog = {
	name: PackageLogType;
	createdBy: string;
	createdAt: number;
};
export class Package extends Entity {
	public _collectionName = 'Package';

	@IsOptional({ groups: F })
	@Length(13, 13, { groups: F })
	@IsString({ groups: F })
	@Filterable()
	@Sortable()
	_key: string = null;

	@IsEmpty()
	_id: string = null;

	@IsOptional({ groups: CF })
	@Length(2, 20, { groups: CF })
	@IsString({ groups: CF })
	@Filterable()
	@Sortable()
	public status: string = null;

	@IsOptional({ groups: CUF })
	@IsArray({ groups: CUF })
	@Filterable()
	@Sortable()
	public shiftCode: string[] = []; //this can be retreived from database

	@IsEmpty()
	public items: string[] = [];

	@Sortable()
	createdAt: number = null;

	@IsObject()
	packingOption: { capacity: number; title: string; _key: string };

	createdBy: string = null;

	constructor() {
		super();
	}
}
