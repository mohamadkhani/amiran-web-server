import Entity, { C, CU, CUF, db, F } from '../Base/Entity';
import {
	IsArray,
	IsDateString,
	IsDefined,
	IsOptional,
	IsString,
	Length
} from 'class-validator';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Updatable } from '../Model/CustomValidators/General/Updatable';
import { Username } from '../Model/CustomValidators/Username';
import { MaritalStatus } from '../Model/CustomValidators/MaritalStatus';
import { Gender } from '../Model/CustomValidators/Gender';
import { IranPhoneNumber } from '../Model/CustomValidators/IranPhoneNumber';
import { IranMobileNumber } from '../Model/CustomValidators/IranMobileNumber';
import { NationalCode } from '../Model/CustomValidators/NationalCode';
import { Password } from '../Model/CustomValidators/Password';
import { Access, Role, RoleIds, ZoneName } from './Role';
import { Sortable } from '../Model/CustomValidators/General/Sortable';
import { ActionAccess } from '../Base/Controller';
import { SortMDL } from '../Model/Entity/ReadMDL';
import { ArangojsError } from 'arangojs/lib/request.node';
import { aql } from 'arangojs';

export class User extends Entity {
	public _collectionName = 'User';

	_key: string;
	@IsDefined({ groups: CUF })
	@Length(2, 20, { groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	public firstName: string = null;

	@IsDefined({ groups: CUF })
	@Length(2, 20, { groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	public lastName: string = null;

	@IsDefined({ groups: CUF })
	@NationalCode({ groups: CUF })
	public nationalCode: string = null;

	@IsDefined({ groups: CUF })
	@IranMobileNumber({ groups: CUF })
	@Filterable()
	public mobile: string = null;

	@IsDefined({ groups: CUF })
	@IranPhoneNumber({ groups: CUF })
	public phone: string = null;

	@IsDefined({ groups: CUF })
	@IsDateString({}, { groups: CUF })
	public birthdate: string = null;

	@IsDefined({ groups: CUF })
	@Gender({ groups: CUF })
	public gender: string = null;

	@IsDefined({ groups: CUF })
	@MaritalStatus({ groups: CUF })
	public maritalStatus: string = null;

	@IsDefined({ groups: C })
	@Username({ groups: C })
	public username: string = null;

	@IsOptional({ groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	@Updatable()
	@Sortable()
	fullname: string = null;

	@IsOptional({ groups: CUF })
	@IsString({ groups: CUF })
	@Filterable()
	@Updatable()
	@Sortable()
	status: string = null;

	@IsDefined({ groups: CU })
	@Password({ groups: CU })
	public password: string = null;

	@IsArray({ groups: CUF })
	@Filterable()
	@Updatable()
	public roles: Role[];

	createdAt: number = null;

	createdBy: string = null;

	@ActionAccess('User/read', 'Company')
	async read(
		data: {
			pageSize?: number;
			page?: number;
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			//@ts-ignore
			filter?: FilterMDL[];
			sort?: SortMDL[];
		} = {
			pageSize: 10,
			page: 1,
			filter: [],
			sort: []
		}
	) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}

		if (typeof data === 'string') {
			let result;
			try {
				const cursor = await db.query(
					`RETURN DOCUMENT("` +
						this._collectionName +
						`/` +
						data +
						`")`
				);
				result = await cursor.reduce((acm, item) => item, undefined);
			} catch (e) {
				throw {
					message: (e as ArangojsError).message,
					name: (e as ArangojsError).name,
					stack: (e as ArangojsError).stack
				};
			}
			if (result === undefined || result === null) throw 'Not Found';
			else return result;
		} else {
			// LIST REQUESTED
			data = {
				...data,
				pageSize: data.pageSize || 10,
				page: data.page ? data.page - 1 : 0
			};

			try {
				const collection = db.collection(this._collectionName);
				const query1 = `
				FOR u in ${collection.name}
				${!!data?.sort?.length ? 'SORT' : ''}
				${
					!!data?.sort?.length
						? data?.sort?.reduce(
								(acm, sortItem) =>
									(acm ? acm + ', ' : '') +
									('u.' +
										sortItem.key +
										'' +
										(sortItem.desc ? ' DESC ' : '')),
								''
						  )
						: ''
				}
				FILTER ${
					data?.filter
						?.map(({ op, value, key }) =>
							op === 'IN' || op === 'NOT IN'
								? '"' + value + '" ' + op + ' u.' + key
								: 'u.' +
								  key +
								  ' ' +
								  op +
								  (typeof value === 'string'
										? ' "' + value + '"'
										: value)
						)
						.reduce(
							(strFilters, strFilter) =>
								strFilters +
								(strFilters ? ' AND ' : '') +
								strFilter,
							''
						) || ' true '
				}
					LIMIT ${data.page * data.pageSize},${data.pageSize}
					
					RETURN u
			`;
				const query2 = `
				FOR u in ${collection.name}
				${!!data?.sort?.length ? 'SORT' : ''}
				${
					!!data?.sort?.length
						? data?.sort?.reduce(
								(acm, sortItem) =>
									(acm ? acm + ', ' : '') +
									('u.' +
										sortItem.key +
										'' +
										(sortItem.desc ? ' DESC ' : '')),
								''
						  )
						: ''
				}
				FILTER ${
					data?.filter
						?.map(({ op, value, key }) =>
							op === 'IN' || op === 'NOT IN'
								? '"' + value + '" ' + op + ' u.' + key
								: 'u.' + key + ' ' + op + ' "' + value + '"'
						)
						.reduce(
							(strFilters, strFilter) =>
								strFilters +
								(strFilters ? ' AND ' : '') +
								strFilter,
							''
						) || ' true '
				}
				RETURN  u
			`;

				let cursor = await db.query(query1);
				const items = await cursor.all();
				cursor = await db.query(query2);
				const totalCount = await cursor.all();
				return {
					items,
					totalCount: totalCount.length
				};
			} catch (e) {
				return e;
			}
		}
	}

	constructor() {
		super();
	}

	async fetchZoneTree(
		zone: ZoneName = 'Company',
		parentZone: string = null
	): Promise<{ zoneKey: ZoneName; accessList: Access[] }> {
		const res = await this.getAccess(zone, parentZone);
		if (!res) return { zoneKey: zone, accessList: [] };
		//const { access, zoneId } = res;

		const accessList = await Promise.all(
			res.map(async ({ access, zoneId }): Promise<Access> => {
				if (access && access?.subZones) {
					const res = access?.subZones?.map(
						async (
							z
						): Promise<{
							zoneKey: ZoneName;
							accessList: Access[];
						}> => await this.fetchZoneTree(z, zoneId)
					);
					const subZoneAccessList = await Promise.all(res);
					//@ts-ingore
					access['zoneId'] = zoneId;
					//@ts-ignore
					access['subZones'] = subZoneAccessList;
				}
				return access;
			})
		);

		return { zoneKey: zone, accessList };
	}

	async getUserRole(
		zone: ZoneName,
		parentZone: string
	): Promise<{ roleId: RoleIds; zoneId: string }[]> {
		const c = await User.runAQL(
			aql`
			FOR i IN @@zone
				FILTER @zoneName == "Company" OR LENGTH(i.zones[@parentZoneName][* FILTER CURRENT.k == @parentZone AND !CURRENT.l]) > 0 
				FILTER i.zoneAccess AND !!i.zoneAccess[@user]
				RETURN {roleId: i.zoneAccess[@user], zoneId: i._id }
		`.query,
			{
				user: 'User/' + this._key,
				'@zone': zone,
				parentZone: parentZone?.split('/')[1] || '',
				zoneName: zone,
				parentZoneName: parentZone?.split('/')[0] || ''
			}
		);
		if (c && c.length) {
			return c as { roleId: RoleIds; zoneId: string }[];
		} else {
			return [];
		}
	}

	async getAccess(
		zone: ZoneName,
		parentZone: string
	): Promise<{ access: Access; zoneId: string }[]> {
		const res = await this.getUserRole(zone, parentZone);
		const roles = res.map(async ({ roleId, zoneId }) => ({
			access: (
				await User.getDB()
					.collection('Role')
					.document(roleId.replace('Role/', ''))
			).accessList[zone] as Access,
			zoneId
		}));
		return await Promise.all(roles);
	}
}
