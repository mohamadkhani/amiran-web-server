import { IsBoolean, IsNumber, IsString } from 'class-validator';
import Entity, { F } from '../Base/Entity';
import { Filterable } from '../Model/CustomValidators/General/Filterable';
import { Sortable } from '../Model/CustomValidators/General/Sortable';

export class WarehouseReception extends Entity {
	_collectionName = 'WarehouseReception';

	@IsString({ groups: F })
	@Sortable()
	@Filterable()
	_key: string = null;

	packages: string[] = [];

	description: string = null;

	@IsNumber({}, { groups: F })
	@Sortable()
	@Filterable()
	packageCount: number = null;

	@IsNumber({}, { groups: F })
	@Sortable()
	itemCount: string = null;

	@IsBoolean({ groups: F })
	@Sortable()
	@Filterable()
	forced: boolean = false;

	@IsNumber({}, { groups: F })
	@Sortable()
	@Filterable()
	createdAt: number = null;

	createdBy: string = null;
}
