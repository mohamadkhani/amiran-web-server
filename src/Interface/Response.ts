export type ResponseStatus =
	| 'OK'
	| 'Bad Request'
	| 'Not Found'
	| 'Internal Server Error'
	| 'Forbidden'
	| 'Unauthorized'
	| 'Not Implemented';

type Message = {
	title: string;
	content?: string | any;
	type: 'success' | 'error' | 'warning' | 'info';
	devMsg?: string[];
};

class Response {
	constructor(
		public data: Record<string, any> | {},
		public messages: Message[] = [],
		public status: ResponseStatus = 'OK',
		public devMsg: string[] = []
	) {}
}

export default Response;
