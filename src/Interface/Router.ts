import Response from './Response';
import { Socket } from 'socket.io';
import SocketServer from '../APIServer';

import User from '../Controller/UserController';
import Item from '../Controller/ItemController';
import Package from '../Controller/PackageController';
import ItemType from '../Controller/ItemTypeController';
import ItemSpec from '../Controller/ItemSpecController';
import Role from '../Controller/RoleController';
import Shift from '../Controller/ShiftController';
import Warehouse from '../Controller/WarehouseController';
import WarehouseReception from '../Controller/WarehouseReceptionController';
import GuaranteeReception from '../Controller/GuaranteeReceptionController';
import ProductionLine from '../Controller/ProductionLineController';
import Company from '../Controller/CompanyController';
import Customer from '../Controller/CustomerController';
import Order from '../Controller/OrderController';
import { ZoneName } from '../Entity/Role';

export const Factory = {
	User,
	Item,
	Package,
	ItemType,
	ItemSpec,
	Role,
	Shift,
	ProductionLine,
	Order,
	Warehouse,
	WarehouseReception,
	GuaranteeReception,
	Customer,
	Company
};
export type factoryKeys = keyof typeof Factory;
type requestType = {
	model: keyof typeof Factory;
	action: string;
	zone: Record<ZoneName, string>;
	data: any;
};

class Router {
	private readonly model: factoryKeys;
	private readonly action: string;
	private readonly zone: Record<ZoneName, string>;
	private readonly data: Record<string, any> = {};
	private readonly socket: Socket;
	private readonly runApiMethodFn: (
		model: string,
		method: string,
		zone: Record<ZoneName, string>,
		req: any,
		io: Socket
	) => Promise<any>;

	constructor(request: requestType) {
		this.socket = SocketServer.socket;
		this.model = request.model;
		this.zone = request.zone;
		this.action = request.action;
		this.data = request.data;
		this.runApiMethodFn = this.route();
	}

	public async run(req: any, io: Socket): Promise<Response> {
		return await this.runApiMethodFn(
			this.model,
			this.action,
			this.zone,
			req,
			io
		);
	}

	private route() {
		if (!Object.keys(Factory).includes(this.model)) {
			return () =>
				new Promise((resolve) => {
					resolve(
						new Response(
							null,
							[{ title: 'Wrong Entity', type: 'error' }],
							'Not Found'
						)
					);
				});
		}
		const factory = new Factory[this.model]();
		const method = factory.runApiMethodFn;
		return method.bind(factory) as () => Promise<Response>;
	}
}

export default Router;
