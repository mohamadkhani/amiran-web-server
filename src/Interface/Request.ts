import Router from './Router';
import Response from './Response';
import { Socket } from 'socket.io';
import UserController from '../Controller/UserController';

class Request {
	constructor(private request: any, private io: Socket) {}

	public async exec(): Promise<Response> {
		if (this.isStandard()) {
			if (!this.isAuthorized()) {
				return new Response(
					null,
					[{ type: 'error', title: 'Unauthorized' }],
					'Unauthorized',
					[]
				);
			}

			try {
				return await new Router(this.request).run(
					this.request.data,
					this.io
				);
			} catch (e) {
				return new Response(
					undefined,
					[{ type: 'error', title: 'Internal Server Error' }],
					'Internal Server Error',
					[e.stack]
				);
			}
		} else {
			return new Response(
				null,
				[{ type: 'error', title: 'Bad Request' }],
				'Bad Request',
				[]
			);
		}
	}

	public isAuthorized(): boolean {
		if (
			UserController.userInfo[this.io.id] ||
			(this.request.model === 'User' &&
				(this.request.action === 'login' ||
					this.request.action === 'authenticate'))
		)
			return true;
		else return false;
	}

	public isStandard() {
		return (
			this.isJSON(JSON.stringify(this.request)) &&
			this.request.hasOwnProperty('model') &&
			this.request.hasOwnProperty('action') &&
			this.request.hasOwnProperty('data') &&
			typeof this.request.model === 'string' &&
			typeof this.request.action === 'string' &&
			this.isJSON(JSON.stringify(this.request.data))
		);
	}

	public isJSON(str: string) {
		let result: boolean;
		try {
			result = !!JSON.parse(str);
		} catch (e: any) {
			result = false;
		}
		return result;
	}
}

export default Request;
