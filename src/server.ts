import APIServer from './APIServer';
new APIServer(3003, {
	cors: {
		origin: '*',
		methods: ['GET', 'POST'],
		allowedHeaders: ['my-custom-header'],
		credentials: true
	}
});
