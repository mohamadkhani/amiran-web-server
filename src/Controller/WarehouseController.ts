import { aql } from 'arangojs';
import { Socket } from 'socket.io';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import { db } from '../Base/Entity';
import Model from '../Base/Model';
import { Package } from '../Entity/Package';
import { Warehouse } from '../Entity/Warehouse';
import Response from '../Interface/Response';
import CriticalInfoMDL from '../Model/Warehouse/CriticalInfoMDL';
import SummaryInGRNTMDL from '../Model/Warehouse/SummaryInGRNTMDL';
import SummaryInLodgingMDL from '../Model/Warehouse/SummaryInLodgingMDL';
import { NULL_MDL } from './UserController';

export default class WarehouseController extends DataController<Warehouse> {
	protected defaultEntity = new Warehouse();
	models: Record<string, Model> = {
		create_api: Warehouse,
		getVirtualShifts_api: NULL_MDL,
		getSummary_api: NULL_MDL,
		criticalInfo_api: CriticalInfoMDL,
		getSummaryInGuaranteeSpan_api: SummaryInGRNTMDL,
		getSummaryInLodgingSpan_api: SummaryInLodgingMDL
	};

	async read_api(data, zone) {
		return super.read_api(data, zone);
	}
	@ActionAccess('Warehouse/getVirtualShifts', 'Warehouse')
	async getVirtualShifts_api({}) {
		const query = `
			FOR s IN Shift
			FILTER LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == 'virtual'])>0
			RETURN s
		`;
		const cursor = await db.query(query);
		const virtualShifts = await cursor.all();
		if (!virtualShifts || virtualShifts.length === 0)
			return new Response(
				{},
				[{ title: 'No VitualShiftFound', type: 'error' }],
				'Not Found'
			);
		else {
			return new Response(virtualShifts);
		}
	}
	@ActionAccess('Warehouse/fetchDrivers', 'Warehouse')
	async fetchDrivers_api({}) {
		const query = `
			FOR d IN Driver
			RETURN d
		`;
		const cursor = await db.query(query);
		const virtualShifts = await cursor.all();
		if (!virtualShifts || virtualShifts.length === 0)
			return new Response(
				{},
				[{ title: 'No VitualShiftFound', type: 'error' }],
				'Not Found'
			);
		else {
			return new Response(virtualShifts);
		}
	}

	@ActionAccess('Warehouse/getSummary', 'Warehouse')
	async getSummary_api({}, zone: ZoneObj, socket: Socket) {
		let result;
		try {
			result = await Package.runAQL(
				aql`
					LET itemTypes = (
						FOR i IN ItemType 
						SORT i.title
						RETURN i )
					FOR it IN itemTypes
					LET itemCount =  LENGTH(
						FOR itm IN Item 
						FILTER REGEX_TEST(itm._key,it.itemRegex) 
						//FILTER DOCUMENT(Shift, itm.shiftCode).itemType._key == it._key
						AND LENGTH(itm.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						RETURN itm)
					LET packageCount = LENGTH(
						FOR p IN Package 
						FILTER REGEX_TEST(p._key,it.packageRegex)
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						FILTER p.status == 'stored' || p.status == 'stored-opened'
						RETURN p)
					LET packageCountGroupedByOption = 
					(FOR p IN Package
						FILTER REGEX_TEST(p._key,it.packageRegex)
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						FILTER p.status == 'stored'
						COLLECT 
							packing = p.packing WITH COUNT INTO pCount
						RETURN { packing, packageCount: pCount}
					)	
					RETURN {itemType: it, details:{itemCount, packageCount, packageCountGroupedByOption}}
			`.query,
				{ zoneKey: zone.key }
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Could not fetch data1',
						type: 'error',
						devMsg: [e, 'error']
					}
				],
				'Bad Request'
			);
		}
		if (!result || result.length == 0)
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);

		return new Response(result, [], 'OK');
	}

	@ActionAccess('Warehouse/criticalInfo', 'Warehouse')
	async criticalInfo_api({ guaranteeDate, days }, zone: ZoneObj) {
		const daysLength = days * 24 * 3600 * 1000;
		try {
			const result = (await Warehouse.runAQL(
				aql`
			LET inGuararnteeAlert = LENGTH(
				LET shifts =  (
					FOR s IN Shift
					FILTER s.guaranteeDate < @guaranteeDate
					RETURN s._key
					)
					
				
				FOR p IN Package
				FILTER LENGTH(FOR s IN p.shiftCode
					FILTER s IN shifts
					RETURN s) > 0
				FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				RETURN p
				)
			LET lodging = LENGTH(
			FOR p IN Package
			FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND (DATE_NOW() - CURRENT.e)> @daysLength AND !CURRENT.l])>0
			RETURN p)
			RETURN {inGuararnteeAlert, lodging}
			`.query,
				{
					zoneKey: zone.key,
					guaranteeDate,
					daysLength
				}
			)) || [{ inGuararnteeAlert: 0, lodging: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Warehouse/getSummaryInGuaranteeSpan', 'Warehouse')
	async getSummaryInGuaranteeSpan_api({ guaranteeDate }, zone: ZoneObj) {
		let result;
		try {
			result = await Package.runAQL(
				aql`
					LET shifts =  (
						FOR s IN Shift
						FILTER s.guaranteeDate < @guaranteeDate
						RETURN s._key
						)
					LET itemTypes = (
						FOR i IN ItemType 
						SORT i.title
						RETURN i )
					FOR it IN itemTypes
					LET itemCount =  LENGTH(
						FOR itm IN Item 
						FILTER REGEX_TEST(itm._key,it.itemRegex) 
						FILTER LENGTH(FOR s IN shifts
							FILTER itm.shiftCode == s
							RETURN s) > 0
						//FILTER DOCUMENT(Shift, itm.shiftCode).itemType._key == it._key
						AND LENGTH(itm.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						RETURN itm)
					LET packageCount = LENGTH(
						FOR p IN Package 
						FILTER REGEX_TEST(p._key,it.packageRegex)
						FILTER LENGTH(FOR s IN p.shiftCode
							FILTER s IN shifts
							RETURN s) > 0
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						FILTER p.status == 'stored' || p.status == 'stored-opened'
						RETURN p)
					LET packageCountGroupedByOption = 
					(FOR p IN Package
						FILTER REGEX_TEST(p._key,it.packageRegex)
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						FILTER p.status == 'stored'
						COLLECT 
							packing = p.packing WITH COUNT INTO pCount
						RETURN { packing, packageCount: pCount}
					)	
					RETURN {itemType: it, details:{itemCount, packageCount, packageCountGroupedByOption}}
			`.query,
				{ zoneKey: zone.key, guaranteeDate }
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Could not fetch data1',
						type: 'error',
						devMsg: [e, 'error']
					}
				],
				'Bad Request'
			);
		}
		if (!result || result.length == 0)
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);

		return new Response(result, [], 'OK');
	}

	@ActionAccess('Warehouse/getSummaryInLodgingSpan', 'Warehouse')
	async getSummaryInLodgingSpan_api({ days }, zone: ZoneObj) {
		let result;
		const daysLength = days * 24 * 3600 * 1000;
		try {
			result = await Package.runAQL(
				aql`
					LET itemTypes = (
						FOR i IN ItemType 
						SORT i.title
						RETURN i )
					FOR it IN itemTypes
					LET itemCount =  LENGTH(
						FOR itm IN Item 
						FILTER REGEX_TEST(itm._key,it.itemRegex) 
						
						AND LENGTH(itm.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND (DATE_NOW() - CURRENT.e)> @daysLength AND !CURRENT.l])>0
						RETURN itm)
					LET packageCount = LENGTH(
						FOR p IN Package 
						FILTER REGEX_TEST(p._key,it.packageRegex)
					
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND (DATE_NOW() - CURRENT.e)> @daysLength AND !CURRENT.l])>0
						FILTER p.status == 'stored' || p.status == 'stored-opened'
						RETURN p)
					LET packageCountGroupedByOption = 
					(FOR p IN Package
						FILTER REGEX_TEST(p._key,it.packageRegex)
						AND LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND (DATE_NOW() - CURRENT.e)> @daysLength AND !CURRENT.l])>0
						FILTER p.status == 'stored'
						COLLECT 
							packing = p.packing WITH COUNT INTO pCount
						RETURN { packing, packageCount: pCount}
					)	
					RETURN {itemType: it, details:{itemCount, packageCount, packageCountGroupedByOption}}
			`.query,
				{ zoneKey: zone.key, daysLength }
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Could not fetch data',
						type: 'error',
						devMsg: [e, 'error']
					}
				],
				'Bad Request'
			);
		}
		if (!result || result.length == 0)
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);

		return new Response(result, [], 'OK');
	}
}
