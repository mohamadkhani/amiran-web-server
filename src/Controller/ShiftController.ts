import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import { Shift, ShiftStatus, shiftStatuses } from '../Entity/Shift';
import ReadMDL, { FilterMDL, SortMDL } from '../Model/Entity/ReadMDL';
import Response from '../Interface/Response';
import { Socket } from 'dgram';
import { nanoid } from 'nanoid';
import UpdateMDL from '../Model/Entity/UpdateMDL';
import Controller, { ActionAccess, ZoneObj } from '../Base/Controller';
import { User } from '../Entity/User';
import { StartMDL } from '../Model/Shift/StartMDL';
import { aql } from 'arangojs';
import { PauseMDL } from '../Model/Shift/PauseMDL';
import { NULL_MDL } from './UserController';
import { StopMDL } from '../Model/Shift/StopMDL';
import { QCVerifyMDL } from '../Model/Shift/QCVerifyMDL';
import { UpdateWorkersRolesMDL } from '../Model/Shift/UpdateWorkersRolesMDL';
import FindByKey from '../Model/FindByKey';
import { kavenegar } from 'kavenegar';
import moment from 'moment-jalaali';
export default class ShiftController extends DataController<Shift> {
	models: Partial<Record<keyof ShiftController, Model>> = {
		create_api: Shift,
		addStatusListener_api: class extends Model {},
		removeStatusListener_api: class extends Model {},
		start_api: StartMDL,
		pause_api: PauseMDL,
		stop_api: PauseMDL,
		getAssignableWorkers_api: NULL_MDL,
		getShiftRoles_api: NULL_MDL,
		getInfo_api: NULL_MDL,
		qCVerify_api: QCVerifyMDL,
		getAdminShiftsHistory_api: ReadMDL,
		updateWorkersRoles_api: UpdateWorkersRolesMDL,
		getQCShiftsList_api: ReadMDL,
		find_api: FindByKey
	};

	defaultEntity = new Shift();

	private static lineStatusChangeListeners: Record<
		string,
		(shiftId: string) => void
	> = {};

	constructor() {
		super();
	}

	@ActionAccess('Shift/create', 'ProductionLine')
	async create_api(data: Shift, zone: ZoneObj, socket) {
		const userInstance = new User();
		const userKey = this.getUserInfo(socket).userKey;
		const filter = [new FilterMDL('==', '_key', userKey)];
		const usersList = await userInstance.read({ filter });
		if (usersList.totalCount === 0)
			return new Response(
				{},
				[{ title: 'User Not Found', type: 'error' }],
				'Not Found'
			);

		//data.workers = [...data.workers, pm];
		//data.workersRoles[pm._key] = pmRole;
		data.zoneAccess = {
			...data.zoneAccess,
			['User/' + userKey]: 'Role/productManager'
		};
		data.status = 'paused';
		const res = await super.create_api(data, zone, socket);
		Controller.sendCommandToAllUsers('ReloadZoneTree');
		return res;
	}

	@ActionAccess('Shift/read', 'ProductionLine')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}

	@ActionAccess('Shift/update', 'ProductionLine')
	async update_api(data: UpdateMDL<Shift>) {
		const status = data.data.status;
		if (!shiftStatuses.includes(status))
			return new Response({}, [], 'Bad Request');
		try {
			const response = await super.update_api({
				key: data.key,
				data: { status }
			} as UpdateMDL<Shift>);

			Object.values(ShiftController.lineStatusChangeListeners).forEach(
				(fn) => {
					fn(response.data as any as string);
					return new Response(
						{},
						[{ title: 'Successfully Updated', type: 'success' }],
						'OK'
					);
				}
			);
			const newStatus = (response.data as { after: Shift }).after.status;
			let s = '';
			switch (newStatus) {
				case 'producing':
					s = 'Started';
					break;
				case 'finished':
					s = 'Finihsed';
					break;
				case 'paused':
					s = 'Paused';
					break;
			}
			response.messages = [
				{
					title: 'Shift ' + s,
					type: 'success'
				}
			];
			return response;
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Internal Server Error', type: 'error' }],
				'Internal Server Error'
			);
		}
	}

	// @ActionAccess('Shift/getActiveOnes', 'ProductionLine')
	// async getActiveOnes_api(data: null, zone: ZoneObj) {
	// 	const shiftInstance = new Shift();
	// 	const filter = [
	// 		new FilterMDL<Shift, ShiftStatus>('==', 'status', 'producing')
	// 	];
	// 	const shiftList = await shiftInstance.read({ filter }, zone);
	// 	if (!shiftList || shiftList.totalCount === 0)
	// 		return new Response(
	// 			{},
	// 			[{ title: 'No Active Shift Found', type: 'warning' }],
	// 			'Not Found'
	// 		);
	// 	else return new Response(shiftList, [], 'OK');
	// }

	@ActionAccess('Shift/addStatusListener', 'Shift')
	addStatusListener_api(data: any, zone, socket: Socket) {
		const listnerId = nanoid();
		ShiftController.lineStatusChangeListeners[listnerId] = (shiftId) => {
			socket.emit(
				listnerId,
				new Response({ id: shiftId }, [
					{
						title: 'Shift Status Changed',
						type: 'info'
					}
				])
			);
		};
		socket.on('disconnect', () => {
			ShiftController.removeListener(listnerId);
		});
		socket.on('error', () => {
			ShiftController.removeListener(listnerId);
		});
		return new Response({ id: listnerId });
	}

	@ActionAccess('Shift/removeStatusListener', 'Shift')
	removeStatusListener_api({ id }: { id: string }) {
		ShiftController.removeListener(id);
		return new Response({});
	}

	@ActionAccess('Shift/find', 'ProductionLine')
	async find_api({ _key }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Shift.runAQL(
				aql`
				FOR s IN Shift
				FILTER s._key == @shiftKey 
				FILTER LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey])>0
				RETURN s
			`.query,
				{ shiftKey: _key, zoneKey: zone.key }
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Shift not found',
							type: 'error'
						}
					],
					'Not Found'
				);
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
	}

	static removeListener(id: string) {
		ShiftController.lineStatusChangeListeners = Object.fromEntries(
			Object.entries(ShiftController.lineStatusChangeListeners).filter(
				([k]) => k !== id
			)
		);
	}

	@ActionAccess('Shift/start', 'ProductionLine')
	async start_api({ shiftKey, zoneAccess }: StartMDL, zone: ZoneObj, socket) {
		const userKey = this.getUserInfo(socket).userKey;
		const query = aql`
			LET $shift = DOCUMENT("Shift", @shiftKey)
			LET $newInterval = {
				started: DATE_NOW(),
				zoneAccess: @zoneAccess
			}
			LET updated = (FOR $item IN [$shift]
				FILTER $item.status == "paused"
				UPDATE $item._key WITH {
					zoneAccess: MERGE(@zoneAccess, {[@userId]: $item.zoneAccess[@userId] }),
					intervals: $item.intervals?PUSH($item.intervals, $newInterval):[$newInterval], 
					status: "producing"
					} IN Shift OPTIONS { mergeObjects: false }
				RETURN NEW
			)
			LET $err = LENGTH(updated) ? false : ($shift ? {success: false, error: "shift status is invalid"} :{success: false, error: "shift not found", updated})
			RETURN $err  || ({success: true, updated})
		`;
		const result = await Shift.runAQL(query.query, {
			shiftKey,
			zoneAccess,
			userId: 'User/' + userKey
		});
		if (result[0].success) {
			Object.values(ShiftController.lineStatusChangeListeners).forEach(
				(l) => typeof l == 'function' && l(shiftKey)
			);
			Controller.sendCommandToAllUsers('ReloadZoneTree');

			const shift: Shift = await Shift.getDB()
				.collection('Shift')
				.document(shiftKey);
			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/manager'
			]);
			numbers.forEach((number) =>
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: shift.productionEstimation,
					token10: shift.itemType?.title,
					token20: moment().format('jYYYY/jM/jD - H:m'),
					template: 'plStart'
				})
			);

			return new Response({ new: result[0].updated }, [
				{ title: 'The shift successfully started.', type: 'success' }
			]);
		} else {
			return new Response({ new: result[0].updated }, [
				{
					title: 'There is a problem with starting shift.',
					type: 'error',
					devMsg: [result[0].error]
				}
			]);
		}
	}

	@ActionAccess('Shift/pause', 'ProductionLine')
	async pause_api({ shiftKey }: PauseMDL) {
		const query = aql`
			LET $shift = DOCUMENT("Shift", @shiftKey)
			LET updated= (FOR $item IN [$shift]
				FILTER $item.status == "producing"
				UPDATE $item._key WITH {
					intervals: REPLACE_NTH($item.intervals, -1, MERGE(LAST($item.intervals), {finished: DATE_NOW()})), 
					status: "paused"
					} IN Shift OPTIONS { mergeObjects: false }
				RETURN NEW
			)
			LET $err = LENGTH(updated) ? false : {success: false, error: "item not found", updated}
			RETURN $err  || ({success: true, updated: updated})
		`;
		const result = await Shift.runAQL(query.query, { shiftKey });
		if (result[0].success) {
			Object.values(ShiftController.lineStatusChangeListeners).forEach(
				(l) => typeof l == 'function' && l(shiftKey)
			);

			const shift: Shift = await Shift.getDB()
				.collection('Shift')
				.document(shiftKey);
			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/manager'
			]);
			numbers.forEach((number) =>
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: 'code:' + shift._key,
					token10: shift.itemType?.title,
					token20: moment().format('jYYYY/jM/jD - H:m'),
					template: 'plPause'
				})
			);
			return new Response({ new: result[0].updated }, [
				{ title: 'The shift successfully paused.', type: 'success' }
			]);
		} else {
			return new Response({ new: result[0].updated }, [
				{
					title: 'There is a problem with pausing shift.',
					type: 'error',
					devMsg: [result[0].error]
				}
			]);
		}
	}

	@ActionAccess('Shift/stop', 'ProductionLine')
	async stop_api(
		{ shiftKey, report, force }: StopMDL,
		{ key: zoneKey }: ZoneObj,
		socket
	) {
		const userKey = this.getUserInfo(socket).userKey;

		if (!force) {
			const qcCheckQuery = aql`FOR $item IN Shift
				FILTER 
					LENGTH($item.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				FILTER
					($item.status == "producing" || $item.status == "paused") && $item.verifiedByQC == true
				RETURN $item
					`;
			const qcCheckResult = await Shift.runAQL(qcCheckQuery.query, {
				zoneKey
			});
			if (qcCheckResult.length === 0) {
				return new Response({ qcVerificationNotPassed: true }, [
					{
						title: 'QC Verification Problem',
						content: 'Please wait for the QC Manager verification.',
						type: 'error'
					}
				]);
			}
		}

		const query = aql`
		LET updated= (FOR $item IN Shift
			FILTER 
				LENGTH($item.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			FILTER
				$item.status == "producing" || $item.status == "paused"
			UPDATE $item._key WITH {
				zoneAccess: {},
				intervals: REPLACE_NTH($item.intervals, -1, MERGE(LAST($item.intervals), {finished: DATE_NOW()})), 
				status: "finished",
				report: @report
				} IN Shift OPTIONS { mergeObjects: false }
			RETURN NEW
		)
		LET $err = LENGTH(updated) ? false : {success: false, error: "item not found", updated}
		RETURN $err  || ({success: true, updated: updated })
	`;
		const result = await Shift.runAQL(query.query, {
			zoneKey,
			report
		});
		if (result[0].success) {
			Object.values(ShiftController.lineStatusChangeListeners).forEach(
				(l) => typeof l == 'function' && l(shiftKey)
			);
			Controller.sendCommandToAllUsers('ReloadZoneTree');
			const shift: { type: string; items: number; duration: number }[] =
				(await Shift.runAQL(
					aql`
				LET shift =  DOCUMENT('Shift',@shiftKey)
				RETURN {
						type:shift.itemType.title,
						items: length(for i in Item filter i.shiftCode == @shiftKey return i),
						duration: (LAST(shift.intervals).finished - FIRST(shift.intervals).started)/1000
					 }
			`.query,
					{ shiftKey }
				)) || [{ type: '', items: 0, duration: 0 }];

			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/manager'
			]);
			numbers.forEach((number) =>
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: moment().format('H:m'),
					token10: shift[0]?.type,
					token20: `${shift[0].items} :تعداد\n${moment()
						.utc(shift[0].duration)
						.format('H:m')} :مدت`,
					template: 'plStop'
				})
			);
			return new Response({ new: result[0].updated }, [
				{ title: 'The shift successfully finished.', type: 'success' }
			]);
		} else {
			return new Response({ new: result[0].updated }, [
				{
					title: 'There is a problem with pausing shift.',
					type: 'error',
					devMsg: [result[0].error]
				}
			]);
		}
	}

	@ActionAccess('Shift/getAssignableWorkers', 'ProductionLine')
	async getAssignableWorkers_api(data: null, zone: ZoneObj) {
		const workers = await Shift.runAQL(
			aql`
		FOR u in User 
			FILTER u.status =='active' AND LENGTH(u.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			RETURN {fullname: u.fullname, _key:u._key, username:u.username}
		`.query,
			{ zoneKey: zone.key }
		);
		return new Response({
			workers
		});
	}

	@ActionAccess('Shift/getShiftRoles', 'Company')
	async getShiftRoles_api({}, zone: ZoneObj) {
		return new Response({
			shiftRoles: await Shift.runAQL(
				aql`
			FOR u in Role
				FILTER LENGTH(u.zones.Company[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				FILTER !!u.accessList.Shift && u._key != 'productManager'
				RETURN u
			`.query,
				{ zoneKey: zone.key }
			)
		});
	}

	@ActionAccess('Shift/getInfo', 'Shift')
	async getInfo_api({}, zone) {
		return new Response(
			await Shift.getDB().collection('Shift').document(zone.key)
		);
	}

	@ActionAccess('Shift/qCVerify', 'ProductionLine')
	async qCVerify_api(
		{ shiftKey, report }: QCVerifyMDL,
		{ key: zoneKey },
		socket
	) {
		try {
			const userKey = this.getUserInfo(socket).userKey;
			const query = aql`
		LET updated= (FOR $item IN Shift
			FILTER @shiftKey == $item._key
			FILTER
				LENGTH($item.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			UPDATE $item._key WITH {
				verifiedByQC: true,
				QCManager: @userId,
				QCReport: @report,
				zoneAccess: MERGE($item.zoneAccess, {[@userId]: 'Role/qc' }),
			} IN Shift OPTIONS { mergeObjects: false }
			RETURN NEW
		)
		LET $err = LENGTH(updated) ? false : {success: false, error: "item not found", updated}
		RETURN $err  || ({success: true, updated: updated })
	`;

			const result = await Shift.runAQL(query.query, {
				zoneKey,
				userId: 'User/' + userKey,
				shiftKey,
				report
			});
			if (result[0].success) {
				return new Response({}, [
					{
						title: 'Success',
						content: 'The shift verified successfully',
						type: 'success'
					}
				]);
			} else {
				return new Response(
					{},
					[
						{
							title: 'Error',
							content:
								'There was a problem in verification of the shift.',
							type: 'error'
						}
					],
					'Forbidden'
				);
			}
		} catch (e) {
			return new Response({}, [
				{
					title: 'Error',
					content:
						'There was a problem in verification of the shift.',
					type: 'error',
					devMsg: [e]
				}
			]);
		}
	}

	@ActionAccess('Shift/getAdminShiftsHistory', 'ProductionLine')
	async getAdminShiftsHistory_api(
		{ pageSize, page }: ReadMDL<Shift>,
		{ key },
		socket
	) {
		pageSize = pageSize || 10;
		page = page || 1;
		return new Response(
			(
				await Shift.runAQL(
					aql`
	LET items = (FOR s in Shift
		SORT s._key DESC
		FILTER 
			LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
		LIMIT @index, @pageSize
		LET workers = (FOR zoneAccessUserId IN ATTRIBUTES(s.zoneAccess) RETURN {userFullname: DOCUMENT(zoneAccessUserId).fullname, roleTitle: DOCUMENT(s.zoneAccess[zoneAccessUserId]).title})
		LET intervals = (
			FOR interval in s.intervals
			FILTER LENGTH(ATTRIBUTES(interval.zoneAccess)) > 0
				let wk = (FOR zoneAccessUserId IN ATTRIBUTES(interval.zoneAccess) RETURN {userFullname: DOCUMENT(zoneAccessUserId).fullname, roleTitle: DOCUMENT(interval.zoneAccess[zoneAccessUserId]).title})
			RETURN {started: interval.started,finished: interval.finished, wk}
				)
		LET producedPackagesCount = LENGTH(FOR p in Package
				FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == s._key])>0
				RETURN p
			)
		LET producedItemsCount = SUM(FOR p in Package
			FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == s._key])>0
			RETURN COUNT(p.items)
		)
		RETURN MERGE(s, {workers, producedPackagesCount, producedItemsCount, intervals}))
	LET totalCount = 
		LENGTH(FOR s in Shift
			FILTER 
				LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			RETURN s
		)
	RETURN {items, totalCount}
					`.query,
					{ zoneKey: key, pageSize, index: pageSize * (page - 1) }
				)
			)[0],
			[]
		);
	}

	@ActionAccess('Shift/getQCShiftsList', 'ProductionLine')
	async getQCShiftsList_api(
		{ pageSize, page }: ReadMDL<Shift>,
		{ key },
		socket
	) {
		pageSize = pageSize || 10;
		page = page || 1;
		return new Response(
			(
				await Shift.runAQL(
					aql`
	LET items = (FOR s in Shift
		SORT s._key DESC
		FILTER 
			LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
		LIMIT @index, @pageSize
		LET workers = (FOR zoneAccessUserId IN ATTRIBUTES(s.zoneAccess) RETURN {userFullname: DOCUMENT(zoneAccessUserId).fullname, roleTitle: DOCUMENT(s.zoneAccess[zoneAccessUserId]).title})
		LET producedPackagesCount = LENGTH(FOR p in Package
				FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == s._key])>0
				RETURN p
			)
		LET producedItemsCount = SUM(FOR p in Package
			FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == s._key])>0
			RETURN COUNT(p.items)
		)
		RETURN MERGE(s, {workers, producedPackagesCount, producedItemsCount, QCManagerFullname: s.QCManager ? DOCUMENT(s.QCManager).fullname: ''}))
	LET totalCount = 
		LENGTH(FOR s in Shift
			FILTER 
				LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			RETURN s
		)
	RETURN {items, totalCount}
					`.query,
					{ zoneKey: key, pageSize, index: pageSize * (page - 1) }
				)
			)[0],
			[]
		);
	}

	@ActionAccess('Shift/updateWorkersRoles', 'ProductionLine')
	async updateWorkersRoles_api(
		{ shiftKey, zoneAccess }: StartMDL,
		zone: ZoneObj,
		socket
	) {
		const userKey = this.getUserInfo(socket).userKey;
		const query = aql`
			LET $shift = DOCUMENT("Shift", @shiftKey)
			LET $newInterval = {
				started: DATE_NOW(),
				zoneAccess: @zoneAccess
			}
			LET updated = (FOR $item IN [$shift]
				FILTER $item.status == "paused" OR $item.status == "producing"
				UPDATE $item._key WITH {
					zoneAccess: MERGE(@zoneAccess, {[@userId]: $item.zoneAccess[@userId] }),
					intervals: $item.intervals?PUSH($item.intervals, $newInterval):[$newInterval], 
				} IN Shift OPTIONS { mergeObjects: false }
				RETURN NEW
			)
			LET $err = LENGTH(updated) ? false : ($shift ? {success: false, error: "shift status is invalid"} :{success: false, error: "shift not found", updated})
			RETURN $err  || ({success: true, updated})
		`;
		const result = await Shift.runAQL(query.query, {
			shiftKey,
			zoneAccess,
			userId: 'User/' + userKey
		});
		if (result[0].success) {
			Object.values(ShiftController.lineStatusChangeListeners).forEach(
				(l) => typeof l == 'function' && l(shiftKey)
			);
			Controller.sendCommandToAllUsers('ReloadZoneTree');
			return new Response({ new: result[0].updated }, [
				{
					title: 'The new workers roles set successfully.',
					type: 'success'
				}
			]);
		} else {
			return new Response({ new: result[0].updated }, [
				{
					title: 'There is a problem with setting the new workers roles.',
					type: 'error',
					devMsg: [result[0].error]
				}
			]);
		}
	}
}
