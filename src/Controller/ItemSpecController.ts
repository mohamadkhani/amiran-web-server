import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import { ItemSpec } from '../Entity/ItemSpec';
import ReadMDL from '../Model/Entity/ReadMDL';
import { ActionAccess, ZoneObj } from '../Base/Controller';

export default class ItemSpecController extends DataController<ItemSpec> {
	models: Record<string, Model> = {
		create_api: ItemSpec,
		read_api: ReadMDL
	};

	defaultEntity = new ItemSpec();

	constructor() {
		super();
	}
	@ActionAccess('ItemSpec/read', 'Company')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}
}
