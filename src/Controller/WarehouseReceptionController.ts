import { aql } from 'arangojs';
import { ActionAccess } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { WarehouseReception } from '../Entity/WarehouseReception';
import Response from '../Interface/Response';
import ReadMDL from '../Model/Entity/ReadMDL';

export default class WarehouseReceptionController extends DataController<WarehouseReception> {
	protected defaultEntity = new WarehouseReception();
	models: Record<string, Model> = {
		readd_api: ReadMDL
	};

	@ActionAccess('WarehouseReception/readd', 'Warehouse')
	async readd_api({ pageSize, page }, zone) {
		try {
			pageSize = pageSize || 10;
			page = page || 1;
			const result = await WarehouseReception.runAQL(
				aql`
				FOR w in WarehouseReception
				FILTER LENGTH(w.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0 
				LET packageDate = (FOR p IN Package 
				FILTER p._key == w.packages[0]
				RETURN p)
				SORT w.createdAt DESC
				LIMIT @index, @pageSize
				RETURN MERGE(w,{prodDate:packageDate[0].createdAt, prodType: packageDate[0].itemType})
			`.query,
				{ zoneKey: zone.key, index: pageSize * (page - 1), pageSize }
			);
			if (!result || result.length == 0)
				return new Response(
					{},
					[{ title: 'Not Found', type: 'error' }],
					'Not Found'
				);
			const totalCount = await WarehouseReception.runAQL(
				aql`
				RETURN LENGTH(FOR w in WarehouseReception
				FILTER LENGTH(w.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0 
				RETURN w)
			`.query,
				{ zoneKey: zone.key }
			);

			return new Response({ items: result, totalCount });
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
}
