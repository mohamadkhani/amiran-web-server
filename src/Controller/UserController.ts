import Response from '../Interface/Response';
import Model from '../Base/Model';
import LoginMDL from '../Model/User/LoginMDL';
import { User } from '../Entity/User';
import { DataController } from '../Base/DataController';
import ReadMDL, { FilterMDL } from '../Model/Entity/ReadMDL';
import md5 from 'md5';
import { sign, verify } from 'jsonwebtoken';
import { Socket } from 'socket.io';
import AuthMDL from '../Model/User/AuthMDL';
import Controller, { ActionAccess, ZoneObj } from '../Base/Controller';
import { aql } from 'arangojs';
import FindByKey from '../Model/FindByKey';
import ResetPassMDL from '../Model/User/ResetPassMDL';
import * as dotenv from 'dotenv';
dotenv.config({ path: `${process.cwd()}/.env.${process.env.NODE_ENV}` });

export const NULL_MDL = class extends Model {};

export default class UserController extends DataController<User> {
	models: Record<string, Model> = {
		login_api: LoginMDL,
		logout_api: class extends Model {},
		authenticate_api: AuthMDL,
		create_api: User,
		read_api: ReadMDL,
		getStaff_api: ReadMDL,
		resetPass_api: FindByKey,
		setPass_api: ResetPassMDL,
		getProfile_api: NULL_MDL,
		fetchWorkers_api: NULL_MDL,
		fetchZones_api: NULL_MDL,
		fetchZoneTree_api: NULL_MDL,
		getRoleInfo_api: NULL_MDL,
		getProvinces_api: NULL_MDL,
		getCities_api: FindByKey
	};

	defaultEntity = new User();

	constructor() {
		super();
	}

	/*@ActionAccess('User/fetchWorkers', 'Shift')
	public async fetchWorkers_api(data: null, zone: ZoneObj) {
		const role = new Role();
		const filter = [new FilterMDL('==', 'title', 'Worker')];
		const roles = await role.read({ filter }, zone);
		if (roles.totalCount === 0)
			return new Response(
				{},
				[{ title: 'Worker Role Not Found', type: 'error' }],
				'Not Found'
			);

		const workerRole = roles.items[0];
		let workers = [];
		try {
			const query = `
				FOR u IN User
				FILTER "${workerRole._key}" IN u.roles AND u.status == 'active'
	
				RETURN u
			`;
			const cursor = await db.query(query);
			workers = await cursor.all();
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Internal Server Error', type: 'error' }],
				'Internal Server Error'
			);
		}
		if (workers.length === 0)
			return new Response(
				{},
				[{ title: 'No Workers Found', type: 'error' }],
				'Not Found'
			);
		return new Response(workers, [], 'OK');
	}*/

	@ActionAccess('User/getStaff', 'Company')
	async getStaff_api(data, zone: ZoneObj) {
		const staff = User.getDB().collection('Company');
	}

	@ActionAccess('User/read', 'Company')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}
	@ActionAccess('User/getProvinces', 'Company')
	async getProvinces_api(data, zone: ZoneObj) {
		try {
			const result = await User.runAQL(
				aql`
				For l in Location
				RETURN {_key: l._key, title: l.name}
			`.query,
				{}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Could not fetch provinces',
							type: 'error'
						}
					],
					'Not Found'
				);
			return new Response({ items: result });
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Could not fetch provinces',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
	}
	@ActionAccess('User/getCities', 'Company')
	async getCities_api({ _key }, zone: ZoneObj) {
		try {
			const result = await User.runAQL(
				aql`
				For l in Location
				FILTER l._key == @lKey
				RETURN l.cities
			`.query,
				{ lKey: _key }
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Could not fetch cities',
							type: 'error'
						}
					],
					'Not Found'
				);
			return new Response({ items: result[0] });
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Could not fetch cities',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
	}

	@ActionAccess('User/getProfile', 'Company')
	public async getProfile_api(data: any, zone: ZoneObj, socket) {
		const userInfo = this.getUserInfo(socket);
		const userInstance = new User();
		const filter = [new FilterMDL('==', '_key', userInfo.userKey)];
		const userList = await userInstance.read({ filter });
		if (!userList || userList.totalCount === 0)
			return new Response(
				{},
				[{ title: 'Cannot Fetch Data', type: 'error' }],
				'Not Found'
			);
		const user = userList.items[0];
		delete user.password;
		delete user._key;
		delete user._id;
		return new Response(user, [], 'OK');
	}
	@ActionAccess('User/resetPass', 'Company')
	public async resetPass_api({ _key }, zone: ZoneObj, socket) {
		try {
			const result = await User.runAQL(
				aql`
				FOR u IN User
				FILTER u.username != 'admin' AND u._key == @uKey
				UPDATE u._key WITH {password: MD5('a12345')} IN User
				RETURN {before:OLD, after: NEW}
			`.query,
				{ uKey: _key }
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Forbidden', type: 'error' }],
					'Forbidden'
				);
			else
				return new Response(
					{},
					[{ title: 'Success', type: 'success' }],
					'OK'
				);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('User/setPass', 'Company')
	async setPass_api({ oldPass, newPass }, zone: ZoneObj, socket) {
		try {
			const userInfo = this.getUserInfo(socket);
			let result = await User.runAQL(
				aql`
				FOR u in User
				FILTER u._key == @uKey AND u.password == MD5(@oldPass)
				RETURN u
			`.query,
				{ uKey: userInfo.userKey, oldPass }
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Wrong password',
							type: 'error'
						}
					],
					'Not Found'
				);

			result = await User.runAQL(
				aql`
				UPDATE @uKey WITH {password: MD5(@newPass)} IN User
				RETURN NEW
			`.query,
				{ uKey: userInfo.userKey, newPass }
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Could not update the password',
							type: 'error'
						}
					],
					'Bad Request'
				);

			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Password successfully changed',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('User/login', 'Company')
	public async login_api(data: LoginMDL, zone: ZoneObj, socket: Socket) {
		let usersList = [];
		try {
			usersList = await User.runAQL(
				aql`
				FOR u in User
				FILTER ((u.username == @username AND u.password == @pass) OR ( u._key == @username AND u.password == @pass)) AND u.status=="active"
				AND u._id IN ATTRIBUTES(DOCUMENT('Company/amiran').zoneAccess)
				LET accessList = DOCUMENT(DOCUMENT('Company/amiran').zoneAccess[u._id]).accessList.Company
				RETURN MERGE(u, {accessList})
			`.query,
				{
					username: data.username,
					pass: md5(data.password)
				}
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Error In finding the user', type: 'error' }],
				'Internal Server Error',
				[e]
			);
		}
		if (usersList.length === 0)
			return new Response(
				{},
				[
					{
						title: 'Authentication Issue',
						content: 'Invalid username or password',
						type: 'error'
					}
				],
				'Not Found'
			);
		const user: { accessList: any; _key: string; _id: string } =
			usersList[0];

		// const getSubZonesCursor = await db.query(
		// 	`
		// 	FOR sz IN ` +
		// 	user?.access?.subZones[0] +
		// 	`
		// 		FILTER sz.zoneAccess[@uId]
		// 			RETURN {zoneId: sz._id, roleId: sz.zoneAccess[@uId], userId: @uId }
		// `,
		// 	{ uId: user._id }
		// );
		// const subZones = await getSubZonesCursor.all();

		const userInstance = new User();
		userInstance._key = user._key;
		//await this.fetchZones_api(user._key);

		const tokenInfo = {
			access: user.accessList,
			userKey: user._key
		};

		const token = sign(
			tokenInfo,
			process.env.ACCESS_TOKEN_SECERT as string
		);

		socket.on('disconnect', () => {
			delete UserController.userInfo[socket.id];
		});

		if (!token) {
			return new Response(
				{},
				[
					{
						title: 'Tokenization Problem',
						content: 'Problem in generating token',
						type: 'error'
					}
				],
				'Not Found'
			);
		} else {
			UserController.userInfo[socket.id] = { token, userKey: user._key };

			return new Response({ token: token }, [
				{ title: 'Authentication Success', type: 'success' }
			]);
		}
	}

	@ActionAccess('User/authenticate', 'Company')
	public authenticate_api(data: AuthMDL, zone: ZoneObj, socket: Socket) {
		let authenticationResult = false;
		let messageTitle: string = 'Unauthorized';
		let type: 'error' | 'success' = 'error';
		let status: 'Unauthorized' | 'OK' = 'Unauthorized';

		verify(data.token, process.env.ACCESS_TOKEN_SECERT as string, (err) => {
			if (!err) {
				const decodedData = JSON.parse(atob(data.token.split('.')[1]));
				UserController.userInfo[socket.id] = {
					token: data.token,
					userKey: decodedData.userKey
				};
				authenticationResult = true;
				messageTitle = 'Authentication Success';
				type = 'success';
				status = 'OK';
			}
		});
		return new Response(
			{ value: authenticationResult },
			[{ title: messageTitle, type }],
			status
		);
	}

	@ActionAccess('User/fetchZones', 'Company')
	public async fetchZones_api({ userKey }) {
		const userInstance = new User();
		userInstance._key = userKey;
		try {
			return new Response(await userInstance.fetchZoneTree());
		} catch (e) {
			return e;
		}
	}

	@ActionAccess('User/logout', 'Company')
	public async logout_api({}, zone: ZoneObj, socket: Socket) {
		delete Controller.userInfo[socket.id];
		return new Response({}, [], 'OK');
	}
	@ActionAccess('User/getRoleInfo', 'Company')
	public async getRoleInfo_api({}, zone: ZoneObj, socket: Socket) {
		let company;
		try {
			company = await User.getDB()
				.collection('Company')
				.document(zone.key);
		} catch (e) {}
		if (!company || !company.zoneAccess)
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Company not found',
						type: 'error'
					}
				],
				'Not Found'
			);
		const userInfo = this.getUserInfo(socket);
		const role = company.zoneAccess['User/' + userInfo.userKey];
		if (role) return new Response(role, [], 'OK');
		else
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'You have no role in this company',
						type: 'error'
					}
				],
				'Not Found'
			);
	}

	@ActionAccess('User/fetchZoneTree', 'Company')
	async fetchZoneTree_api({}, zone, socket: Socket) {
		const userKey = this.getUserInfo(socket).userKey;
		const user = new User();
		user._key = userKey;
		return new Response(await user.fetchZoneTree());
	}
}
