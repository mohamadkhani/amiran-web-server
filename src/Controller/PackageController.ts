import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import { Package } from '../Entity/Package';
import Response, { ResponseStatus } from '../Interface/Response';
import { nanoid } from 'nanoid';
import { Socket } from 'socket.io';
import RemoveRegListener from '../Model/Package/RemoveRegListener';
import ReadMDL, { FilterMDL } from '../Model/Entity/ReadMDL';
import UpdateMDL from '../Model/Entity/UpdateMDL';
import { Shift } from '../Entity/Shift';
import { db } from '../Base/Entity';
import { Item } from '../Entity/Item';
import GetLinePackages from '../Model/Package/GetLinePackages';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import { NULL_MDL } from './UserController';
import { aql } from 'arangojs';
import MoveToWarehouse from '../Model/Package/MoveToWarehouse';
import { WarehouseReception } from '../Entity/WarehouseReception';
import FindByKey from '../Model/FindByKey';
import FindPackageByItem from '../Model/Package/FindPackageByItem';
import { ArangoOperationsType } from '../Model/CustomValidators/ArangoOperations';
import PMPackageListMDL from '../Model/Package/PMPackageListMDL';

export default class PackageController extends DataController<Package> {
	models: Record<string, Model> = {
		create_api: Package,
		read_api: ReadMDL,
		update_api: UpdateMDL,
		addRegListener_api: NULL_MDL,
		removeRegListener_api: RemoveRegListener,
		getAPackage_api: FindByKey,
		findAPackageByItem_api: FindPackageByItem,
		getLinePackages_api: GetLinePackages,
		packageList_api: NULL_MDL,
		pmPackageList_api: PMPackageListMDL,
		pmLabel_api: FindByKey,
		findForWarehouseReception_api: FindByKey,
		findForWarehouseChecking_api: FindByKey,
		moveToWarehouse_api: MoveToWarehouse,
		label_api: FindByKey,
		findFillingStored_api: FindByKey,
		findFilling_api: FindByKey,
		deleteFilling_api: FindByKey
	};

	private static packageCreationListeners: Record<
		string,
		(pkgId: string) => void
	> = {};

	defaultEntity = new Package();

	constructor() {
		super();
	}

	@ActionAccess('Package/read', 'Shift')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}

	@ActionAccess('Package/deleteFilling', 'ProductionLine')
	async deleteFilling_api({ _key }, zone: ZoneObj) {
		try {
			const trx = await Package.getDB().beginTransaction({
				write: ['Package']
			});
			const step1Result: { success: boolean } = await trx.step(
				async () => {
					try {
						const cursor = await Package.getDB().query(
							aql`
								FOR p IN Package
								FILTER p._key ==@pKey AND p.status == "filling" AND LENGTH(p.items) == 0
								AND DATE_NOW() - p.createdAt > 600000
								FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @shiftKey AND !CURRENT.l]) > 0
								REMOVE {_key:p._key} IN Package
							`.query,
							{
								pKey: _key,
								shiftKey: zone.key
							}
						);
						const pkg = await cursor.all();

						if (cursor.extra.stats.writesExecuted === 1) {
							return { success: true };
						} else {
							return { success: false };
						}
					} catch (e) {
						return { success: false, e };
					}
				}
			);

			if (!step1Result.success) {
				trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Fobidden',
							content:
								'Only filling packages with zero items and with an age more than 10 minutes can be removed from current shift',
							type: 'error'
						}
					],
					'Forbidden'
				);
			} else {
				trx.commit();
				return new Response(
					{},
					[{ title: 'Success', type: 'success' }],
					'OK'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Package/packageList', 'Shift')
	async packageList_api(
		{ pageSize, page }: { pageSize: number; page: number },
		zone: ZoneObj
	) {
		const query = aql`
		LET items = (FOR p IN Package
			FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			SORT p._key DESC
			LIMIT @f, @l
			RETURN p)
		Let totalCount = LENGTH(
			FOR p IN Package
			FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			RETURN p
		)
		return {items, totalCount}
		`.query;
		const result: { items: Package[]; totalCount: number }[] =
			await Shift.runAQL(query, {
				zoneKey: zone.key,
				f: (page - 1) * pageSize,
				l: pageSize
			});
		return new Response(result[0]);
	}
	@ActionAccess('Package/label', 'Shift')
	async label_api(data: { _key: string }, zone: ZoneObj, socket: Socket) {
		// const shift = db.collection(zone.name).document(zone.key);
		// if(!shift) return new Response({},[{title: '', type:'error'}],'Forbidden')
		const filter = [
			new FilterMDL('==', '_key', data._key),
			new FilterMDL('==', 'status', 'filled')
		];
		const pkgIntsance = new Package();
		let pkgList: { totalCount: number; items: Package[] } = {
			totalCount: 0,
			items: []
		};
		try {
			pkgList = await pkgIntsance.read({ filter }, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						content: 'Package must be in "filled" status',
						type: 'error'
					}
				],
				'Forbidden',
				[e]
			);
		}
		if (!pkgList || pkgList.totalCount === 0)
			return new Response(
				{},
				[{ title: 'NOT FILLED OR ALREADY LEABELED', type: 'error' }],
				'Not Found'
			);

		try {
			const pkg: Package = pkgList.items[0];
			const result = await pkgIntsance.update({
				key: data._key,
				data: { status: 'labeled' }
			});

			if (result.after)
				return new Response(
					{},
					[{ title: 'Labeled', type: 'success' }],
					'OK'
				);
			else
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Could not set the package label',
							type: 'error'
						}
					],
					'Not Found'
				);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Could not set the package label',
						type: 'error'
					}
				],
				'Not Found',
				[e]
			);
		}
	}

	@ActionAccess('Package/pmPackageList', 'ProductionLine')
	async pmPackageList_api(
		{
			pageSize,
			page,
			shiftKey,
			status
		}: { pageSize: number; page: number; shiftKey: string; status: string },
		zone: ZoneObj
	) {
		pageSize = pageSize || 10;
		page = page || 1;
		try {
			let result: { items: Package[]; totalCount: number }[] = [];
			if (status) {
				result = await Package.runAQL(
					aql`
					LET items = (FOR p IN Package
						FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey])>0
						FILTER p.status == @status AND @shiftKey IN p.shiftCode
						SORT p._key DESC
						LIMIT @f, @l
						RETURN p)
					Let totalCount = LENGTH(
						FOR p IN Package
						FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey])>0
						FILTER p.status == @status AND @shiftKey IN p.shiftCode
						RETURN p
					)
					return {items, totalCount}
					`.query,
					{
						zoneKey: zone.key,
						status,
						shiftKey,
						f: (page - 1) * pageSize,
						l: pageSize
					}
				);
			} else {
				result = await Package.runAQL(
					aql`
					LET items = (FOR p IN Package
						FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey])>0
						FILTER @shiftKey IN p.shiftCode
						SORT p._key DESC
						LIMIT @f, @l
						RETURN p)
					Let totalCount = LENGTH(
						FOR p IN Package
						FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey])>0
						FILTER @shiftKey IN p.shiftCode
						RETURN p
					)
					return {items, totalCount}
					`.query,
					{
						zoneKey: zone.key,
						shiftKey,
						f: (page - 1) * pageSize,
						l: pageSize
					}
				);
			}
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Package/pmLabel', 'ProductionLine')
	async pmLabel_api(data: { _key: string }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Package.runAQL(
				aql`
				FOR p IN Package
				FILTER p._key == @pKey AND (p.status == "filled" OR p.status == "labeled")
				FILTER LENGTH(p.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				RETURN p
				`.query,
				{
					pKey: data._key,
					zoneKey: zone.key
				}
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content:
								'Package must be in "filled" or "labeled" status in current production line',
							type: 'error'
						}
					],
					'Forbidden'
				);
			const pStatus = result[0]?.status;
			const updateResult = await Package.runAQL(
				aql`
				UPDATE @pKey WITH {status: @pStatus} IN Package
				RETURN {before: OLD, after: NEW}
			`.query,
				{
					pKey: data._key,
					pStatus: pStatus === 'filled' ? 'labeled' : 'filled'
				}
			);

			if (!updateResult)
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Could not label the package',
							type: 'error'
						}
					],
					'Bad Request'
				);
			else
				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Package labeled',
							type: 'success'
						}
					],
					'OK'
				);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	/*@ActionAccess('Package/getLinePackages', 'Company')
	async getLinePackages_api(
		data: { shiftCode: string; page: number; pageSize: number },
		zone: ZoneObj,
		socket: Socket
	) {
		//FIXME Access controll should be added
		const shiftInstance = new Shift();
		const filter = [new FilterMDL('==', '_key', data.shiftCode)];
		const shiftList = await shiftInstance.read(
			{
				filter,
				page: data.page,
				pageSize: data.pageSize
			},
			zone
		);
		if (shiftList.totalCount === 0)
			return new Response(
				{},
				[{ title: 'No Active Shift Found', type: 'error' }],
				'Not Found'
			);
		const shift: Shift = shiftList.items[0];
		const pkgInstance = new Package();
		const pkgFilter = [
			new FilterMDL<Package, string>('IN', 'shiftCode', shift._key)
		];
		const sort = [new SortMDL('_key', true)];
		const pkgList = await pkgInstance.read(
			{
				filter: pkgFilter,
				sort,
				page: data.page,
				pageSize: data.pageSize
			},
			zone
		);
		if (pkgList.totalCount === 0)
			return new Response(
				{},
				[
					{
						title: 'No Package Found',
						content: 'No package found for this shift',
						type: 'info'
					}
				],
				'Not Found'
			);

		return new Response(pkgList, [], 'OK');
	}*/

	@ActionAccess('Package/getAPackage', 'Company')
	async getAPackage_api(data: { _key: string }) {
		const query = `
		FOR u IN ${new Package()._collectionName}
		FILTER u._key == "${data._key}"
		RETURN MERGE(u, {items:DOCUMENT(${new Item()._collectionName}, u.items)})			
		`;
		const cursor = await db.query(query);
		const pkgList = await cursor.all();
		if (pkgList.length !== 0) {
			const pkg = pkgList[0];
			return new Response(pkg, [], 'OK');
		} else
			return new Response(
				{},
				[{ title: 'Package Not Found', type: 'error' }],
				'Not Found'
			);
	}

	@ActionAccess('Package/findAPackageByItem', 'Company')
	async findAPackageByItem_api(
		data: {
			_key: string;
			shiftCode: string;
		},
		zone: ZoneObj
	) {
		// -------------------------------------------------------
		const itemInstance = new Item();
		const filter = [
			new FilterMDL<Package, string>('==', '_key', data._key),
			new FilterMDL<Package, string>('==', 'shiftCode', data.shiftCode)
		];
		const itemList = await itemInstance.read({ filter }, zone);
		if (!itemList || itemList.totalCount === 0)
			return new Response(
				{},
				[{ title: 'Item Not Found', type: 'error' }],
				'Not Found'
			);
		const item: Item = itemList.items[0];
		if (!item || !item._key)
			return new Response(
				{},
				[{ title: 'Item Not Found', type: 'error' }],
				'Not Found'
			);
		// -------------------------------------------------------
		let pkgList = [];

		try {
			const query = `
				FOR u IN ${new Package()._collectionName}
				FILTER u._key == "${item.packageKey}"
				AND "${item._key}" IN u.items
				RETURN MERGE(u , {items: DOCUMENT("Item", u.items)})
			`;

			const cursor = await db.query(query);
			pkgList = await cursor.all();
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Package Not Found', type: 'error' }],
				'Not Found'
			);
		}
		if (!pkgList || pkgList.length === 0)
			return new Response(
				{},
				[{ title: 'Package Not Found', type: 'error' }],
				'Not Found'
			);

		const pkg = pkgList[0];

		return new Response(
			pkg,
			[{ title: 'Package Found', type: 'success' }],
			'OK'
		);
	}

	@ActionAccess('Package/addRegListener', 'Company')
	addRegListener_api(data: Package, zone: ZoneObj, socket: Socket) {
		const listnerId = nanoid();
		PackageController.packageCreationListeners[listnerId] = (pkgId) => {
			socket.emit(
				listnerId,
				new Response({ id: pkgId }, [
					{ title: 'A package created', type: 'info' }
				])
			);
		};
		socket.on('disconnect', () => {
			PackageController.removeListener(listnerId);
		});
		socket.on('error', () => {
			PackageController.removeListener(listnerId);
		});
		return new Response({ id: listnerId });
	}

	@ActionAccess('Package/removeRegListener', 'Company')
	removeRegListener_api(
		{ id }: { id: string },
		zone: ZoneObj,
		socket: Socket
	) {
		PackageController.removeListener(id);
		return new Response({});
	}

	static removeListener(key: string) {
		PackageController.packageCreationListeners = Object.fromEntries(
			Object.entries(PackageController.packageCreationListeners).filter(
				([k]) => k !== key
			)
		);
	}

	@ActionAccess('Package/findForWarehouseReception', 'Warehouse')
	async findForWarehouseReception_api(
		{ _key }: { _key: string },
		zone: ZoneObj,
		socket: Socket
	) {
		const result = await Package.runAQL(
			aql`
			For p IN Package
			FILTER p._key == @_key
			RETURN MERGE(p, {itemType:DOCUMENT('ItemType', p.itemType)})
		`.query,
			{ _key }
		);

		const pkg: Package = result[0];
		if (pkg) {
			if (pkg.status === 'stored')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: 'Package is already stored',
							type: 'error'
						}
					],
					'Forbidden'
				);
			// if (pkg.status !== 'labeled' && pkg.status !== 'filling') {
			if (pkg.status !== 'labeled') {
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content:
								'Package is not in "labeled" or "filling". Package status is "' +
								pkg.status +
								'"',
							type: 'error'
						}
					],
					'Forbidden'
				);
			}

			if (
				pkg?.zones?.Warehouse?.reduce<boolean>( //TODO
					(acc, wh) => acc || (!wh.l && wh.k == zone.key),
					false
				)
			) {
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: 'Package is already in a warehouse',
							type: 'error'
						}
					],
					'Forbidden'
				);
			}

			return new Response(pkg, [], 'OK');
		} else {
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Forbidden'
			);
		}
	}

	@ActionAccess('Package/findForWarehouseChecking', 'Warehouse')
	async findForWarehouseChecking_api({ _key }, zone) {
		try {
			const result =
				(await Package.runAQL(
					aql`
				FOR p IN Package
				FILTER p._key == @pKey
				FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @wKey AND !CURRENT.l]) > 0
				RETURN p
			`.query,
					{
						pKey: _key,
						wKey: zone.key
					}
				)) || [];
			if (result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							type: 'error',
							content:
								'No package found for this code in this warehouse'
						}
					],
					'Not Found'
				);
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Package/findFillingStored', 'Warehouse')
	async findFillingStored_api({ _key }, zone: ZoneObj, socket) {
		let result;
		try {
			result = await Package.runAQL(
				aql`
				FOR p IN Package
				FILTER p._key == @_key AND p.status == 'fillingStored'
				FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				RETURN MERGE(p, {itemType: DOCUMENT('ItemType', p.itemType), shift:DOCUMENT('Shift', p.shiftCode[0]) })
			`.query,
				{ _key, zoneKey: zone.key }
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
		if (!result)
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);

		return new Response(result);
	}

	@ActionAccess('Package/findFilling', 'Warehouse')
	async findFilling_api({ _key }, zone: ZoneObj, socket) {
		const pkgInstance = new Package();
		const filter = [new FilterMDL('IN', 'items', _key)];
		let pkgList = [];
		try {
			pkgList = await Package.runAQL(
				aql`
				FOR p in Package
				FILTER p_.key == @_key
				RETURN MERGE(p, {itemType: DOCUMENT('ItemType', p.itemType)})
			`.query,
				{ _key }
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
		if (!pkgList || pkgList.length === 0)
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);
		const pkg: Package = pkgList[0];

		if (pkg.status !== 'filling')
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Package is not in Filling status',
						type: 'error'
					}
				],
				'Not Found'
			);
		if (
			!pkg.zones['Shift'] ||
			pkg.zones['Shift'].filter((item) => !item.l).length === 0
		)
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Package is not in a Shift',
						type: 'error'
					}
				],
				'Not Found'
			);

		return new Response(pkg);
	}

	// This is a very important action that can change the zone of
	// packages and alternate their previous zones
	@ActionAccess('Package/moveToWarehouse', 'Company')
	async moveToWarehouse_api(
		{
			packages,
			description,
			force,
			warehouseZoneKey
		}: {
			packages: string[];
			description: string;
			force: boolean;
			warehouseZoneKey: string;
		},
		zone,
		socket: Socket
	) {
		const userInfo = this.getUserInfo(socket);
		let notLabeledPackageCount;
		try {
			const wh = Package.runAQL(
				aql`
				FOR w IN Warehouse
				FILTER w._key == @warehouseZoneKey AND w.zoneAccess[@userId] == 'Role/warehouseKeeper'
				return w
			`.query,
				{ warehouseZoneKey, userId: 'User/' + userInfo.userKey }
			);

			if (!wh)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Warehouse not found',
							type: 'error'
						}
					],
					'Not Found'
				);
			notLabeledPackageCount = await Package.runAQL(
				aql`
					FOR p IN Package
					FILTER p._key IN @packages AND p.status != 'labeled'
					RETURN p
				`.query,
				{ packages }
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error'
					}
				],
				'Forbidden',
				[e]
			);
		}

		if (notLabeledPackageCount.length > 1)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'Packages must be labeled before warehouse import.'
					}
				],
				'Forbidden'
			);

		let pkgList;

		try {
			pkgList = await Package.runAQL(
				aql`
				FOR p IN Package
				FILTER p._key IN @packages 
				RETURN p
			`.query,
				{ packages }
			);
			if (pkgList.length < packages.length)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: 'Problem in fetching your packages',
							type: 'error'
						}
					],
					'Forbidden'
				);
		} catch (e) {
			return new Response({}, [], 'Bad Request');
		}

		const totalPackageRecords = pkgList.length;
		const totalItemRecords = pkgList.reduce(
			(acc, pkg) => (acc = acc + pkg.items.length),
			0
		);

		try {
			const trx = await Package.getDB().beginTransaction({
				write: ['Package', 'Item', 'WarehouseReception']
			});

			const transactionResult: {
				result: boolean;
				msg: {
					title: string;
					content: string;
					type: 'error' | 'success';
					respType: ResponseStatus;
				};
			} = await trx.step(async () => {
				const cursor = await Package.getDB().query(
					aql`
					FOR p IN Package
					FILTER p._key IN @packages
					FILTER LENGTH(p.zones.ProductionLine[* FILTER !CURRENT.l])>0
					FILTER LENGTH(p.zones.Shift[* FILTER !CURRENT.l])>0

					UPDATE p._key WITH {
						zones: {
							Shift: p.zones.Shift[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
							ProductionLine: p.zones.ProductionLine[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
							Warehouse: [{k: @zoneKey, e: DATE_NOW()}]
						},
						status: 'stored'
					} IN Package

					FOR i IN Item
					FILTER i._key IN p.items
						FILTER LENGTH(i.zones.ProductionLine[* FILTER !CURRENT.l])>0
						FILTER LENGTH(i.zones.Shift[* FILTER !CURRENT.l])>0
						UPDATE i._key WITH {
							zones: {
								Shift: i.zones.Shift[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
								ProductionLine: i.zones.ProductionLine[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
								Warehouse: [{k: @zoneKey, e: DATE_NOW()}]
							}
						} IN Item
					`.query,
					{
						packages,
						zoneKey: warehouseZoneKey
					}
				);
				await cursor.all();
				if (
					totalPackageRecords + totalItemRecords !=
					cursor.extra.stats.writesExecuted
				) {
					return {
						result: false,
						msg: {
							title: 'Bad Request',
							content: 'Your are importing wrong packages',
							type: 'error',
							respType: 'Bad Request'
						}
					};
				} else {
					const warehouseReceptionInstance = new WarehouseReception();
					warehouseReceptionInstance.projectFrom({
						packages,
						description,
						packageCount: totalPackageRecords,
						itemCount: totalItemRecords,
						warehouseKey: warehouseZoneKey,
						forced: force
					});
					let result;
					try {
						result = await warehouseReceptionInstance.create(
							userInfo,
							{ name: 'Warehouse', key: warehouseZoneKey }
						);
					} catch (e) {
						return {
							result: false,
							msg: {
								title: 'Bad Request',
								type: 'error',
								content: 'Could not store the request',
								respType: 'Bad Request'
							}
						};
					}
					if (!result)
						return {
							result: false,
							msg: {
								title: 'Bad Request',
								content: 'Warehouse Reception not saved',
								type: 'error',
								respType: 'Bad Request'
							}
						};
					return {
						result: true,
						msg: {
							title: 'Success',
							type: 'success',
							content:
								totalPackageRecords +
								' packages containing ' +
								totalItemRecords +
								' items successfully moved to the Warehouse.',
							respType: 'OK'
						}
					};
				}
			});
			if (transactionResult.result) {
				await trx.commit();
			} else {
				await trx.abort();
			}
			return new Response(
				{},
				[
					{
						title: transactionResult.msg.title,
						content: transactionResult.msg.content,
						type: transactionResult.msg.type
					}
				],
				transactionResult.msg.respType
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
