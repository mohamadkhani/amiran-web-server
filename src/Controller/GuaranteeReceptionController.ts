import { aql } from 'arangojs';
import { ActionAccess } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { GuaranteeReception } from '../Entity/GuaranteeReception';
import { Item } from '../Entity/Item';
import Response from '../Interface/Response';
import ReadMDL from '../Model/Entity/ReadMDL';

export default class GuaranteeReceptionController extends DataController<GuaranteeReception> {
	protected defaultEntity = new GuaranteeReception();
	models: Record<string, Model> = {
		readd_api: ReadMDL,
		create_api: GuaranteeReception
	};

	@ActionAccess('GuaranteeReception/read', 'Company')
	async read_api(data, zone) {
		return super.read_api(data, zone);
	}

	@ActionAccess('GuaranteeReception/create', 'Company')
	async create_api(data, zone, socket) {
		try {
			const trx = await GuaranteeReception.getDB().beginTransaction({
				write: ['Item', 'GuaranteeReception']
			});
			const step1Result: { success: boolean; data?: Item[] } =
				await trx.step(async () => {
					const cursor = await Item.getDB().query(
						aql`
                            FOR i IN Item
                            FILTER i._key IN @items
                            FILTER LENGTH(i.zones.Sold[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
                            UPDATE i._key WITH {
                                zones: {
                                    Sold: i.zones.Sold[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
                                    Guarantee: [{k: @zoneKey, e: DATE_NOW()}]
                                }
                            } IN Item OPTIONS { mergeObjects: true, keepNull: false }		
                            `.query,
						{
							items: data.items,
							zoneKey: 'one'
						}
					);

					const itm = await cursor.all();
					if (
						cursor.extra.stats.writesExecuted === data.items.length
					) {
						super.create_api(data, zone, socket);
						return { success: true, data: itm };
					} else {
						return { success: false };
					}
				});
			if (step1Result.success) {
				trx.commit();
				return new Response(
					{},
					[
						{
							title: 'Success',
							content:
								data.items?.length +
								' items returned for guarantee',
							type: 'success'
						}
					],
					'OK'
				);
			} else {
				trx.abort();
				return new Response(
					{},
					[{ title: 'Bad Request', type: 'error' }],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
