import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import { ItemType } from '../Entity/ItemType';
import { RemoveMDL } from '../Model/Entity/RemoveMDL';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import UpdatePriceMDL from '../Model/ItemType/UpdatePriceMDL';
import { aql } from 'arangojs';
import Response from '../Interface/Response';
import moment from 'moment-jalaali';
import GetMonthlyStatMDL from '../Model/ItemType/GetMonthlyStatMDL';
import GetDailyStatMDL from '../Model/ItemType/GetDailyStatMDL';
import FindByKey from '../Model/FindByKey';

export default class ItemTypeController extends DataController<ItemType> {
	models: Record<string, Model> = {
		create_api: ItemType,
		remove: RemoveMDL,
		setPrice_api: UpdatePriceMDL,
		getSalesStat_api: GetMonthlyStatMDL,
		getProductionStat_api: GetMonthlyStatMDL,
		getProductionDailyStat_api: GetDailyStatMDL,
		customersPurchase_api: FindByKey
	};

	defaultEntity = new ItemType();

	constructor() {
		super();
	}
	@ActionAccess('ItemType/read', 'Company')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}

	@ActionAccess('ItemType/create', 'Company')
	async create_api(data, zone: ZoneObj, socket) {
		return super.create_api(data, zone, socket);
	}

	@ActionAccess('ItemType/update', 'Company')
	async update_api(data) {
		return super.update_api(data);
	}

	@ActionAccess('ItemType/remove', 'Company')
	async remove_api(data, zone: ZoneObj, socket) {
		return super.remove_api(data, zone, socket);
	}

	@ActionAccess('ItemType/setPrice', 'Company')
	async setPrice_api({ _key, price }, zone: ZoneObj, socket) {
		let itemTypes: ItemType[];
		try {
			//FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
			itemTypes = await ItemType.runAQL(
				aql`
					For i IN ItemType
					FILTER i._key == @iKey 
					RETURN i
			`.query,
				{ iKey: _key }
			);
			if (!itemTypes || !itemTypes[0]._key)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'ItemType not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'ItemType not Found',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}

		try {
			ItemType.runAQL(
				aql`
				UPDATE @iKey WITH {price: @price} IN ItemType
				RETURN {before: OLD,after:NEW }
			`.query,
				{ iKey: itemTypes[0]._key, price }
			);
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'ItemType price updated',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'ItemType not updated',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
	}

	public static monthIntervals(
		year: number
	): { month: number; start: number; end: number }[] {
		const intervals: { month: number; start: number; end: number }[] = [];

		for (let month = 1; month <= 12; month++) {
			const m = moment(year + '-' + month, 'jYYYY-jM');

			var start = m.startOf('jMonth').valueOf();
			var end = m.endOf('jMonth').valueOf();
			intervals.push({ month: month, start, end });
		}

		return intervals;
	}
	public static daysIntervals(
		year: number,
		month: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
	): { day: number; start: number; end: number }[] {
		const intervals: { day: number; start: number; end: number }[] = [];

		const m = moment(year + '-' + month, 'jYYYY-jM');

		for (let day = 1; day <= m.daysInMonth(); day++) {
			m.jDate(day);

			var start = m.startOf('day').valueOf();
			var end = m.endOf('day').valueOf();
			intervals.push({ day: day, start, end });
		}

		return intervals;
	}
	public static getDayInBetween(
		startDate: number,
		endDate: number
	): string[] {
		var dates = [];

		var currDate = moment(startDate).startOf('day');
		var lastDate = moment(endDate).startOf('day');

		while (currDate.add(1, 'days').diff(lastDate) < 0) {
			dates.push(currDate.clone().toDate());
		}

		return dates;
	}

	@ActionAccess('ItemType/getSalesStat', 'Company')
	async getSalesStat_api({ rgx, year }, zone) {
		const intervals = ItemTypeController.monthIntervals(year);
		try {
			const result = await ItemType.runAQL(
				aql`
				for int in @intervals
				  let itemCount = LENGTH(for i in Item
				filter REGEX_TEST(i, @rgx)
				  filter length(i.zones.Sold[* filter CURRENT.k AND CURRENT.e < int.end AND CURRENT.e > int.start])>0
				  return i)
				  return {month: int.month, count: itemCount}
			`.query,
				{ intervals, rgx }
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Bad Request', type: 'error' }],
					'Bad Request'
				);
			else return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ItemType/getProductionStat', 'Company')
	async getProductionStat_api({ rgx, year }, zone) {
		const intervals = ItemTypeController.monthIntervals(year);
		try {
			const result = await ItemType.runAQL(
				aql`
				for int in @intervals
				  
				  let itemCount = LENGTH(for i in Item
				  filter regex_test(i._key,@rgx)
				  filter length(i.zones.ProductionLine[* filter CURRENT.k AND CURRENT.e < int.end AND CURRENT.e > int.start])>0
				  return i)
				  return {month: int.month, count: itemCount}
			`.query,
				{ intervals, rgx }
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Bad Request', type: 'error' }],
					'Bad Request'
				);
			else return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ItemType/getProductionDailyStat', 'Company')
	async getProductionDailyStat_api({ rgx, year, month }, zone) {
		const intervals = ItemTypeController.daysIntervals(year, month);
		try {
			const result = await ItemType.runAQL(
				aql`
				for int in @intervals
				  
				  let itemCount = LENGTH(for i in Item
				  filter regex_test(i._key,@rgx)
				  filter length(i.zones.ProductionLine[* filter CURRENT.k AND CURRENT.e < int.end AND CURRENT.e > int.start])>0
				  return i)
				  return {day: int.day, count: itemCount}
			`.query,
				{ intervals, rgx }
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Bad Request', type: 'error' }],
					'Bad Request'
				);
			else return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('ItemType/customersPurchase', 'Company')
	async customersPurchase_api({ _key }, zone: ZoneObj) {
		try {
			const result =
				(await ItemType.runAQL(
					aql`
					LET itemType = DOCUMENT( 'ItemType',@itemTypeKey)

					FOR c IN Customer
										
					LET orders = (FOR o IN Order
						FILTER o.customer == c._key
						RETURN o._id
						)
						
					LET count = LENGTH(
						FOR i IN Item 
						FILTER i.order IN orders
						AND REGEX_TEST(i._key, itemType.itemRegex)
						FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
						RETURN i)
					RETURN {customer: c.fullname, count}
			`.query,
					{
						itemTypeKey: _key,
						cKey: zone.key
					}
				)) || [];
			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
