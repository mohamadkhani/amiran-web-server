import { aql } from 'arangojs';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { ProductionLine } from '../Entity/ProductionLine';
import { Shift } from '../Entity/Shift';
import Response from '../Interface/Response';
import FindByKey from '../Model/FindByKey';
import { EstimateMDL } from '../Model/PL/EstimateMDL';
import { ProductionGraphMDL } from '../Model/PL/ProdcutionGraphMDL';
import { NULL_MDL } from './UserController';

export default class ProductionLineController extends DataController<ProductionLine> {
	protected defaultEntity = new ProductionLine();
	models: Record<string, Model> = {
		getCurrentShift_api: NULL_MDL,
		estimate_api: EstimateMDL,
		scanmansStat_api: FindByKey,
		gaugeStat_api: FindByKey,
		productionGraph_api: ProductionGraphMDL,
		penaltimateSameKindShift_api: FindByKey
	};

	@ActionAccess('ProductionLine/getCurrentShift', 'ProductionLine')
	async getCurrentShift_api(data: null, zone: ZoneObj) {
		const result = await Shift.runAQL(
			aql`
			FOR s IN Shift
				FILTER LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
				FILTER s.status == 'producing' OR s.status == 'paused'
				RETURN s
			`.query,
			{ zoneKey: zone.key }
		);
		if (result.length > 1) {
			return new Response({}, [
				{
					title: 'Multiple Active Production Line',
					content:
						'There is more than one active shift in this production line.',
					type: 'error'
				}
			]);
		}
		if (result.length == 1) {
			return new Response({ currentShift: result[0] }, [], 'OK');
		} else {
			return new Response(
				{},
				[{ title: 'No Shift Found', type: 'error' }],
				'Not Found'
			);
		}
	}

	@ActionAccess('ProductionLine/estimate', 'ProductionLine')
	async estimate_api({ shiftKey, period }, zone) {
		period = period * 60 * 1000;
		try {
			const result = await ProductionLine.runAQL(
				aql`
			
			let item = length(
				for i in Item
				filter i.shiftCode == @shiftKey AND i.createdAt < Date_now()  AND i.createdAt >  Date_now() - @period
				return i
				)
				let package = length(
				for p in Package
				filter @shiftKey in p.shiftCode AND p.createdAt < Date_now()  AND p.createdAt >  Date_now() - @period
				return p
				)
				
				return {package , item}
				`.query,
				{
					shiftKey,
					period
				}
			);
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ProductionLine/scanmansStat', 'ProductionLine')
	async scanmansStat_api({ _key }, zone) {
		try {
			const result = await ProductionLine.runAQL(
				aql`
			
				let workers = (for i in Item
					filter i.shiftCode == @shiftKey
					COLLECT user = i.createdBy 
					return user
					)
					
					for u In workers
						let itemCount = length(for ii in Item 
							filter ii.createdBy == u and ii.shiftCode == @shiftKey
							return ii)
						let packageCount = length(for pp in Package 
							filter pp.createdBy == u and @shiftKey IN pp.shiftCode
							return pp)
						let pSpeed = length(for pp in Package 
							filter pp.createdBy == u and pp.createdAt > Date_now() - 3600000 and @shiftKey IN pp.shiftCode
							return pp)
						let iSpeed = length(for ii in Item 
							filter ii.createdBy == u and ii.createdAt > Date_now() - 3600000 and ii.shiftCode == @shiftKey
							return ii)
					return {user:DOCUMENT('User', u).fullname,itemCount, packageCount, pSpeed, iSpeed}
					
				`.query,
				{
					shiftKey: _key
				}
			);
			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ProductionLine/gaugeStat', 'ProductionLine')
	async gaugeStat_api({ _key }, zone) {
		try {
			const result = (await ProductionLine.runAQL(
				aql`
			
				let lph = length(for i in Item
					filter i.shiftCode == @shiftKey
					and i.createdAt > (date_now() - 1800000)
					return i 
					) * 2
				let pph = length(for p in Package
					filter @shiftKey IN p.shiftCode
					and p.createdAt > (date_now() - 1800000)
					return p
					) * 2
					return {lph, pph}
					
				`.query,
				{
					shiftKey: _key
				}
			)) || [{ lph: 0, pph: 0 }];

			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ProductionLine/productionGraph', 'ProductionLine')
	async productionGraph_api(
		{
			shiftKey,
			intervals
		}: { shiftKey: string; intervals: { start: string; end: string }[] },
		zone
	) {
		try {
			const result =
				(await ProductionLine.runAQL(
					aql`
					FOR interval IN @intervals
						LET count = LENGTH(
							FOR i IN Item
							FILTER i.shiftCode == @shiftKey AND i.createdAt > interval.start AND i.createdAt < interval.end
							RETURN i
						)*2
					RETURN {count, interval }
				`.query,
					{
						shiftKey,
						intervals
					}
				)) || [];

			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('ProductionLine/penaltimateSameKindShift', 'ProductionLine')
	async penaltimateSameKindShift_api({ _key }, zone) {
		try {
			const result = (await Shift.runAQL(
				aql`
				LET currentShift = DOCUMENT('Shift', @shiftKey)
				FOR s IN Shift
				FILTER s.itemType._key == currentShift.itemType._key AND s._key != currentShift._key AND s.createdAt < currentShift.createdAt
				FILTER LENGTH(s.zones.ProductionLine[* FILTER CURRENT.k == @plKey])>0
				SORT s.createdAt DESC
				LET itemCount = LENGTH(FOR i IN Item
					FILTER i.shiftCode == s._key
					RETURN i
					)
				LIMIT 0,1
				RETURN {itemCount}
			`.query,
				{
					shiftKey: _key,
					plKey: zone.key
				}
			)) || [{ itemCount: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
