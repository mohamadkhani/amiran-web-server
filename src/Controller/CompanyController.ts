import { aql } from 'arangojs';
import { ActionAccess } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { Company } from '../Entity/Company';
import Response from '../Interface/Response';
import QuarantineMDL from '../Model/Company/QuarantineMDL';
import { NULL_MDL } from './UserController';
import got from 'got';
export default class CompanyController extends DataController<Company> {
	protected defaultEntity = new Company();
	models: Record<string, Model> = {
		getStatus_api: NULL_MDL,
		paralyse_api: NULL_MDL,
		mobilize_api: NULL_MDL,
		getSMSCredit_api: NULL_MDL,
		quarantine_api: QuarantineMDL
	};

	@ActionAccess('Company/getZoneAccessedPeople', 'Company')
	async read_api(data, zone) {
		try {
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('Company/getStatus', 'Company')
	async getStatus_api(data, zone) {
		try {
			const result = (await Company.runAQL(
				aql`
				FOR c IN Company
				FILTER c._key == @cKey
				RETURN {status: c.status}
			`.query,
				{
					cKey: zone.key
				}
			)) || [{ status: null }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}
	@ActionAccess('Company/getSMSCredit', 'Company')
	async getSMSCredit_api({}, zone) {
		try {
			const response = await got(
				'https://api.kavenegar.com/v1/' +
					process.env.SMSAPIKEY +
					'/account/info.json'
			).json();
			return new Response(response['entries']);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Company/paralyse', 'Company')
	async paralyse_api(data, zone) {
		try {
			const result = await Company.runAQL(
				aql`
						FOR c IN Company
						FILTER c._key == @cKey AND LENGTH(ATTRIBUTES(c.zoneAccess)) > 1 AND c.status == 'mobilized' 
						LET newZoneAccess = (FOR id IN ATTRIBUTES(c.zoneAccess) FILTER c.zoneAccess[id] == 'Role/admin' RETURN id)[0]
						UPDATE c._key WITH {zoneAccess: {[newZoneAccess]:'Role/admin'}, zoneAccessBackup: c.zoneAccess, status:'paralysed'} IN Company OPTIONS {mergeObjects: false}
						RETURN NEW
				`.query,
				{
					cKey: zone.key
				}
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content:
								'Everyone is already paralysed or you might not have access',
							type: 'error'
						}
					],
					'Forbidden'
				);
			return new Response(
				{},
				[
					{
						title: 'Everyone Expect you is paralysed 😈😜',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {}
	}

	@ActionAccess('Company/mobilize', 'Company')
	async mobilize_api({ paralyse }, zone) {
		try {
			const result = await Company.runAQL(
				aql`
					FOR c IN Company
					FILTER c._key == @cKey AND c.status == 'paralysed'
					UPDATE c._key WITH {zoneAccess: c.zoneAccessBackup, status: 'mobilized'} IN Company OPTIONS {mergeObjects: false}
					RETURN NEW
			`.query,
				{
					cKey: zone.key
				}
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Everyone is already paralysed',
							type: 'error'
						}
					],
					'Not Found'
				);
			return new Response({}, [
				{
					title: 'Staff are now mobilized 👍',
					type: 'success'
				}
			]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Company/quarantine', 'Company')
	async quarantine_api({ staff }, zone) {
		try {
			const result = await Company.runAQL(
				aql`
					FOR c IN Company
					FILTER c._key == 'amiran' AND c.status == 'mobilized'
					UPDATE c._key WITH {zoneAccess: {admin:zoneAccessBackup}, status: 'quarantined'} OPTIONS {mergeObjects: false}
					RETURN NEW
			`.query,
				{
					cKey: zone.key
				}
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Everyone is already paralysed',
							type: 'error'
						}
					],
					'Not Found'
				);
			return new Response({}, [
				{
					title: 'Staff are now mobilized 👍',
					type: 'success'
				}
			]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	// @ActionAccess('Company/quarantine', 'Company')
	// async quarantine_api({ staff }, zone) {
	// 	try {
	// 		const result = await Company.runAQL(
	// 			aql`
	// 				FOR c IN Company
	// 				FILTER c._key == 'amiran' AND c.status == 'mobilized'
	// 				UPDATE c._key WITH {zoneAccess: {admin:zoneAccessBackup}, status: 'quarantined'} OPTIONS {mergeObjects: false}
	// 				RETURN NEW
	// 		`.query,
	// 			{
	// 				cKey: zone.key
	// 			}
	// 		);

	// 		if (!result || result.length === 0)
	// 			return new Response(
	// 				{},
	// 				[
	// 					{
	// 						title: 'Not Found',
	// 						content: 'Everyone is already paralysed',
	// 						type: 'error'
	// 					}
	// 				],
	// 				'Not Found'
	// 			);
	// 		return new Response({}, [
	// 			{
	// 				title: 'Staff are now mobilized 👍',
	// 				type: 'success'
	// 			}
	// 		]);
	// 	} catch (e) {
	// 		return new Response(
	// 			{},
	// 			[{ title: 'Bad Request', type: 'error' }],
	// 			'Bad Request'
	// 		);
	// 	}
	// }
}
