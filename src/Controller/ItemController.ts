import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import { Item } from '../Entity/Item';
import { Socket } from 'socket.io';
import { Package } from '../Entity/Package';
import ReadMDL, { FilterMDL, SortMDL } from '../Model/Entity/ReadMDL';
import moment from 'moment-jalaali';
import { Shift } from '../Entity/Shift';
import FindPackage from '../Model/Item/FindPackage';
import { db, F } from '../Base/Entity';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import { ItemSpec } from '../Entity/ItemSpec';
import { aql } from 'arangojs';
import Response from '../Interface/Response';
import FindByKey from '../Model/FindByKey';
import { ItemType } from '../Entity/ItemType';
import SetBreakReasonMDL from '../Model/Item/SetBreakReasonMDL';
import { NULL_MDL } from './UserController';

const decadeYearsCodes = {
	'0': 'B',
	'1': 'C',
	'2': 'D',
	'3': 'E',
	'4': 'F',
	'5': 'G',
	'6': 'H',
	'7': 'I',
	'8': 'J',
	'9': 'K'
};
const breakdownReasons = [
	'IC',
	'Chip',
	'Inductor',
	'Capacitor',
	'DiodBridge',
	'Solder',
	'Cap',
	'Varistor',
	'Unknown'
];
export default class ItemController extends DataController<Item> {
	models: Record<string, Model> = {
		create_api: Item,
		delete_api: Item,
		findPackage_api: FindPackage,
		findTheGreatestItemCode_api: FindByKey,
		find_api: FindByKey,
		findForGuarantee_api: FindByKey,
		findForBreakdownReason_api: FindByKey,
		getBrokenDowns_api: ReadMDL,
		setBreakReason_api: SetBreakReasonMDL,
		purchasedProducedStat_api: NULL_MDL
	};

	defaultEntity = new Item();

	constructor() {
		super();
	}

	@ActionAccess('Item/findTheGreatestItemCode', 'Company')
	async findTheGreatestItemCode_api({ _key }, zone: ZoneObj, socket: Socket) {
		try {
			const code = await Shift.runAQL(
				aql`
					let items =(
							FOR i IN Item
							FILTER REGEX_TEST(i._key, @regex)
							FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @zoneKey])>0
							return right(i._key, 8)
						)
					return MAX(items)
			`.query,
				{ regex: _key + '[0-9]{8}', zoneKey: zone.key }
			);
			if (!code || code.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content:
								'Could not find the latest code for this type',
							type: 'warning'
						}
					],
					'Not Found'
				);
			return new Response({ code });
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Not Found', type: 'error' }],
				'Not Found'
			);
		}
	}

	async checkTheShiftLine(userKey: string, zone: ZoneObj): Promise<string> {
		const shiftInstance = new Shift();
		const shiftFilter = [
			new FilterMDL('==', 'status', 'producing'),
			new FilterMDL('IN', 'workers', userKey)
		];
		const sort = [new SortMDL('_key', true)];
		const shifts = await shiftInstance.read(
			{
				filter: shiftFilter,
				sort,
				page: 1,
				pageSize: 1
			},
			zone
		);

		const shift: Shift = shifts?.items[0];
		return shift ? shift._key : null;
	}

	async generateThePackageCode(
		shiftInstance: Shift,
		zone: ZoneObj
	): Promise<null | string> {
		const shift: Shift = shiftInstance;

		const type = shift.itemType?.values.find(
			(item) => item.itemSpec.title === 'Type'
		).value.key;

		const color = shift.itemType?.values.find(
			(item) => item.itemSpec.title == 'Color'
		)?.value.key;

		const yearCode = decadeYearsCodes[moment().format('jYY').charAt(1)];

		let power = shift.itemType?.values.find(
			(item) => item.itemSpec.title === 'Power'
		)?.value.key;

		if (!type || !power || !color || !yearCode) return null;

		power = ('000' + power).substr(-3);

		const firstMomentOfThisYear = moment().startOf('jYear').valueOf();
		const currentMoment = moment().valueOf();

		let pkgList = [];
		try {
			const qeury = `
				FOR p IN Package
				FILTER REGEX_TEST(p._key,"${type}${yearCode}${power}${color}")
				AND p.createdAt < ${currentMoment}
				AND p.createdAt >= ${firstMomentOfThisYear}
				SORT p.createdAt DESC
				LIMIT 0,1
				RETURN p
			`;
			const cursor = await db.query(qeury);
			pkgList = await cursor.all();
		} catch (e) {
			return null;
		}

		let tempCode = null;
		if (!pkgList || pkgList.length === 0) {
			tempCode = type + yearCode + power + '000000';
		} else {
			const pkg: Package = pkgList[0];
			tempCode = pkg?._key;
		}

		const lastPackageCode: number = parseInt(
			tempCode.slice(tempCode.length - 6)
		);
		const count = ('000000' + (lastPackageCode + 1)).substr(-6);
		return type + yearCode + power + color + count;
	}

	// @ActionAccess('Item/create', 'Company')
	async create2_api(itemData: Item, zone: ZoneObj, socket: Socket) {
		const userInfo = this.getUserInfo(socket);

		const shift = (await Item.getDB()
			.collection('Shift')
			.document(zone.key)) as Shift;

		if (!shift)
			return new Response(
				{},
				[{ title: 'Shift Not Found', type: 'error' }],
				'Not Found'
			);

		const values: {
			itemSpec: ItemSpec;
			value: { key: string; title: string };
		}[] = shift.itemType.values;

		const parsedItemKey = {
			type: itemData._key.slice(0, 2),
			year: itemData._key.slice(2, 3),
			power: itemData._key.slice(3, 6),
			color: itemData._key.slice(6, 7),
			count: itemData._key.slice(7, 15)
		};
		const mustContain = {
			type: null,
			year: null,
			power: null,
			color: null
		};
		const year = moment().format('jYY').charAt(1);
		mustContain.year = year;
		values.forEach((item) => {
			if (item.itemSpec.title === 'Type')
				mustContain.type = item.value.key;
			else if (item.itemSpec.title === 'Power')
				mustContain.power = ('000' + item.value.key).substr(-3);
			else if (item.itemSpec.title === 'Color') {
				// TODO Color is now hardcoded so it should be changed if a problem comes up
				if (item.value.key === 'A') mustContain.color = '1';
				else if (item.value.key === 'M') mustContain.color = '2';
			}
		});
		const isItemCompatible = Object.entries(mustContain).reduce(
			(acc, [key, value]) => acc && parsedItemKey[key] === value,
			true
		);
		//|| !parseInt(parsedItemKey.count)
		if (!isItemCompatible)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'Item is not compatible with current production line'
					}
				],
				'Forbidden'
			);
		// ---- pacakge ----
		// we get the package code that is being sent by client
		const pkg = new Package();
		const filter = [
			new FilterMDL<Package, string>('==', '_key', itemData.packageKey),
			//@ts-ignore
			new FilterMDL<Package, string>(
				'>',
				//@ts-ignore
				'_key AND LENGTH(u.zones.' +
					zone.name +
					'[* FILTER CURRENT.k == "' +
					zone.key +
					'" AND !CURRENT.l])',
				0
			)
			// FILTER LENGTH(u.zones.ProductionLine[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
		];
		const pkgList = await pkg.read({ filter, page: 1, pageSize: 1 }, zone);
		// ----------------

		let packageKey: string = null;
		let packageItems: string[] = [];
		if (pkgList?.totalCount === 0) {
			// --------------------
			const _key = await this.generateThePackageCode(shift, zone);
			if (!_key)
				return new Response(
					{},
					[
						{
							title: 'Package Cannot be created with new code',
							type: 'error'
						}
					],
					'Internal Server Error'
				);

			const newPkg = new Package();

			newPkg.projectFrom({
				shiftCode: [zone.key],
				status: 'filling',
				items: [],
				itemType: shift.itemType?._key
			});
			newPkg._key = _key;
			const createdPkg: Package = await newPkg.create(userInfo, zone);
			packageKey = createdPkg._key;
			packageItems = createdPkg.items;
		} else {
			const _pkg = pkgList.items[0] || null;

			if (!_pkg)
				return new Response(
					{},
					[{ title: 'Package Not Found', type: 'error' }],
					'Not Found',
					[]
				);
			if (_pkg.status !== 'filling')
				return new Response(
					{},
					[
						{
							title: 'This package is already filled',
							type: 'error'
						}
					],
					'OK',
					[]
				);

			packageKey = _pkg._key;
			packageItems = _pkg.items;
		}
		itemData.packageKey = packageKey;

		const item = new Item();
		item.projectFrom(itemData);
		item._key = itemData._key;

		let result;
		try {
			result = await item.create(userInfo, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						content: '',
						title: 'Another Package has this item',
						type: 'error'
					}
				],
				'Forbidden'
			);
		}
		if (!result._key)
			return new Response(
				{},
				[
					{
						title: 'Cannot Create Item',
						content: 'Check out your sending data',
						type: 'error'
					}
				],
				'Bad Request'
			);

		const items = [...packageItems, result._key];

		const status =
			parseInt(shift.packing.capacity) === packageItems.length + 1
				? 'filled'
				: undefined;

		const pckg = new Package();

		let resp;
		try {
			resp = await pckg.update({
				key: packageKey,
				data: { items, status }
			});
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Cannot add this item to the package',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
		if (!resp || !resp.after)
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Check out your sending data',
						type: 'error'
					}
				],
				'Bad Request'
			);
		else
			return new Response(
				result,
				[
					{
						title: 'Item Created.',
						type: 'success'
					}
				],
				'OK'
			);

		//TODO: after package creation we need to add the item to the package items
		/*return new Promise<Response>((resolve, reject) => {
			try {
				super
					.create_api(itemData, zone, socket)
					.then((response) => {
						if (response.data?.hasOwnProperty('_key')) {
							const items = [
								...packageItems,
								response.data['_key']
							];
							const pkg = new Package();

							const status =
								parseInt(shift.packing.capacity) ===
								packageItems.length + 1
									? 'filled'
									: undefined;
							pkg.update({
								key: packageKey,
								data: { items, status }
							}).then(() => {
								resolve(response);
							});
						} else {
							//FIXME handle if the item already exists in this package

							resolve(
								new Response(
									{},
									[
										{
											content: '',
											title: 'Another Package has this item',
											type: 'error'
										}
									],
									'Forbidden'
								)
							);
						}
					})
					.catch((e) => {
						resolve(
							new Response(
								{},
								[
									{
										content: '',
										title: 'Error in creating item',
										type: 'error'
									}
								],
								'Forbidden',
								[e]
							)
						);
					});
			} catch (e) {
				resolve(
					new Response(
						{},
						[
							{
								content: '',
								title: 'Error in creating item',
								type: 'error'
							}
						],
						'Forbidden',
						[e]
					)
				);
			}
		});*/
	}

	@ActionAccess('Item/create', 'Company')
	async create_api(itemData: Item, zone: ZoneObj, socket: Socket) {
		const userInfo = this.getUserInfo(socket);

		let ifAlreadyExist;
		try {
			ifAlreadyExist = await Package.runAQL(
				aql`
				FOR i IN Item
				FILTER i._key == @iKey
				RETURN i
			`.query,
				{ iKey: itemData._key }
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
		if (ifAlreadyExist && ifAlreadyExist.length > 0)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content: 'Item is in another package'
					}
				],
				'Forbidden'
			);

		let shift: Shift;

		try {
			shift = (await Item.getDB()
				.collection('Shift')
				.document(zone.key)) as Shift;
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Shift not found',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
		if (!shift)
			return new Response(
				{},
				[{ title: 'Shift Not Found', type: 'error' }],
				'Not Found'
			);

		const values: {
			itemSpec: ItemSpec;
			value: { key: string; title: string };
		}[] = shift.itemType.values;

		const parsedItemKey = {
			type: itemData._key.slice(0, 2),
			year: itemData._key.slice(2, 3),
			power: itemData._key.slice(3, 6),
			color: itemData._key.slice(6, 7),
			count: itemData._key.slice(7, 15)
		};
		const mustContain = {
			type: null,
			year: null,
			power: null,
			color: null
		};
		const year = moment().format('jYY').charAt(1);
		mustContain.year = year;
		values.forEach((item) => {
			if (item.itemSpec.title === 'Type')
				mustContain.type = item.value.key;
			else if (item.itemSpec.title === 'Power')
				mustContain.power = ('000' + item.value.key).substr(-3);
			else if (item.itemSpec.title === 'Color') {
				// TODO Color is now hardcoded so it should be changed if a problem comes up
				if (item.value.key === 'A') mustContain.color = '1';
				else if (item.value.key === 'M') mustContain.color = '2';
			}
		});
		const isItemCompatible = Object.entries(mustContain).reduce(
			(acc, [key, value]) => acc && parsedItemKey[key] === value,
			true
		);
		//|| !parseInt(parsedItemKey.count)
		if (!isItemCompatible)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'Item is not compatible with current production line'
					}
				],
				'Forbidden'
			);

		let pkg: Package = null;
		if (itemData.packageKey) {
			try {
				const pkgs = await Package.runAQL(
					aql`
						FOR p IN Package
						FILTER p._key == @packageKey AND p.status == 'filling'
						FILTER LENGTH(p.zones.Shift[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0 
						RETURN p
						`.query,
					{ packageKey: itemData.packageKey, zoneKey: zone.key }
				);
				if (pkgs && pkgs[0]._key) {
					pkg = pkgs[0];
				}
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							type: 'error',
							content: 'Package Not Found'
						}
					],
					'Not Found'
				);
			}
		}

		if (!pkg) {
			// create item
			// update package
			const trx = await Package.getDB().beginTransaction({
				write: ['Package', 'Item']
			});

			const now = moment().valueOf();

			const _key = await this.generateThePackageCode(shift, zone);
			if (!_key)
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Cannot generate new code',
							type: 'error'
						}
					],
					'Bad Request'
				);
			const pkgDoc = {
				_key,
				status: 'filling',
				items: [],
				shiftCode: [shift._key],
				createdAt: now,
				itemType: shift.itemType._key,
				packing: shift.packing,
				createdBy: userInfo.userKey,
				zones: {
					Company: shift.zones.Company,
					ProductionLine: shift.zones.ProductionLine,
					Shift: [
						{
							k: shift._key,
							e: now
						}
					]
				}
			};

			const step1Result: { success: boolean; data?: Package[] } =
				await trx.step(async () => {
					try {
						const cursor = await Package.getDB().query(
							aql`
								INSERT @pkgDoc INTO Package RETURN NEW		
							`.query,
							{
								pkgDoc: pkgDoc
							}
						);
						const pkg = await cursor.all();

						if (cursor.extra.stats.writesExecuted === 1) {
							return { success: true, data: pkg };
						} else {
							return { success: false };
						}
					} catch (e) {
						return { success: false, e };
					}
				});

			if (!step1Result.success) {
				await trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Package not registered',
							type: 'error'
						}
					],
					'Bad Request'
				);
			}

			const itmDoc = {
				_key: itemData._key,
				shiftCode: shift._key,
				createdAt: now,
				createdBy: userInfo.userKey,
				packageKey: step1Result.data[0]._key,
				zones: {
					Company: shift.zones.Company,
					ProductionLine: shift.zones.ProductionLine,
					Shift: [{ k: shift._key, e: now }]
				}
			};
			const step2Result: { success: boolean; data?: Item[] } =
				await trx.step(async () => {
					try {
						const cursor = await Package.getDB().query(
							aql`
							INSERT @itemDoc INTO Item RETURN NEW		
						`.query,
							{
								itemDoc: itmDoc
							}
						);

						const itm = await cursor.all();
						if (cursor.extra.stats.writesExecuted === 1) {
							return { success: true, data: itm };
						} else {
							return { success: false };
						}
					} catch (e) {
						return { success: false, e };
					}
				});

			if (!step2Result.success) {
				await trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Item is in another package',
							type: 'error'
						}
					],
					'Bad Request'
				);
			}
			const pkgKey = step1Result.data[0]._key;
			const uData = { items: [step2Result.data[0]._key] };
			const step3Result: { success: boolean } = await trx.step(
				async () => {
					try {
						const cursor = await Package.getDB().query(
							aql`
							UPDATE @pKey WITH @uData IN Package
							RETURN { before: OLD, after: NEW }
						`.query,
							{
								pKey: pkgKey,
								uData
							}
						);
						await cursor.all();
						if (cursor.extra.stats.writesExecuted === 1) {
							return { success: true };
						} else {
							return { success: false };
						}
					} catch (e) {
						return { success: false, e };
					}
				}
			);

			if (step3Result.success) {
				await trx.commit();
				return new Response(
					step2Result.data,
					[
						{
							title: 'Item Created.',
							type: 'success'
						}
					],
					'OK'
				);
			} else {
				await trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Could not register item please try again',
							type: 'error'
						}
					],
					'Bad Request'
				);
			}
		} else {
			const trx = await Package.getDB().beginTransaction({
				write: ['Package', 'Item']
			});

			const now = moment().valueOf();

			const itmDoc = {
				_key: itemData._key,
				shiftCode: shift._key,
				createdAt: now,
				createdBy: userInfo.userKey,
				packageKey: pkg._key,
				zones: {
					Company: shift.zones.Company,
					ProductionLine: shift.zones.ProductionLine,
					Shift: [{ k: shift._key, e: now }]
				}
			};
			const step1Result: { success: boolean; data?: Item[] } =
				await trx.step(async () => {
					const cursor = await Package.getDB().query(
						aql`
							INSERT @itemDoc INTO Item RETURN NEW		
						`.query,
						{
							itemDoc: itmDoc
						}
					);

					const itm = await cursor.all();
					if (cursor.extra.stats.writesExecuted === 1) {
						return { success: true, data: itm };
					} else {
						return { success: false };
					}
				});

			if (!step1Result.success) {
				await trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Could not create item',
							type: 'error'
						}
					],
					'Bad Request'
				);
			}
			const uData = {
				items: [...pkg.items, step1Result.data[0]._key],
				status:
					pkg.items.length + 1 === parseInt(shift.packing.capacity)
						? 'filled'
						: 'filling'
			};
			const pKey = pkg._key;
			const step2Result: { success: boolean; data?: Package[] } =
				await trx.step(async () => {
					const cursor = await Package.getDB().query(
						aql`
						UPDATE @pKey WITH @uData IN Package
						RETURN { before: OLD, after: NEW }
					`.query,
						{
							pKey,
							uData
						}
					);
					await cursor.all();
					if (cursor.extra.stats.writesExecuted === 1) {
						return { success: true };
					} else {
						return { success: false };
					}
				});

			if (step2Result.success) {
				await trx.commit();
				return new Response(
					step1Result.data,
					[{ title: 'Item Created', type: 'success' }],
					'OK'
				);
			} else {
				await trx.abort();
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							content: 'Item was not created. try again',
							type: 'error'
						}
					],
					'Bad Request'
				);
			}
		}
	}

	@ActionAccess('Item/find', 'Company')
	async find_api({ _key }, zone: ZoneObj) {
		try {
			const result = await Item.runAQL(
				aql`
				FOR i IN Item
				FILTER i._key == @iKey
				RETURN MERGE(i, {shift: DOCUMENT("Shift", i.shiftCode)})
			`.query,
				{ iKey: _key }
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Not Found', type: 'error' }],
					'Not Found'
				);
			else return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Item/findForGuarantee', 'Company')
	async findForGuarantee_api({ _key }, zone: ZoneObj) {
		try {
			const result = await Item.runAQL(
				aql`
				FOR i IN Item
				FILTER i._key == @iKey
				RETURN MERGE(i, {shift:DOCUMENT('Shift', i.shiftCode)})
			`.query,
				{ iKey: _key }
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[{ title: 'Not Found', type: 'error' }],
					'Not Found'
				);
			if (!result[0]?.zones?.Sold)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: 'Item is not sold',
							type: 'error'
						}
					],
					'Forbidden'
				);
			if (result[0]?.zones?.Guarantee)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: 'Item has aleady gone to guarantee',
							type: 'error'
						}
					],
					'Forbidden'
				);
			if (result[0]?.shift?.guaranteeDate < moment().unix())
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content: "This item's guarantee date is passed",
							type: 'error'
						}
					],
					'Forbidden'
				);
			else return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Item/findForBreakdownReason', 'Company')
	async findForBreakdownReason_api({ _key }, zone: ZoneObj) {
		try {
			const result = await Item.runAQL(
				aql`
				FOR i IN Item
				FILTER i._key == @iKey
				FILTER LENGTH(i.zones.Guarantee[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l]) > 0
				RETURN i
			`.query,
				{ iKey: _key, zoneKey: 'one' }
			);
			if (result.length === 0)
				return new Response(
					{},
					[{ title: 'Not Found', type: 'error' }],
					'Not Found'
				);
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	/*findDuplicates(arr: string[]) {
		let sorted_arr = arr.slice().sort(); // You can define the comparing function here.
		// JS by default uses a crappy string compare.
		// (we use slice to clone the array so the
		// original array won't be modified)
		let results = [];
		for (let i = 0; i < sorted_arr.length - 1; i++) {
			if (sorted_arr[i + 1] == sorted_arr[i]) {
				results.push(sorted_arr[i]);
			}
		}
		return results;
	}

	@ActionAccess('Item/batchAdd', 'Company')
	async batchAdd_api(data: { items: string[] }, zone, socket) {
		const userInfo = this.getUserInfo(socket);
		if (this.findDuplicates(data.items).length > 0)
			return new Response(
				{},
				[{ title: 'Duplicate Items', type: 'error' }],
				'Forbidden'
			);

		const shift = (await Item.getDB()
			.collection('Shift')
			.document(zone.key)) as Shift;

		if (!shift)
			return new Response(
				{},
				[{ title: 'Shift Not Found', type: 'error' }],
				'Not Found'
			);

		const values: {
			itemSpec: ItemSpec;
			value: { key: string; title: string };
		}[] = shift.itemType.values;

		const year = moment().format('YY').charAt(1);
		const mustContain = {
			type: null,
			year: null,
			power: null,
			color: null
		};
		mustContain.year = year;
		values.forEach((item) => {
			if (item.itemSpec.title === 'Type')
				mustContain.type = item.value.key;
			else if (item.itemSpec.title === 'Power')
				mustContain.power = ('000' + item.value.key).substr(-3);
			else if (item.itemSpec.title === 'Color') {
				// TODO Color is now hardcoded so it should be changed if a problem comes up
				if (item.value.key === 'A') mustContain.color = '1';
				else if (item.value.key === 'M') mustContain.color = '2';
			}
		});
		const validItems = data.items.reduce((acc, item) => {
			if (item.length != 15) return false;
			const parsedItemKey = {
				type: item.slice(0, 2),
				year: item.slice(2, 3),
				power: item.slice(3, 6),
				color: item.slice(6, 7),
				count: item.slice(7, 15)
			};

			const isItemCompatible = Object.entries(mustContain).reduce(
				(acc, [key, value]) => acc && parsedItemKey[key] === value,
				true
			);
			return acc && isItemCompatible;
		}, true);

		if (!validItems)
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error',
						content: 'Check the items'
					}
				],
				'Bad Request'
			);
		const packageKey = '';
		const doc = [];
		data.items.forEach((item) => {
			doc.push({
				_key: item,
				shiftCode: shift._key,
				createdAt: moment().valueOf(),
				createdBy: userInfo.userKey,
				zones: {
					Company: [
						{
							k: 'amiran'
						}
					]
				}
			});
		});

		const query = `
			FOR doc in [
				{ "name": "Doc 1", "value": 1 }, 
				{ "name": "Doc 2", "value": 1 }, 
				{ "name": "Doc 3", "value": 1 }]
			
			INSERT doc IN collection
		`;
	}*/

	//FIXME this should be done with its key
	@ActionAccess('Item/delete', 'Shift')
	async delete_api(data: Item, zone: ZoneObj) {
		// const shiftInstance = new Shift();
		// const shiftFilter = [new FilterMDL('==', '_key', data.shiftCode)];
		// const shiftList = await shiftInstance.read(
		// 	{ filter: shiftFilter },
		// 	zone
		// );
		// if (
		// 	!shiftList ||
		// 	shiftList.totalCount === 0 ||
		// 	!shiftList.items[0]?._key
		// )
		// 	return new Response(
		// 		{},
		// 		[{ title: 'Shift Not Found', type: 'error' }],
		// 		'Not Found'
		// 	);

		// const shiftKey = shiftList.items[0]?._key;
		// ------- Item --------
		let item: Item[];
		try {
			item = await Package.runAQL(
				aql`
					FOR i IN Item
					FILTER i._key == @itemKey
					FILTER LENGTH(i.zones.Shift[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0 
					RETURN i
					`.query,
				{ itemKey: data._key, zoneKey: zone.key }
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error',
						content: 'Item Not Found'
					}
				],
				'Bad Request'
			);
		}
		if (!item || !item[0]._key)
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						type: 'error',
						content: 'Item Not Found'
					}
				],
				'Not Found'
			);

		// ------ Package ------
		const packageInstance = new Package();
		const pkgFilter = [
			new FilterMDL('==', '_key', data.packageKey),
			new FilterMDL('IN', 'shiftCode', zone.key),
			new FilterMDL('IN', 'items', item[0]._key)
		];
		const pkgList = await packageInstance.read(
			{
				filter: pkgFilter,
				page: 1,
				pageSize: 1
			},
			zone
		);
		if (pkgList.totalCount === 0) {
			return new Response(
				{},
				[{ title: 'Item Not Found In This Package', type: 'error' }],
				'Not Found'
			);
		}
		const pkgData: Package = pkgList.items[0];
		if (pkgData.status !== 'filling' && pkgData.status !== 'filled')
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						content:
							'Removing item from a package with a status other than "filling" or "filled" is not allowed',
						type: 'error'
					}
				],
				'Forbidden'
			);

		const items = pkgData.items.filter(
			(itm: string) => item[0]._key !== itm
		);
		// ----------------------

		const status = pkgData.status === 'filled' ? 'filling' : undefined;

		const trx = await Package.getDB().beginTransaction({
			write: ['Package', 'Item']
		});
		const step1Result: { success: boolean } = await trx.step(async () => {
			try {
				const itemInstance = new Item();
				await packageInstance.update({
					key: pkgData._key,
					data: { items, status }
				});
				await itemInstance.remove({ _key: item[0]._key });
				return { success: true };
			} catch (e) {
				return { success: false };
			}
		});
		if (step1Result.success) {
			await trx.commit();
			return new Response(
				{},
				[
					{
						content: 'Success',
						title: 'Item Deleted',
						type: 'success'
					}
				],
				'OK'
			);
		} else {
			await trx.abort();
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Item Not Deleted',
						type: 'error'
					}
				],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Item/findPackage', 'Company')
	async findPackage_api(
		data: { _key: string; shiftCode: string },
		zone: ZoneObj
	) {
		try {
			const itemInstance = new Item();
			const filter = [new FilterMDL('==', '_key', data._key)];
			const itemList = await itemInstance.read(
				{ filter, pageSize: 1 },
				zone
			);

			if (itemList.totalCount === 0)
				return new Response(
					{},
					[{ title: 'Item Not Found', type: 'error' }],
					'Not Found'
				);

			const item = itemList.items[0];
			const pkgInstance = new Package();
			const pkgFilter = [
				new FilterMDL('IN', 'shiftproCode', data.shiftCode),
				new FilterMDL('IN', 'items', item._key)
			];
			const pkgList = await pkgInstance.read(
				{
					filter: pkgFilter,
					pageSize: 1
				},
				zone
			);

			if (pkgList.totalCount === 0)
				return new Response(
					{},
					[{ title: 'Package Not Found', type: 'error' }],
					'Not Found'
				);
			const pkg = pkgList.items[0];

			return new Response(pkg, [], 'OK');
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Internal Server Error', type: 'error' }],
				'Internal Server Error'
			);
		}
	}
	@ActionAccess('Item/getBrokenDowns', 'Company')
	async getBrokenDowns_api({ page, pageSize }, zone: ZoneObj) {
		try {
			const result = await Item.runAQL(
				aql`
				LET items = (FOR i IN Item
					FILTER LENGTH(i.zones.Guarantee[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l]) > 0
					SORT LENGTH(i.brkRsn)
					LIMIT @page, @pageSize
					RETURN {_key: i._key, createdAt: i._key, shift:i.shiftCode, breakdownReasons: i.brkRsn OR []})
				LET totalCount = LENGTH(FOR i IN Item
					FILTER LENGTH(i.zones.Guarantee[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l]) > 0
					RETURN i)
				RETURN {items, totalCount}			
			`.query,
				{
					zoneKey: 'one',
					page,
					pageSize
				}
			);
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Item/setBreakReason', 'Company')
	async setBreakReason_api(
		{ _key, reasons }: { _key: string; reasons: string[] },
		zone: ZoneObj
	) {
		const verify = reasons.reduce(
			(acc, item) => acc && breakdownReasons.includes(item),
			true
		);
		if (!verify)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						content: 'Reason is not allowed',
						type: 'error'
					}
				],
				'Forbidden'
			);
		try {
			const result = await Item.runAQL(
				aql`
				FOR i IN Item
				FILTER i._key == @iKey
				FILTER LENGTH(i.zones.Guarantee[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l]) > 0
				UPDATE @iKey WITH {brkRsn:@reasons} IN Item OPTIONS {mergeObjects: false}
				RETURN {after: NEW, before: OLD}
			`.query,
				{ iKey: _key, zoneKey: 'one', reasons }
			);
			if (result.length === 0)
				return new Response(
					{},
					[{ title: 'Not Found', type: 'error' }],
					'Not Found'
				);

			return new Response({});
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Item/purchasedProducedStat', 'Company')
	async purchasedProducedStat_api({}, zone: ZoneObj) {
		try {
			const result = (await Item.runAQL(
				aql`
					LET itemTypes = [{type:'M',rgx:'[0-9]{6}2(.){8}'},{type:'A',rgx:'[0-9]{6}1(.){8}'}]

					LET purchased = (FOR i IN Item 
						FILTER i.order
						FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
						LET type =  (
						FOR it IN itemTypes
						FILTER REGEX_TEST(i._key, it.rgx)
						RETURN it.type)[0]

					COLLECT cnt = type INTO itemsByType

					RETURN {
							type: cnt,
							count: length(itemsByType[*])
						})
					let produced = (
					FOR i IN Item 
						LET type =  (
						FOR it IN itemTypes
						FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
						FILTER REGEX_TEST(i._key, it.rgx)
						RETURN it.type)[0]

					COLLECT cnt = type INTO itemsByType

					RETURN {
							type: cnt,
							count: length(itemsByType[*])
						}
					)
					RETURN {purchased, produced}
				`.query,
				{
					cKey: zone.key
				}
			)) || [{ purchased: [], produced: [] }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
