import Model from '../Base/Model';
import { DataController } from '../Base/DataController';
import Controller, {
	ActionAccess,
	ZoneObj,
	ZoneTree
} from '../Base/Controller';
import { AbstractOrderItem, Order, OrderItem } from '../Entity/Order';
import ReadMDL, { FilterMDL } from '../Model/Entity/ReadMDL';
import { Socket } from 'socket.io';
import FindByKey from '../Model/FindByKey';
import Response, { ResponseStatus } from '../Interface/Response';
import { NULL_MDL } from './UserController';
import { aql } from 'arangojs';
import { GetOrderDetailMDL } from '../Model/Order/GetOrderDetailMDL';
import { LoadPackageMDL } from '../Model/Order/LoadPackageMDL';
import { Package } from '../Entity/Package';
import { RejectOrder } from '../Model/Order/RejectOrder';
import { UpdateTransportInfoMDL } from '../Model/Order/UpdateTransportInfoMDL';
import { LoadUnpackedItemMDL } from '../Model/Order/LoadUnpackedItemMDL';
import { Item } from '../Entity/Item';
import { nanoid } from 'nanoid';
import { SetLadingCodeMDL } from '../Model/Order/SetLadingCodeMDL';
import IntervalMDL from '../Model/IntervalMDL';
import ItemTypeController from './ItemTypeController';
import { MonthlySalesMDL } from '../Model/Order/MonthlySales';
import { DailySalesMDL } from '../Model/Order/DailySales';

export default class OrderController extends DataController<Order> {
	models: Partial<Record<keyof OrderController, Model>> = {
		create_api: Order,
		read_api: ReadMDL,
		readd_api: ReadMDL,
		getReadyToLoad_api: NULL_MDL,
		getOrderDetail_api: GetOrderDetailMDL,
		loadPackage_api: LoadPackageMDL,
		accVerify_api: FindByKey,
		accReject_api: RejectOrder,
		adminVerify_api: FindByKey,
		adminReject_api: RejectOrder,
		adminCancel_api: RejectOrder,
		cancel_api: FindByKey,
		setOrderPendingAcc_api: FindByKey,
		setOrderSuspended_api: FindByKey,
		updateTransportInfo_api: UpdateTransportInfoMDL,
		loadUnpackedItem_api: LoadUnpackedItemMDL,
		setOrderToLoading_api: FindByKey,
		setOrderFinished_api: FindByKey,
		addStatusListener_api: FindByKey,
		removeStatusListener_api: FindByKey,
		getOrderById_api: FindByKey,
		setOrderLading_api: SetLadingCodeMDL,
		intervalSalesStat_api: IntervalMDL,
		intervalSales_api: IntervalMDL,
		intervalOrders_api: IntervalMDL,
		dailySales_api: DailySalesMDL,
		monthlySales_api: MonthlySalesMDL
	};

	defaultEntity = new Order();

	constructor() {
		super();
	}

	@ActionAccess('Order/read', 'Company')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}

	@ActionAccess('Order/readd', 'Warehouse')
	async readd_api({ page, pageSize, status }, zone: ZoneObj) {
		pageSize = pageSize || 10;
		page = page || 1;
		status = status || null;
		const result =
			status && status !== null
				? (
						await Order.runAQL(
							aql`
						LET items = (FOR o IN Order
							SORT o._key DESC
							FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
							AND o.status == @status
							LIMIT @index, @pageSize
							RETURN MERGE(o, 
								{
									customer: DOCUMENT('Customer', o.customer)
								}
							))
							LET totalCount = 
								LENGTH(FOR o in Order
									FILTER 
										LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
										AND o.status == @status
									RETURN o
								)
							RETURN {items, totalCount}
						`.query,
							{
								zoneKey: zone.key,
								pageSize,
								index: pageSize * (page - 1),
								status
							}
						)
				  )[0]
				: (
						await Order.runAQL(
							aql`
								LET items = (FOR o IN Order
								SORT o._key DESC
								FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
								LIMIT @index, @pageSize
								RETURN MERGE(o, 
									{
										customer: DOCUMENT('Customer', o.customer)
									}
								)

								)
								LET totalCount = 
									LENGTH(FOR o in Order
										FILTER 
											LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
										RETURN o
								)
								RETURN {items, totalCount}
			`.query,
							{
								zoneKey: zone.key,
								pageSize,
								index: pageSize * (page - 1)
							}
						)
				  )[0];
		return new Response(result);
	}

	@ActionAccess('Order/create', 'Warehouse')
	async create_api(data: Order, zone: ZoneObj, socket: Socket) {
		data.status = 'suspended';
		return super.create_api(data, zone, socket);
	}

	@ActionAccess('Order/accVerify', 'Company')
	async accVerify_api({ _key }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
			`.query,
				{
					oKey: _key
				}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (order.status !== 'pendingAccountantVerify')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only verify the orders with status "Pending Accountant Verify"'
						}
					],
					'Forbidden'
				);
			try {
				const updateResult = await Order.runAQL(
					aql`
					UPDATE @oKey WITH 
					{status: 'pendingAdminVerify'} IN Order
					RETURN NEW
				`.query,
					{
						oKey: _key
					}
				);
				if (!updateResult || updateResult.length === 0)
					return new Response(
						{},
						[
							{
								title: 'Bad Request',
								type: 'error',
								content: 'Could not update the order'
							}
						],
						'Bad Request'
					);
				const numbers = await Controller.getRolePhoneNumbers([
					'Role/admin',
					'Role/salesManager',
					'Role/salesperson',
					'Role/salesExpert',
					'Role/supervisor'
				]);
				numbers.forEach((number) =>
					this.SMSAPI.VerifyLookup({
						receptor: number,
						token: order._key,
						token10: order.customer['fullname'],
						template: 'orderAccVerified'
					})
				);

				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Order is pending Admin verify',
							type: 'success'
						}
					],
					'OK'
				);
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/adminVerify', 'Company')
	async adminVerify_api({ _key }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
			`.query,
				{
					oKey: _key
				}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (order.status !== 'pendingAdminVerify')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only verify the orders with status "Pending Admin Verify"'
						}
					],
					'Forbidden'
				);
			try {
				const updateResult = await Order.runAQL(
					aql`
					UPDATE @oKey WITH 
					{status: 'pendingWarehouseLoading'} IN Order
					RETURN NEW
				`.query,
					{
						oKey: _key
					}
				);
				if (!updateResult || updateResult.length === 0)
					return new Response(
						{},
						[
							{
								title: 'Bad Request',
								type: 'error',
								content: 'Could not update the order'
							}
						],
						'Bad Request'
					);
				const numbers = await Controller.getRolePhoneNumbers([
					'Role/admin',
					'Role/supervisor',
					'Role/manager',
					'Role/salesManager',
					'Role/salesExpert',
					'Role/salesperson',
					'Role/accountingExpert',
					'Role/warehouseKeeper'
				]);
				numbers.forEach((number) =>
					this.SMSAPI.VerifyLookup({
						receptor: number,
						token: order._key,
						token10: order.customer['fullname'],
						template: 'orderVerified'
					})
				);

				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Order is pending warehouse loading',
							type: 'success'
						}
					],
					'OK'
				);
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}

		const orderInstance = new Order();
		const filter = [new FilterMDL('==', '_key', _key)];
		let orderList: { totalCount: number; items: Order[] };
		try {
			orderList = await orderInstance.read({ filter }, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
		if (!orderList || orderList.totalCount === 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Order Not Found',
						type: 'error'
					}
				],
				'Not Found'
			);
		}

		const order: Order = orderList.items[0];
		if (order.status !== 'pendingAdminVerify')
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'You can only verify the orders with status "Pending Admin Verify"'
					}
				],
				'Forbidden'
			);
		try {
			await orderInstance.update({
				key: order._key,
				data: { status: 'pendingWarehouseLoading' }
			});
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order is pending Warehouse Loading',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error',
						content: 'Could not update the order'
					}
				],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Order/adminReject', 'Company')
	async adminReject_api({ _key, reason }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
			`.query,
				{
					oKey: _key
				}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (order.status !== 'pendingAdminVerify')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only reject the orders with status "Pending Admin Verify"'
						}
					],
					'Forbidden'
				);
			try {
				const updateResult = await Order.runAQL(
					aql`
					UPDATE @oKey WITH 
					{status: 'adminRejected', adminRejectReason: @reason} IN Order
					RETURN NEW
				`.query,
					{
						oKey: _key,
						reason
					}
				);
				if (!updateResult || updateResult.length === 0)
					return new Response(
						{},
						[
							{
								title: 'Bad Request',
								type: 'error',
								content: 'Could not update the order'
							}
						],
						'Bad Request'
					);
				const numbers = await Controller.getRolePhoneNumbers([
					'Role/supervisor',
					'Role/salesExpert',
					'Role/salesManager',
					'Role/salesperson'
				]);
				numbers.forEach((number) =>
					this.SMSAPI.VerifyLookup({
						receptor: number,
						token: order._key,
						token10: order.customer['fullname'],
						token20: reason,
						template: 'orderAdminReject'
					})
				);

				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Order rejected',
							type: 'info'
						}
					],
					'OK'
				);
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/adminCancel', 'Company')
	async adminCancel_api({ _key, reason }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
			`.query,
				{
					oKey: _key
				}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (
				order.status === 'loading' ||
				order.status === 'loaded' ||
				order.status === 'sent'
			)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only cancel the orders before status "Loading"'
						}
					],
					'Forbidden'
				);
			try {
				const updateResult = await Order.runAQL(
					aql`
					UPDATE @oKey WITH 
					{status: 'adminCanceled', adminCancelReason: @reason } IN Order
					RETURN NEW
				`.query,
					{
						oKey: _key,
						reason
					}
				);
				if (!updateResult || updateResult.length === 0)
					return new Response(
						{},
						[
							{
								title: 'Bad Request',
								type: 'error',
								content: 'Could not update the order'
							}
						],
						'Bad Request'
					);
				const numbers = await Controller.getRolePhoneNumbers([
					'Role/supervisor',
					'Role/salesperson',
					'Role/salesExpert',
					'Role/salesManager'
				]);
				numbers.forEach((number) =>
					this.SMSAPI.VerifyLookup({
						receptor: number,
						token: order._key,
						token10: order.customer['fullname'],
						token20: reason,
						template: 'orderAdminCanceled'
					})
				);

				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Order canceled',
							type: 'info'
						}
					],
					'OK'
				);
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/accReject', 'Company')
	async accReject_api({ _key, reason }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
			`.query,
				{
					oKey: _key
				}
			);
			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (order.status !== 'pendingAccountantVerify')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only reject the orders with status "Pending Accountant Verify"'
						}
					],
					'Forbidden'
				);
			try {
				const updateResult = await Order.runAQL(
					aql`
					UPDATE @oKey WITH 
					{status: 'accountantRejected', accountantRejectReason: @reason} IN Order
					RETURN NEW
				`.query,
					{
						oKey: _key,
						reason
					}
				);
				if (!updateResult || updateResult.length === 0)
					return new Response(
						{},
						[
							{
								title: 'Bad Request',
								type: 'error',
								content: 'Could not update the order'
							}
						],
						'Bad Request'
					);
				const numbers = await Controller.getRolePhoneNumbers([
					'Role/admin',
					'Role/supervisor',
					'Role/salesperson',
					'Role/salesExpert',
					'Role/salesManager'
				]);
				numbers.forEach((number) =>
					this.SMSAPI.VerifyLookup({
						receptor: number,
						token: order._key,
						token10: order.customer['fullname'],
						token20: reason,
						template: 'orderAccReject'
					})
				);

				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Order rejected',
							type: 'info'
						}
					],
					'OK'
				);
			} catch (e) {
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			}
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/cancel', 'Company')
	async cancel_api({ _key }, zone: ZoneObj, socket: Socket) {
		const orderInstance = new Order();
		const filter = [new FilterMDL('==', '_key', _key)];
		let orderList: { totalCount: number; items: Order[] };
		try {
			orderList = await orderInstance.read({ filter }, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
		if (!orderList || orderList.totalCount === 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Order Not Found',
						type: 'error'
					}
				],
				'Not Found'
			);
		}

		const order: Order = orderList.items[0];
		if (
			order.status !== 'pendingAccountantVerify' &&
			order.status !== 'suspended'
		)
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'You can only cancel the orders with status "Pending Accountant Verify" OR "Suspended"'
					}
				],
				'Forbidden'
			);
		try {
			await orderInstance.update({
				key: order._key,
				data: {
					status: 'canceled'
				}
			});
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order Cancelled',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error',
						content: 'Could not update the order'
					}
				],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Order/setOrderPendingAcc', 'Company')
	async setOrderPendingAcc_api({ _key }, zone: ZoneObj, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o in Order
				FILTER o._key == @oKey
				RETURN MERGE(o,{customer: DOCUMENT('Customer', o.customer)})
			`.query,
				{
					oKey: _key
				}
			);

			if (!result || result.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Order Not Found',
							type: 'error'
						}
					],
					'Not Found'
				);
			const order: Order = result[0];
			if (order.status !== 'suspended')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'You can only send the orders to accountant with status "Suspended"'
						}
					],
					'Forbidden'
				);

			const updateResult = await Order.runAQL(
				aql`
					UPDATE @oKey WITH {status: 'pendingAccountantVerify'} IN Order
					RETURN NEW
			`.query,
				{
					oKey: _key
				}
			);

			if (!updateResult || updateResult.length === 0)
				return new Response(
					{},
					[
						{
							title: 'Bad Request',
							type: 'error',
							content: 'Could not update the order'
						}
					],
					'Bad Request'
				);
			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/accountant',
				'Role/accountingExpert',
				'Role/admin'
			]);
			numbers.forEach((number) =>
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: order._key,
					token10: order?.customer['fullname'],
					template: 'orderRegistered'
				})
			);

			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order is pending accountant verify',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/setOrderSuspended', 'Company')
	async setOrderSuspended_api({ _key }, zone: ZoneObj, socket: Socket) {
		const orderInstance = new Order();
		const filter = [new FilterMDL('==', '_key', _key)];
		let orderList: { totalCount: number; items: Order[] };
		try {
			orderList = await orderInstance.read({ filter }, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
		if (!orderList || orderList.totalCount === 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Order Not Found',
						type: 'error'
					}
				],
				'Not Found'
			);
		}

		const order: Order = orderList.items[0];
		if (order.status !== 'pendingAccountantVerify')
			return new Response(
				{},
				[
					{
						title: 'Forbidden',
						type: 'error',
						content:
							'You can only suspend the orders with status "Pending Accountant Verify"'
					}
				],
				'Forbidden'
			);
		try {
			await orderInstance.update({
				key: order._key,
				data: {
					status: 'suspended'
				}
			});
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order is suspended',
						type: 'info'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						type: 'error',
						content: 'Could not update the order'
					}
				],
				'Bad Request'
			);
		}
	}

	@ActionAccess('Order/getOrderById', 'Company')
	async getOrderById_api({ _key }, zone: ZoneObj, socket: Socket) {
		const orderInstance = new Order();
		const filter = [new FilterMDL('==', '_key', _key)];
		let orderList: { totalCount: number; items: Order[] };
		try {
			orderList = await orderInstance.read({ filter }, zone);
		} catch (e) {
			return new Response(
				{},
				[
					{
						title: 'Bad Request',
						content: 'Couldnt find the order',
						type: 'error'
					}
				],
				'Bad Request',
				[e]
			);
		}
		if (!orderList || orderList.totalCount === 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Order Not Found',
						type: 'error'
					}
				],
				'Not Found'
			);
		} else {
			return new Response(orderList.items[0], [], 'OK');
		}
	}

	@ActionAccess('Order/getReadyToLoad', 'Warehouse')
	async getReadyToLoad_api(
		{ pageSize, page }: ReadMDL<Order>,
		{ key },
		socket: Socket
	) {
		pageSize = pageSize || 10;
		page = page || 1;
		const result = (
			await Order.runAQL(
				aql`
				LET items = (FOR o IN Order
					SORT o._key DESC
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
					FILTER o.status == 'pendingWarehouseLoading' OR
					o.status == 'loading' OR
					o.status == 'loaded' OR
					o.status == 'sent'
					LIMIT @index, @pageSize
					RETURN MERGE(o, 
						{
							items: o.items[* RETURN MERGE(CURRENT,{itemType: DOCUMENT('ItemType' , CURRENT.itemType), itemTypeKey: CURRENT.itemType})],
							customer: DOCUMENT('Customer', o.customer)
						}
					)

				)
				LET totalCount = 
					LENGTH(FOR o in Order
						FILTER 
							LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
							FILTER o.status == 'pendingWarehouseLoading' OR
					o.status == 'loading' OR
					o.status == 'loaded' OR
					o.status == 'sent'
						RETURN o
					)
				RETURN {items, totalCount}
			`.query,
				{ zoneKey: key, pageSize, index: pageSize * (page - 1) }
			)
		)[0];
		return new Response(result);
	}

	@ActionAccess('Order/setOrderToLoading', 'Warehouse')
	async setOrderToLoading_api({ _key }, zone, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
					FOR o IN Order
					FILTER o._key == @orderKey
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0					
					RETURN MERGE(o,{customer: DOCUMENT('Customer', o.customer)})
				`.query,
				{ zoneKey: zone.key, orderKey: _key }
			);

			if (result.length === 0) {
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							type: 'error',
							content: 'Order not found'
						}
					],
					'Not Found'
				);
			}

			const order: Order = result[0];
			if (order.status === 'loading')
				return new Response(
					{},
					[
						{
							title: 'Info',
							content: 'Order is already in loading state',
							type: 'info'
						}
					],
					'OK'
				);
			else if (order.status && order.status !== 'pendingWarehouseLoading')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content:
								'Only orders with pending warehouser loading are able to start loading',
							type: 'error'
						}
					],
					'Forbidden'
				);

			await Order.runAQL(
				aql`
						UPDATE @oKey WITH {status: 'loading'} IN Order 
						RETURN {before: OLD, after: NEW}
					`.query,
				{ oKey: order._key }
			);
			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/salesManager',
				'Role/salesExpert',
				'Role/manager'
			]);
			numbers.forEach((number) => {
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: order._key,
					token10: order.customer['fullname'],
					token20: order.transportInfo['company'],
					template: 'loadingOrder'
				});
			});

			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order is pending for accountant verify',
						type: 'success'
					}
				],
				'OK'
			);
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order is in loading state',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Not Found'
			);
		}
	}

	@ActionAccess('Order/setOrderFinished', 'Warehouse')
	async setOrderFinished_api({ _key }, zone, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
					FOR o IN Order
					FILTER o._key == @orderKey
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0					
					RETURN MERGE(o,{customer:DOCUMENT('Customer',o.customer)})
				`.query,
				{ zoneKey: zone.key, orderKey: _key }
			);

			if (result.length === 0) {
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							type: 'error',
							content: 'Order not found'
						}
					],
					'Not Found'
				);
			}

			const order: Order = result[0];

			if (order.status && order.status !== 'loading')
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							content:
								'Only orders with loading status can be set finished',
							type: 'error'
						}
					],
					'Forbidden'
				);

			const totalCount = order.abstractItems.reduce(
				(acc, item) => (acc = acc + item.totalCount),
				0
			);
			const loadedItemCount = order.abstractItems.reduce(
				(acc, item) =>
					(acc = acc + (item.items ? item.items.length : 0)),
				0
			);
			const loadedPackageItemCount = order.abstractItems.reduce(
				(acc, item) =>
					(acc =
						acc +
						item.packages?.length * item.packingOption.capacity),
				0
			);

			// if (
			// 	order.items.reduce<boolean>(
			// 		(acc, orderItem) =>
			// 			acc ||
			// 			(orderItem.items ? orderItem.items.length : 0) +
			// 				(orderItem.packages
			// 					? orderItem.packages.length *
			// 					  orderItem.packingOption.capacity
			// 					: 0) ===
			// 				orderItem.totalCount,
			// 		false
			// 	)
			// )
			if (totalCount !== loadedItemCount + loadedPackageItemCount)
				return new Response(
					{},
					[
						{
							title: 'Forbidden',
							type: 'error',
							content:
								'The total number of loaded items is not equal to the order details'
						}
					],
					'Forbidden'
				);
			await Order.runAQL(
				aql`
							UPDATE @oKey WITH {status: 'sent', sentTime: DATE_NOW()} IN Order 
							RETURN {before: OLD, after: NEW}
						`.query,
				{ oKey: order._key }
			);

			const numbers = await Controller.getRolePhoneNumbers([
				'Role/admin',
				'Role/supervisor',
				'Role/salesManager',
				'Role/salesExpert',
				'Role/manager'
			]);
			numbers.forEach((number) =>
				this.SMSAPI.VerifyLookup({
					receptor: number,
					token: order._key,
					token10: order.customer['fullname'],
					token20: `نام‌و‌شماره راننده: ${order.transportInfo['driverFullname']} ${order.transportInfo['driverMobile']}\nشماره‌پلاک: ${order.transportInfo?.vehiclePlate?.[3]}-${order.transportInfo?.vehiclePlate?.[2]}-${order.transportInfo?.vehiclePlate?.[1]}-${order.transportInfo?.vehiclePlate?.[0]}`,
					template: 'orderSent'
				})
			);
			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order loading is finished',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Not Found'
			);
		}
	}
	@ActionAccess('Order/setOrderLading', 'Warehouse')
	async setOrderLading_api({ _key, code }, zone, socket: Socket) {
		try {
			const result = await Order.runAQL(
				aql`
					FOR o IN Order
					FILTER o._key == @orderKey AND o.status == 'sent'
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey])>0					
					RETURN o
				`.query,
				{ zoneKey: zone.key, orderKey: _key }
			);

			if (result.length === 0) {
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							type: 'error',
							content: 'Order not found'
						}
					],
					'Not Found'
				);
			}

			const order: Order = result[0];

			const trnsInfo = order.transportInfo;
			trnsInfo.ladingCode = code;
			await Order.runAQL(
				aql`
							UPDATE @oKey WITH {transportInfo: @trnsInfo} IN Order 
							RETURN {before: OLD, after: NEW}
						`.query,
				{ oKey: _key, trnsInfo }
			);

			return new Response(
				{},
				[
					{
						title: 'Success',
						content: 'Order lading code updated',
						type: 'success'
					}
				],
				'OK'
			);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Not Found'
			);
		}
	}

	@ActionAccess('Order/getOrderDetail', 'Warehouse')
	async getOrderDetail_api(
		{ orderKey }: GetOrderDetailMDL,
		{ key },
		socket: Socket
	) {
		const result = await Order.runAQL(
			aql`
				FOR o IN Order
					FILTER o._key == @orderKey
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
					LET items = (FOR $item in o.items
							RETURN MERGE($item, {itemType: DOCUMENT('ItemType' , $item.itemType), itemTypeKey: $item.itemType})
						)
					LET customerArr = (FOR c IN Customer
							FILTER c._key == o.customer
							RETURN c
						)
					RETURN MERGE(o, {items}, {customer: customerArr[0]})
			`.query,
			{ zoneKey: key, orderKey }
		);
		if (result.length === 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content: 'Order not found.',
						type: 'error'
					}
				],
				'Not Found'
			);
		}
		return new Response(result[0]);
	}

	@ActionAccess('Order/updateTransportInfo', 'Warehouse')
	async updateTransportInfo_api(
		{ transportInfo, orderKey }: UpdateTransportInfoMDL,
		{ key },
		socket: Socket
	) {
		const orderUpdateResult = await Order.runAQL(
			aql`
				FOR o IN Order
					FILTER o._key == @orderKey
					FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
					UPDATE o._key WITH {
						transportInfo: @transportInfo
					} IN Order OPTIONS { mergeObjects: true }
					RETURN NEW

				`.query,
			{
				zoneKey: key,
				orderKey,
				transportInfo
				//itemType: packageUpdateResult[0].itemType
			}
		);
		if (orderUpdateResult.length == 0) {
			return new Response(
				{},
				[
					{
						title: 'Not Found',
						content:
							'The order you want to update its transport info not found',
						type: 'error'
					}
				],
				'Not Found'
			);
		} else {
			return new Response({}, [
				{
					title: 'Update',
					content: 'The order transport information updated.',
					type: 'success'
				}
			]);
		}
	}

	@ActionAccess('Order/loadPackage', 'Warehouse')
	async loadPackage_api(
		{ packageKey, orderKey }: LoadPackageMDL,
		{ key },
		socket: Socket
	) {
		if (packageKey.length === 13) {
			//Is Package
			try {
				const trx = await Order.getDB().beginTransaction({
					write: ['Package', 'Item', 'Order']
				});

				const transactionResult: {
					result: boolean;
					data?: {};
					msg: {
						title: string;
						content: string;
						type: 'error' | 'success';
						respType: ResponseStatus;
					};
				} = await trx.step(async () => {
					const packageUpdateResult = (await Order.runAQL(
						aql`
							FOR p IN Package
							FILTER p._key == @packageKey && p.status == 'stored'
							FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
							LET itemType = DOCUMENT(ItemType, p.itemType).title
							UPDATE p._key WITH {
								order: @orderId,
								status:'sold',
								zones: {
									Warehouse: p.zones.Warehouse[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
									Sold: [{k: 'one', e: DATE_NOW()}]
								}
							} IN Package OPTIONS { mergeObjects: true }
							RETURN MERGE(NEW,{itemType})
						`.query,
						{
							zoneKey: key,
							packageKey,
							orderId: 'Order/' + orderKey
						}
					)) as Package[];

					if (packageUpdateResult.length !== 1) {
						return {
							result: false,
							msg: {
								title: 'Package Not Found',
								content:
									'There is no package with this key Or the package can not load.',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					const loadingPackage = packageUpdateResult[0] as Package;
					const itemsCount = loadingPackage.items.length;

					const itemsUpdateResult = (await Order.runAQL(
						aql`
							FOR key IN DOCUMENT(Package,@packageKey).items
							LET i = DOCUMENT(Item, key)
							FILTER LENGTH(i.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
							UPDATE i._key WITH {
								order: @orderId,
								zones: {
									Warehouse: i.zones.Warehouse[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
									Sold: [{k: 'one', e: DATE_NOW()}]
								}
							} IN Item OPTIONS { mergeObjects: true }
							RETURN NEW
						`.query,
						{
							zoneKey: key,
							packageKey,
							orderId: 'Order/' + orderKey
						}
					)) as Package[];

					if (itemsUpdateResult.length !== itemsCount) {
						return {
							result: false,
							msg: {
								title: 'Package Items Problem',
								content:
									'Package Items not updated. Maybe the items are not in your zone.',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					const abstractOrderItems = await Order.runAQL(
						aql`
							FOR o IN Order
								FILTER o._key == @orderKey
								FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
								FOR i IN o.abstractItems
								FILTER i.itemType == DOCUMENT(Package,@packageId).itemType AND
									i.packingOption._key == DOCUMENT(Package,@packageId).packing._key
								RETURN i						
					`.query,
						{
							zoneKey: key,
							packageId: 'Package/' + packageKey,
							orderKey
						}
					);

					if (abstractOrderItems.length <= 0) {
						return {
							result: false,
							msg: {
								title: 'There is no order item for this package.',
								content: '',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					const abstractOrderItem =
						abstractOrderItems[0] as AbstractOrderItem;

					if (
						abstractOrderItem.totalCount <
						abstractOrderItem.packingOption.capacity *
							((abstractOrderItem?.packages?.length || 0) +
								(abstractOrderItem?.items?.length || 0) +
								+1)
					) {
						return {
							result: false,
							msg: {
								title: 'This package will make the order item overflowed.',
								content: '',
								type: 'error',
								respType: 'Forbidden'
							}
						};
					}

					const orderUpdateResult = await Order.runAQL(
						aql`
						FOR o IN Order
						FILTER o._key == @orderKey
						FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						LET itemIndex = POSITION((FOR i IN o.abstractItems
							RETURN
								i.itemType == DOCUMENT(Package,@packageId).itemType AND
								i.packingOption._key == DOCUMENT(Package,@packageId).packing._key
						), true, true)
						FILTER itemIndex >= 0
						UPDATE o._key WITH {
							abstractItems: REPLACE_NTH(o.abstractItems, itemIndex, 
								MERGE(
									o.abstractItems[itemIndex],
									{ 
										packages: PUSH(
											o.abstractItems[itemIndex].packages?
												o.abstractItems[itemIndex].packages:
												[],
											@packageId
										)
									}
								)
							)
						} IN Order OPTIONS { mergeObjects: true }
						RETURN NEW
	
					`.query,
						{
							zoneKey: key,
							packageId: 'Package/' + packageKey,
							orderKey
							//itemType: packageUpdateResult[0].itemType
						}
					);

					if (orderUpdateResult.length !== 1) {
						return {
							result: false,
							msg: {
								title: 'Package Not Found',
								content:
									'There is no package with this key Or the package can not be loaded.',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					return {
						result: true,
						msg: {
							title: 'Successfully loaded',
							type: 'success',
							content: '',
							respType: 'OK'
						},
						data: { package: packageUpdateResult[0] }
					};
				});

				if (transactionResult.result) {
					await trx.commit();
					Object.values(
						OrderController.orderStatusChangeListeners
					).forEach((l) => typeof l == 'function' && l(orderKey));
				} else {
					await trx.abort();
				}
				return new Response(
					transactionResult.data,
					[
						{
							title: transactionResult.msg.title,
							content: transactionResult.msg.content,
							type: transactionResult.msg.type
						}
					],
					transactionResult.msg.respType
				);
			} catch (e) {
				return new Response(
					{},
					[{ title: 'Internal Server Error', type: 'error' }],
					'Internal Server Error',
					[e]
				);
			}
		} else {
			// Is Item
			try {
				const itemKey = packageKey;
				const trx = await Order.getDB().beginTransaction({
					write: ['Package', 'Item', 'Order']
				});

				const transactionResult: {
					result: boolean;
					data?: {};
					msg: {
						title: string;
						content: string;
						type: 'error' | 'success';
						respType: ResponseStatus;
					};
				} = await trx.step(async () => {
					const itemUpdatedResult = (await Order.runAQL(
						aql`
						FOR i IN Item
						FILTER i._key == @itemKey
						FILTER LENGTH(i.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						LET itemType = DOCUMENT(ItemType, i.itemType).title
						UPDATE i._key WITH {
							packageKey: null,
							order: @orderId,
							zones: {
								Warehouse: i.zones.Warehouse[* RETURN MERGE(CURRENT, {l: DATE_NOW()})],
								Sold: [{k: 'one', e: DATE_NOW()}]
							}
					} IN Item OPTIONS { mergeObjects: true, keepNull: false }
					RETURN MERGE(NEW,{itemType})
	
					`.query,
						{
							zoneKey: key,
							itemKey,
							orderId: 'Order/' + orderKey
						}
					)) as Package[];
					if (itemUpdatedResult.length !== 1) {
						return {
							result: false,
							msg: {
								title: 'Item Not Found',
								content:
									'There is no item with this key Or the item can not load.',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					const loadingItem = itemUpdatedResult[0] as Package;

					const updatePackageResult = await Order.runAQL(
						aql`
						FOR p IN Package
							FILTER LENGTH(p.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
							FILTER LENGTH(p.items[* FILTER CURRENT == @itemKey])>0
							UPDATE p._key WITH {
								status: 'stored-opened',
								items: REMOVE_NTH(p.items, POSITION(p.items, @itemKey, true))
							} IN Package
							RETURN NEW

					`.query,
						{
							zoneKey: key,
							itemKey
						}
					);

					if (updatePackageResult.length <= 0) {
						return {
							result: false,
							msg: {
								title: 'Cannot change item package state',
								content: '',
								type: 'error',
								respType: 'Forbidden'
							}
						};
					}

					const abstractOrderItems = await Order.runAQL(
						aql`
					FOR o IN Order
						FILTER o._key == @orderKey
						FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						FOR oi IN o.abstractItems
							LET item = DOCUMENT(Item,@itemId)
							FILTER oi.itemType == DOCUMENT(Package,item.packageKey).itemType AND
								oi.packingOption._key == DOCUMENT(Package,item.packageKey).packing._key
							RETURN oi						
					`.query,
						{
							zoneKey: key,
							itemId: 'Item/' + itemKey,
							orderKey
						}
					);

					if (abstractOrderItems.length <= 0) {
						return {
							result: false,
							msg: {
								title: 'There is no order item for this item.',
								content: '',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					const abstractOrderItem =
						abstractOrderItems[0] as AbstractOrderItem;

					if (
						abstractOrderItem.totalCount <
						abstractOrderItem.packingOption.capacity *
							(abstractOrderItem?.packages?.length || 0) +
							(abstractOrderItem?.items?.length || 0) +
							1
					) {
						return {
							result: false,
							msg: {
								title: 'The order item related to this item is filled.',
								content: '',
								type: 'error',
								respType: 'Forbidden'
							}
						};
					}

					const orderUpdateResult = await Order.runAQL(
						aql`
						FOR o IN Order
						FILTER o._key == @orderKey
						FILTER LENGTH(o.zones.Warehouse[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0
						LET itemIndex = POSITION((FOR i IN o.items
							LET item = DOCUMENT(Item,@itemId)
							RETURN
								i.itemType == DOCUMENT(Package,item.packageKey).itemType AND
								i.packingOption._key == DOCUMENT(Package,item.packageKey).packing._key
						), true, true)
						FILTER itemIndex >= 0
						UPDATE o._key WITH {
							abstractItems: REPLACE_NTH(o.abstractItems, itemIndex, 
								MERGE(
									o.abstractItems[itemIndex],
									{ 
										items: PUSH(
											o.abstractItems[itemIndex].items?
												o.abstractItems[itemIndex].items:
												[],
											@itemId
										)
									}
								)
							)
						} IN Order OPTIONS { mergeObjects: true }
						RETURN NEW
	
					`.query,
						{
							zoneKey: key,
							itemId: 'Item/' + packageKey,
							orderKey
							//itemType: packageUpdateResult[0].itemType
						}
					);

					if (orderUpdateResult.length !== 1) {
						return {
							result: false,
							msg: {
								title: 'Item Not Found',
								content:
									'There is no item with this key Or the item can not be loaded.',
								type: 'error',
								respType: 'Not Found'
							}
						};
					}

					return {
						result: true,
						msg: {
							title: 'Successfully loaded',
							type: 'success',
							content: '',
							respType: 'OK'
						},
						data: { package: itemUpdatedResult[0] }
					};
				});

				if (transactionResult.result) {
					await trx.commit();
					Object.values(
						OrderController.orderStatusChangeListeners
					).forEach((l) => typeof l == 'function' && l(orderKey));
				} else {
					await trx.abort();
				}
				return new Response(
					transactionResult.data,
					[
						{
							title: transactionResult.msg.title,
							content: transactionResult.msg.content,
							type: transactionResult.msg.type
						}
					],
					transactionResult.msg.respType
				);
			} catch (e) {
				return new Response(
					{},
					[{ title: 'Internal Server Error', type: 'error' }],
					'Internal Server Error',
					[e]
				);
			}
		}
	}

	@ActionAccess('Order/loadUnpackedItem', 'Warehouse')
	async loadUnpackedItem_api(
		{ itemKey, orderKey }: LoadUnpackedItemMDL,
		{ key }
	) {
		const item = (await Order.getDB()
			.collection('Item')
			.document(itemKey)) as Item;

		if (item.packageKey) {
		}
	}

	private static orderStatusChangeListeners: Record<
		string,
		(orderId: string) => void
	> = {};

	@ActionAccess('Order/addStatusListener', 'Company')
	addStatusListener_api(data: any, zone, socket: Socket) {
		const listnerId = nanoid();
		OrderController.orderStatusChangeListeners[listnerId] = (orderId) => {
			socket.emit(listnerId, new Response({ id: orderId }));
		};
		socket.on('disconnect', () => {
			OrderController.removeListener(listnerId);
		});
		socket.on('error', () => {
			OrderController.removeListener(listnerId);
		});
		return new Response({ id: listnerId });
	}

	@ActionAccess('Order/removeStatusListener', 'Company')
	removeStatusListener_api({ id }: { id: string }, socket: Socket) {
		OrderController.removeListener(id);
		return new Response({});
	}

	static removeListener(id: string) {
		OrderController.orderStatusChangeListeners = Object.fromEntries(
			Object.entries(OrderController.orderStatusChangeListeners).filter(
				([k]) => k !== id
			)
		);
	}

	@ActionAccess('Order/intervalSales', 'Company')
	async intervalSales_api({ start, end }, zone) {
		try {
			const result = (await Order.runAQL(
				aql`
				LET orders = (FOR o IN Order
					FILTER o.status == 'sent' AND o.sentTime > @start AND o.sentTime < @end 
					FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey]) > 0
					RETURN o)
				RETURN {amount:SUM(orders[*].totalPrice),count: LENGTH(orders)}
			`.query,
				{
					start,
					end,
					zoneKey: zone.key
				}
			)) || [{ amount: 0, count: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/intervalSalesStat', 'Company')
	async intervalSalesStat_api({ start, end }, zone: ZoneObj) {
		try {
			const result = (await Order.runAQL(
				aql`
				LET sentOrders = LENGTH(FOR o IN Order
					FILTER o.status == 'sent' AND o.sentTime > @start AND o.sentTime < @end 
					FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey]) > 0
					RETURN o)
				LET pendingOrders = LENGTH(FOR o IN Order
					FILTER o.createdAt > @start AND o.createdAt < @end 
					AND (o.status == 'pendingAccountantVerify' OR o.status == 'pendingAdminVerify' OR o.status == 'pendingWarehouseLoading')
					FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey]) > 0
					RETURN o)
				LET loadingOrders = LENGTH(FOR o IN Order
					FILTER o.createdAt > @start AND o.createdAt < @end 
					AND o.status == 'loading'
					FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey]) > 0
					RETURN o)
				RETURN {sentOrders,pendingOrders,loadingOrders}
			`.query,
				{
					start,
					end,
					zoneKey: zone.key
				}
			)) || [{ sentOrders: 0, pendingOrders: 0, loadingOrders: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/intervalOrders', 'Company')
	async intervalOrders_api({ start, end }, zone: ZoneObj) {
		try {
			const result = await Order.runAQL(
				aql`
				FOR o IN Order
				FILTER o.status == 'sent' AND o.sentTime > @start AND o.sentTime < @end
				FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey])>0
				SORT o.createdAt
				RETURN {_key: o._key, customer: DOCUMENT('Customer', o.customer), totalPrice: o.totalPrice, createdAt: o.createdAt}
			`.query,
				{
					start,
					end,
					zoneKey: zone.key
				}
			);

			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/dailySales', 'Company')
	async dailySales_api({ year, month }, zone: ZoneObj) {
		const monthDailySales = ItemTypeController.daysIntervals(year, month);
		try {
			const result = await Order.runAQL(
				aql`
				FOR interval IN @monthDailySales
					LET orders = (
						FOR o IN Order
						FILTER o.status == 'sent' AND o.sentTime >= interval.start AND o.sentTime <= interval.end
						FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey])>0
						RETURN o
					)
				RETURN {amount: SUM(orders[*].totalPrice), day: interval.day}
			`.query,
				{
					monthDailySales,
					zoneKey: zone.key
				}
			);

			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Order/monthlySales', 'Company')
	async monthlySales_api({ year }, zone: ZoneObj) {
		const yearMonthIntervals = ItemTypeController.monthIntervals(year);
		try {
			const result = await Order.runAQL(
				aql`
				FOR interval IN @yearMonthIntervals
					LET orders = (
						FOR o IN Order
						FILTER  o.status == 'sent' AND o.sentTime >= interval.start AND o.sentTime <= interval.end
						FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @zoneKey])>0
						RETURN o
					)
				RETURN {amount: SUM(orders[*].totalPrice), month: interval.month}
			`.query,
				{
					yearMonthIntervals,
					zoneKey: zone.key
				}
			);

			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
