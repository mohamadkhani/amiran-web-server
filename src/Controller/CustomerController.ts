import { aql } from 'arangojs';
import { ActionAccess, ZoneObj } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { Customer } from '../Entity/Customer';
import Response from '../Interface/Response';
import GetOrdersMDL from '../Model/Customer/GetOrdersMDL';
import InactiveForMDL from '../Model/Customer/InactiveForMDL';
import SetCustomerStatusMDL from '../Model/Customer/SetCustomerStatusMDL';
import ReadMDL from '../Model/Entity/ReadMDL';
import UpdateMDL from '../Model/Entity/UpdateMDL';
import FindByKey from '../Model/FindByKey';
import { NULL_MDL } from './UserController';

export default class CustomerController extends DataController<Customer> {
	protected defaultEntity = new Customer();
	models: Record<string, Model> = {
		read_api: ReadMDL,
		create_api: Customer,
		update_api: UpdateMDL,
		setCustomerStatus_api: SetCustomerStatusMDL,
		inactiveFor_api: InactiveForMDL,
		topCustomers_api: ReadMDL,
		purchaseTypes_api: FindByKey,
		totalPurchaseTypes_api: NULL_MDL,
		getOrders_api: GetOrdersMDL
	};

	@ActionAccess('Customer/read', 'Company')
	async read_api(data, zone) {
		return super.read_api(data, zone);
	}
	@ActionAccess('Customer/create', 'Company')
	async create_api(data, zone, socket) {
		return super.create_api(data, zone, socket);
	}
	@ActionAccess('Customer/update', 'Company')
	async update_api(data) {
		delete data._key;
		return super.update_api(data);
	}
	@ActionAccess('Customer/setCustomerStatus', 'Company')
	async setCustomerStatus_api({ _key, active }, zone) {
		try {
			const updateResult = await Customer.runAQL(
				aql`
				FOR c IN Customer
				FILTER c._key == @cKey
				FILTER LENGTH(c.zones.Company[* FILTER CURRENT.k == @zoneKey AND !CURRENT.l])>0 
				
				UPDATE c._key WITH {active: @active } IN Customer

				RETURN {before: OLD, after: NEW}
			`.query,
				{ zoneKey: zone.key, cKey: _key, active: active }
			);
			if (updateResult)
				return new Response(
					{},
					[
						{
							title: 'Success',
							content: 'Customer status updated',
							type: 'success'
						}
					],
					'OK'
				);
			else
				return new Response(
					{},
					[
						{
							title: 'Not Found',
							content: 'Customer not found',
							type: 'error'
						}
					],
					'Not Found'
				);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}

	@ActionAccess('Customer/inactiveFor', 'Company')
	async inactiveFor_api({ days }, zone: ZoneObj) {
		const daysLength = days * 24 * 3600 * 1000;
		try {
			const result =
				(await Customer.runAQL(
					aql`
				LET activeCustomers = (FOR c in Customer
					FILTER c.active == true
					FILTER LENGTH(c.zones.Company[* FILTER CURRENT.k==@zoneKey AND !CURRENT.l])>0
					RETURN c)
					
				FOR o IN Order
				FILTER o.customer IN activeCustomers[*]._key
				FILTER (DATE_NOW() - o.createdAt) > @daysLength
				RETURN DOCUMENT('Customer', o.customer)
			`.query,
					{
						zoneKey: zone.key,
						daysLength
					}
				)) || [];
			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Customer/topCustomers', 'Company')
	async topCustomers_api({ page, pageSize }, zone: ZoneObj) {
		page = page || 1;
		pageSize = pageSize || 10;
		try {
			const result = (await Customer.runAQL(
				aql`
				LET activeCustomers = (FOR c in Customer
					FILTER c.active == true
					FILTER LENGTH(c.zones.Company[* FILTER CURRENT.k==@zoneKey AND !CURRENT.l])>0
					RETURN c)
					
				LET items = (FOR c IN activeCustomers
					LET totalPurchase = SUM(
					FOR o IN Order
					FILTER o.customer == c._key
					AND o.status == 'sent'
					RETURN o.totalPrice)
					sort totalPurchase desc
					LIMIT @page, @pageSize
				LET lastTwoPurchase = (
					FOR oo IN Order
					FILTER oo.customer == c._key AND !oo.isGuarantee
					AND oo.status == 'sent'
					SORT oo.sentTime DESC
					LIMIT 0,2
					RETURN {totalPrice:oo.totalPrice, createdAt: oo.sentTime})
				RETURN {customer:c, purchase:totalPurchase, lastTwoPurchase})
				RETURN {items, totalCount: LENGTH(activeCustomers)}
			`.query,
				{
					zoneKey: zone.key,
					page: pageSize * (page - 1),
					pageSize
				}
			)) || [{ items: [], totalCount: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Customer/purchaseTypes', 'Company')
	async purchaseTypes_api({ _key }, zone: ZoneObj) {
		try {
			const result = (await Customer.runAQL(
				aql`
				LET itemTypeRGX = (FOR i IN ItemType
					RETURN {type: i.title,rgx:i.itemRegex})
				
				LET orders = (FOR o IN Order
					FILTER o.customer == @customerKey AND !o.isGuarantee
					RETURN o._id)
				LET guranteeOrders = (FOR o IN Order
					FILTER o.customer == @customerKey AND o.isGuarantee == true
					RETURN o._id)
				
				LET totalPurchase = (FOR i IN Item 
					FILTER i.order IN orders
					FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
					LET type =  (
					FOR it IN itemTypeRGX
					FILTER REGEX_TEST(i._key, it.rgx)
					RETURN it.type)[0]

				COLLECT cnt = type INTO itemsByType
				
				RETURN {
						type: cnt,
						count: length(itemsByType[*])
					})
				LET totalGuarantee = (FOR i IN Item 
					FILTER i.order IN guranteeOrders
					FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
					LET type =  (
					FOR it IN itemTypeRGX
					FILTER REGEX_TEST(i._key, it.rgx)
					RETURN it.type)[0]

				COLLECT cnt = type INTO itemsByType
				
				RETURN {
						type: cnt,
						count: length(itemsByType[*])
					})
				RETURN {totalPurchase, totalGuarantee}
			`.query,
				{
					customerKey: _key,
					cKey: zone.key
				}
			)) || [{ totalPurchase: [], totalGuarantee: [] }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Customer/totalPurchaseTypes', 'Company')
	async totalPurchaseTypes_api({}, zone: ZoneObj) {
		try {
			const result =
				(await Customer.runAQL(
					aql`
						LET itemTypeRGX = (FOR i IN ItemType
						RETURN {type: i.title,rgx:i.itemRegex})
					
						FOR i IN Item 
						FILTER i.order
						FILTER LENGTH(i.zones.Company[* FILTER CURRENT.k == @cKey])>0
						LET type =  (
						FOR it IN itemTypeRGX
						FILTER REGEX_TEST(i._key, it.rgx)
						RETURN it.type)[0]
	
						COLLECT cnt = type INTO itemsByType
						
						RETURN {
								type: cnt,
								count: length(itemsByType[*]),
								
							}
			`.query,
					{
						cKey: zone.key
					}
				)) || [];
			return new Response(result);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
	@ActionAccess('Customer/getOrders', 'Company')
	async getOrders_api({ _key, page, pageSize }, zone: ZoneObj) {
		page = page || 1;
		pageSize = pageSize || 10;
		try {
			const result = (await Customer.runAQL(
				aql`
						Let items = (FOR o in Order
							FILTER o.customer == @customer
							FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @cKey]) > 0
							SORT o.createdAt DESC
							LIMIT @index, @pageSize
							RETURN o
						)
						let totalCount = LENGTH (
							FOR o in Order
							FILTER o.customer == @customer
							FILTER LENGTH(o.zones.Company[* FILTER CURRENT.k == @cKey]) > 0
							RETURN o
						)
						RETURN {items , totalCount}
			`.query,
				{
					customer: _key,
					cKey: zone.key,
					index: (page - 1) * pageSize,
					pageSize
				}
			)) || [{ items: [], totalCount: 0 }];
			return new Response(result[0]);
		} catch (e) {
			return new Response(
				{},
				[{ title: 'Bad Request', type: 'error' }],
				'Bad Request',
				[e]
			);
		}
	}
}
