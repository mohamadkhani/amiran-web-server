import { ActionAccess, ZoneObj } from '../Base/Controller';
import { DataController } from '../Base/DataController';
import Model from '../Base/Model';
import { Role } from '../Entity/Role';

export default class RoleController extends DataController<Role> {
	protected defaultEntity = new Role();
	models: Record<string, Model> = {
		create_api: Role
	};
	@ActionAccess('Role/read', 'Company')
	async read_api(data, zone: ZoneObj) {
		return super.read_api(data, zone);
	}
}
