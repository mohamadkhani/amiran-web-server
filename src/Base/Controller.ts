/* eslint-disable @typescript-eslint/ban-ts-comment */
import Model from './Model';
import Response from '../Interface/Response';
import { validateSync } from 'class-validator';
import { Socket } from 'socket.io';
import { verify } from 'jsonwebtoken';
import { db, ZoneSpecifier } from './Entity';
import { RoleIds, ZoneName } from '../Entity/Role';
import { gIO } from '../APIServer';
import kavenegar from 'kavenegar';
import { aql } from 'arangojs';
import * as dotenv from 'dotenv';
dotenv.config({ path: `${process.cwd()}/.env.${process.env.NODE_ENV}` });

const apiMethodsPostFix = '_api';
export type Command = 'Logout' | 'ReloadZoneTree';
export function ActionAccess(action: ActionsNames, zone: ZoneName) {
	return (object: any, method: string) => {
		// //@ts-ignore
		if (!global.actionAccess) global.actionAccess = {};
		// //@ts-ignore
		global.actionAccess[action] = zone;
	};
}
export default abstract class Controller {
	protected abstract models: Record<string, Model>;
	protected abstract crudModels: Record<string, Model>;

	public findMethod(methodName: string) {
		//@ts-ignore
		return typeof this[methodName] === 'function'
			? //@ts-ignore
			  this[methodName]
			: undefined;
	}
	public SMSAPI = kavenegar.KavenegarApi({
		apikey: process.env.SMSAPIKEY
	});

	public static async getRolePhoneNumbers(roles: RoleIds[]) {
		async function get(): Promise<string[]> {
			const cusror = await db.query(
				aql`
					LET company = DOCUMENT('Company/amiran')
					LET users = (FOR id IN ATTRIBUTES(company.zoneAccess)
					FILTER company.zoneAccess[id] IN @roles
					RETURN id)
					FOR u IN users
					RETURN DOCUMENT(u).mobile
			`.query,
				{
					roles
				}
			);
			return await cusror.all();
		}
		const result = await get();
		return result;
		// .reduce(
		// 	(acc, item, idx) =>
		// 		(acc = acc + item + (idx < result.length - 1 ? ',' : '')),
		// 	''
		// );
	}

	public getUserInfo(socket: Socket): {
		userKey: string;
		roles: Record<string, string>[];
	} {
		const token = Controller.userInfo[socket['id']]?.token;
		return verify(token, process.env.ACCESS_TOKEN_SECERT as string, {}) as {
			userKey: string;
			roles: [
				{
					title: string;
					key: string;
				}
			];
		};
	}
	static userInfo: Record<string, { token: string; userKey?: string }> = {};

	static sendCommandToAllUsers(command: Command) {
		Object.entries(Controller.userInfo).forEach(([socketId, $userInfo]) => {
			gIO.to(socketId).emit($userInfo.userKey, new Response({ command }));
		});
	}

	public async isUserPermitted(
		userKey: string,
		zone: ZoneObj,
		model: string,
		action: string
	): Promise<boolean | string> {
		if (!zone?.key || !zone?.name) {
			return 'Please specify a zone.';
		}
		const actionKey = model + '/' + action;
		const theRequiredZone: ZoneName = global.actionAccess[actionKey];
		const theRequiredZonesObj = ZoneTree[theRequiredZone];
		if (!theRequiredZonesObj) return 'theRequiredZonesObj is false';

		let zoneObj: {
			zones?: Record<ZoneName, ZoneSpecifier[]>;
			zoneAccess: Record<string, string>;
		};

		try {
			zoneObj = await db.collection(zone.name).document(zone.key);
		} catch (e) {
			return e + 'error 84';
		}
		if (!zoneObj) return 'The zone you want to access not found.';

		let zonesRoles: { zoneName: ZoneName; roleId: string }[] = [];

		const zoneObjZoneAccess = zoneObj.zoneAccess;
		if (!zoneObjZoneAccess['User/' + userKey]) {
			return "You don't have the role to access this zone";
		}

		if (!zoneObjZoneAccess && !zoneObjZoneAccess['User/' + userKey])
			return "You don't have the role to access this zone";
		else
			zonesRoles.push({
				zoneName: zone.name,
				roleId: zoneObjZoneAccess['User/' + userKey]
			});

		const ttt: Partial<Record<ZoneName, ZoneSpecifier>> = {};
		const hasTheRequiredZones = await theRequiredZonesObj?.path?.reduce(
			(acm, item) => {
				const zoneObjZones = zoneObj.zones;
				if (!zoneObjZones) return false;
				const zoneArr = zoneObjZones[item];
				const hasNullLeft = zoneArr.reduce((acm2, item2) => {
					if (!item2.l) ttt[item] = item2;
					return acm2 || !item2.l;
				}, false);
				return acm && hasNullLeft;
			},
			true
		);

		if (!hasTheRequiredZones) return 'hasTheRequiredZones = false';
		try {
			const pathZonesRoles = await Object.keys(ttt).reduce<
				//This will create array of RoleId
				Promise<({ zoneName: ZoneName; roleId: string } | false)[]>
			>(async (acc, zoneName) => {
				const zoneDoc = await db
					.collection(zoneName)
					.document(ttt[zoneName].k);
				return [
					...(await acc),
					!!zoneDoc &&
						!!zoneDoc['zoneAccess'] &&
						!!zoneDoc['zoneAccess']['User/' + userKey] && {
							zoneName: zoneName as ZoneName,
							roleId: zoneDoc['zoneAccess']['User/' + userKey]
						}
				];
			}, Promise.resolve([]) as Promise<({ zoneName: ZoneName; roleId: string } | false)[]>);

			if (pathZonesRoles.includes(false)) {
				// One of path zones don't have the user role in its zoneAccess
				return "One of path zones don't have the user role in its zoneAccess";
			}

			//@ts-ignore
			zonesRoles = [...zonesRoles, ...pathZonesRoles];

			return await zonesRoles?.reduce(
				async (acc, { roleId, zoneName }) => {
					let hasActionAccess = false; // Action Access exists in the Role Zone.actions

					const roleDoc = await db
						.collection('Role')
						.document(roleId);

					hasActionAccess =
						!!roleDoc['accessList'][zoneName]?.actions.includes(
							actionKey
						);
					if (!hasActionAccess)
						return `You don\'t have access to the action in the ${zoneName} zone.`;

					return (await acc) && hasActionAccess;
				},
				Promise.resolve(true)
			);
		} catch (e) {
			return 'error' + e;
		}
	}

	public async runApiMethodFn(
		model: string,
		action: string,
		zone: ZoneObj,
		req: Record<string, any>,
		socket: Socket
	) {
		const ze: ZoneObj = zone;
		const actionAccessKey = model + '/' + action;
		if (!global.actionAccess[actionAccessKey])
			return new Response(
				{},
				[{ title: 'Method Not Found', type: 'error' }],
				'Bad Request'
			);

		const apiAction = action + apiMethodsPostFix;
		const method = this.findMethod(apiAction);
		if (!method) {
			return new Response(
				null,
				[{ title: 'Wrong Method', type: 'error' }],
				'Not Found'
			);
		}
		//  Access Control ----------- Start
		if (
			apiAction !== 'login_api' &&
			apiAction !== 'logout_api' &&
			apiAction !== 'authenticate_api'
		) {
			const userKey = this.getUserInfo(socket)?.userKey;
			const isPermitted = await this.isUserPermitted(
				userKey,
				ze,
				model,
				action
			);
			if (isPermitted != true)
				return new Response(
					{},
					[
						{
							title: 'Permission Denied',
							content:
								"You don't have the required access to this action. \n" +
								(isPermitted || ''),
							type: 'error',
							devMsg: [
								...(typeof isPermitted === 'string'
									? [isPermitted]
									: [])
							]
						}
					],
					'Forbidden'
				);
		}
		//  Access Control ----------- End

		const models = { ...this.models, ...this.crudModels };
		let instance: Model;
		try {
			//@ts-ignore
			const entity = this.defaultEntity;

			if (entity) {
				// @ts-ignore
				instance = new models[apiAction](entity);
			} else {
				// @ts-ignore
				instance = new models[apiAction]();
			}
		} catch (e) {
			return new Response(
				null,
				[{ title: 'Wrong Method', type: 'error' }],
				'Not Found',
				['instance problem']
			);
		}
		if (!(instance instanceof Model)) {
			return new Response(
				null,
				[{ title: 'Regarding Model Not Found.', type: 'error' }],
				'Not Found'
			);
		}
		// assigning JSON object props to model instance props
		// for preparing it for validation
		Object.entries(req).reduce((instance, [prop, value]) => {
			//@ts-ignore
			instance[prop] = value;
			return instance;
		}, instance);
		let validationResult = null;
		if (apiAction === 'create_api') {
			validationResult = validateSync(instance, {
				groups: ['create']
			});
		} else {
			validationResult = validateSync(instance);
		}
		if (validationResult.length > 0) {
			return new Response(
				null,
				[
					{
						title: 'validation issue',
						type: 'error',
						content: validationResult.reduce(
							(acm, item) =>
								item.constraints
									? Object.entries(item.constraints).reduce(
											(acm2, [key, cnst]) =>
												` ${key}: ${cnst} `,
											''
									  )
									: '',
							''
						)
					}
				],
				'Not Found'
			);
		}
		return await method.bind(this)(req, zone, socket);
	}
}

export type ActionsNames =
	| 'Company/getZoneAccessedPeople'
	| 'Company/paralyse'
	| 'Company/mobilize'
	| 'Company/quarantine'
	| 'Company/getStatus'
	| 'Company/getSMSCredit'
	| 'User/read'
	| 'User/getProfile'
	| 'User/login'
	| 'User/logout'
	| 'User/authenticate'
	| 'User/fetchZones'
	| 'User/fetchWorkers'
	| 'User/fetchZoneTree'
	| 'User/getUserRole'
	| 'User/getAccess'
	| 'User/getRoleInfo'
	| 'User/getStaff'
	| 'User/getProvinces'
	| 'User/getCities'
	| 'User/resetPass'
	| 'User/setPass'
	| 'Item/create'
	| 'Item/delete'
	| 'Item/findPackage'
	| 'Item/find'
	| 'Item/findForGuarantee'
	| 'Item/findForBreakdownReason'
	| 'Item/getBrokenDowns'
	| 'Item/setBreakReason'
	| 'ItemSpec/read'
	| 'ItemType/create'
	| 'ItemType/read'
	| 'ItemType/update'
	| 'ItemType/remove'
	| 'ItemType/read'
	| 'ItemType/update'
	| 'ItemType/create'
	| 'ItemType/setPrice'
	| 'ItemType/getSalesStat'
	| 'ItemType/getProductionStat'
	| 'ItemType/getProductionDailyStat'
	| 'ItemType/customersPurchase'
	| 'Item/findTheGreatestItemCode'
	| 'Item/purchasedProducedStat'
	| 'Package/moveToWarehouse'
	| 'Package/read'
	| 'Package/update'
	| 'Package/getAPackage'
	| 'Package/findAPackageByItem'
	| 'Package/addRegListener'
	| 'Package/removeRegListener'
	| 'Package/getLinePackages'
	| 'Package/packageList'
	| 'Package/label'
	| 'Package/deleteFilling'
	| 'Package/findForWarehouseReception'
	| 'Package/findForWarehouseChecking'
	| 'Package/findFillingStored'
	| 'Package/findFilling'
	| 'Package/pmPackageList'
	| 'Package/pmLabel'
	| 'Role/read'
	| 'Shift/create'
	| 'Shift/read'
	| 'Shift/update'
	| 'Shift/addStatusListener'
	| 'Shift/removeStatusListener'
	| 'Shift/start'
	| 'Shift/pause'
	| 'Shift/stop'
	| 'Shift/find'
	| 'Shift/getInfo'
	| 'Shift/qCVerify'
	| 'Shift/getShiftRoles'
	| 'Shift/getAssignableWorkers'
	| 'Shift/getAdminShiftsHistory'
	| 'Shift/getQCShiftsList'
	| 'Shift/updateWorkersRoles'
	| 'ShiftRole/read'
	| 'ProductionLine/getCurrentShift'
	| 'ProductionLine/estimate'
	| 'ProductionLine/scanmansStat'
	| 'ProductionLine/gaugeStat'
	| 'ProductionLine/productionGraph'
	| 'ProductionLine/penaltimateSameKindShift'
	| 'Warehouse/getVirtualShifts'
	| 'Warehouse/getSummary'
	| 'WarehouseReception/readd'
	| 'Warehouse/fetchDrivers'
	| 'Warehouse/criticalInfo'
	| 'Warehouse/getSummaryInGuaranteeSpan'
	| 'Warehouse/getSummaryInLodgingSpan'
	| 'Customer/read'
	| 'Customer/create'
	| 'Customer/update'
	| 'Customer/setCustomerStatus'
	| 'Customer/getPurchaseStat'
	| 'Customer/inactiveFor'
	| 'Customer/topCustomers'
	| 'Customer/purchaseTypes'
	| 'Customer/totalPurchaseTypes'
	| 'Customer/getOrders'
	| 'Order/create'
	| 'Order/read'
	| 'Order/readd'
	| 'Order/update'
	| 'Order/adminVerify'
	| 'Order/adminReject'
	| 'Order/adminCancel'
	| 'Order/accVerify'
	| 'Order/accReject'
	| 'Order/getReadyToLoad'
	| 'Order/loadUnpackedItem'
	| 'Order/getOrderDetail'
	| 'Order/loadPackage'
	| 'Order/updateTransportInfo'
	| 'Order/cancel'
	| 'Order/setOrderPendingAcc'
	| 'Order/setOrderSuspended'
	| 'Order/setOrderToLoading'
	| 'Order/setOrderFinished'
	| 'Order/loadItm'
	| 'Order/isPackageLoadable'
	| 'Order/isItemLoadable'
	| 'Order/addStatusListener'
	| 'Order/removeStatusListener'
	| 'Order/setOrderLading'
	| 'Order/getOrderById'
	| 'Order/intervalSales'
	| 'Order/intervalSalesStat'
	| 'Order/intervalOrders'
	| 'Order/monthlySales'
	| 'Order/dailySales'
	| 'GuaranteeReception/read'
	| 'GuaranteeReception/create';

export type ZoneObj = { name: ZoneName; key: string };

export const ZoneTree: Partial<
	Record<ZoneName, { key: string; name: string; path: string[] }>
> = {
	Company: {
		key: 'company',
		name: 'Company',
		path: []
	},
	ProductionLine: {
		key: 'productionLine',
		name: 'ProductionLine',
		path: ['Company']
	},
	Shift: {
		key: 'shift',
		name: 'Shift',
		path: ['Company', 'ProductionLine']
	},
	Warehouse: {
		key: 'warehouse',
		name: 'Warehouse',
		path: ['Company']
	},
	ShiftInterval: {
		key: 'shiftInterval',
		name: 'ShiftInterval',
		path: ['Company', 'ProductionLine', 'Shift']
	}
};
