import Response from '../Interface/Response';
import Entity from './Entity';

import ReadMDL from '../Model/Entity/ReadMDL';
import UpdateMDL from '../Model/Entity/UpdateMDL';

import Model from './Model';
import Controller, { ZoneObj } from './Controller';
import { Socket } from 'socket.io';
import { RemoveMDL } from '../Model/Entity/RemoveMDL';

export abstract class DataController<Ntity> extends Controller {
	protected abstract defaultEntity: Entity;

	public data: Record<string, any> = {};

	crudModels: Record<string, Model> = {
		read_api: ReadMDL,
		update_api: UpdateMDL,
		remove_api: RemoveMDL
		// create_api: CreateMDL,
		// delete_api: DeleteMDL
	};

	public async read_api(readReq: ReadMDL<Ntity>, zone: ZoneObj) {
		try {
			//@ts-ignore todo
			return new Response(await this.defaultEntity.read(readReq, zone));
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to read the item.',
						content:
							e === 'Not Found'
								? 'Item not found.'
								: 'Unknown error occurred.',
						type: 'error'
					}
				],
				e === 'Not Found' ? e : 'Internal Server Error',
				[e]
			);
		}
	}

	public async getAll_api(): Promise<Response> {
		try {
			return new Response(await this.defaultEntity.getAll());
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to read the items.',
						content: 'Unknown error occurred.',
						type: 'error'
					}
				],
				'Internal Server Error',
				[e]
			);
		}
	}

	public async create_api(data: Ntity, zone: ZoneObj, socket: Socket) {
		const userInfo = this.getUserInfo(socket);

		this.defaultEntity.projectFrom(data);
		try {
			return new Response(await this.defaultEntity.create(userInfo, zone), [
				{
					title: 'Item Created.',
					type: 'success'
				}
			]);
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to create the item.',
						content: 'Unknown error occurred.',
						type: 'error'
					}
				],
				'Internal Server Error',
				[e]
			);
		}
	}

	public async update_api(data: UpdateMDL<Ntity>) {
		try {
			return new Response(
				await this.defaultEntity.update({
					key: data.key,
					data: data.data
				}),
				[
					{
						title: 'Item Updated.',
						type: 'success'
					}
				]
			);
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to update item.',
						content:
							e === 'Not Found'
								? 'Item not found.'
								: 'Unknown error occurred.',
						type: 'error'
					}
				],
				e === 'Not Found' ? e : 'Internal Server Error',
				[e]
			);
		}
	}

	public async replace_api(data: any) {
		try {
			return new Response(await this.defaultEntity.replace('', data), [
				{
					title: 'Item replaced.',
					type: 'success'
				}
			]);
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to replace the item.',
						content:
							e === 'Not Found'
								? 'Item not found.'
								: 'Unknown error occurred.',
						type: 'error'
					}
				],
				e === 'Not Found' ? e : 'Internal Server Error',
				[e]
			);
		}
	}

	public async remove_api(data: any, zone: ZoneObj, socket: Socket) {
		try {
			return new Response(await this.defaultEntity.remove(data), [
				{
					title: 'Item Deleted.',
					type: 'success'
				}
			]);
		} catch (e) {
			return new Response(
				null,
				[
					{
						title: 'Unable to delete the item.',
						content:
							e === 'Not Found'
								? 'Item not found.'
								: 'Unknown error occurred.',
						type: 'error'
					}
				],
				e === 'Not Found' ? e : 'Internal Server Error',
				[e]
			);
		}
	}
}
