import { Database, aql } from 'arangojs';
import { ArangojsError } from 'arangojs/lib/request.node';
import { FilterMDL, SortMDL } from '../Model/Entity/ReadMDL';
import Model from './Model';
import _ from 'lodash';
import moment from 'moment-jalaali';
import * as dotenv from 'dotenv';
dotenv.config({ path: `${process.cwd()}/.env.${process.env.NODE_ENV}` });
import { Dict } from 'arangojs/connection';
import { ZoneObj } from './Controller';
import { ZoneName } from '../Entity/Role';
export type ZoneSpecifier = { k: string; e: number; l: number }; // 'k' is ZoneKey | 'e' is Zone enterance date | 'l' is Zone leaveness date

export const C = ['create'];
export const D = ['delete'];
export const U = ['update'];
export const F = ['filter'];
export const CU = ['create', 'update'];
export const CF = ['create', 'filter'];
export const CUF = ['create', 'update', 'filter'];
export const CFD = ['create', 'filter', 'delete'];
export let db: Database | null = null;
try {
	if (process.env.NODE_ENV === 'development') {
		console.log(
			'server is in development mode. DB -> ' + process.env.DB_NAME
		);
	}
	db = new Database({
		url: process.env.DB_URL,
		databaseName: process.env.DB_NAME,
		auth: {
			username: process.env.DB_USERNAME,
			password: process.env.DB_PASSWORD
		}
	});
} catch (e) {
	throw {
		message: (e as ArangojsError).message,
		name: (e as ArangojsError).name,
		stack: (e as ArangojsError).stack
	};
}

abstract class Entity extends Model {
	public constructor() {
		super();
	}

	public _collectionName?: string;

	public projectFrom(plainObject: Record<string, any>) {
		Object.keys(this)
			.filter((prop) => !prop.startsWith('_'))
			.forEach((item) => {
				if (plainObject.hasOwnProperty(item))
					this[item] = plainObject[item];
			});
	}

	async getAll(): Promise<Entity[]> {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}
		try {
			const collection = db.collection(this._collectionName);
			const users = await db.query(aql`
              FOR item IN ${collection}
              RETURN item
            `);
			return (await users.all()).map((item) => {
				this.projectFrom(item);
				const obj = _.clone(this);
				return obj;
			});
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	async create(userInfo: { userKey: string }, zone: ZoneObj) {
		//this.zones = {};
		try {
			let zones = (await db.collection(zone.name).document(zone.key))
				.zones as Partial<Record<ZoneName, ZoneSpecifier[]>>;

			zones = zones
				? Object.fromEntries(
						Object.entries(zones).map(
							([zoneName, ZoneSpecifiers]) => {
								return [
									zoneName,
									ZoneSpecifiers.filter((zs) => !zs.l)
								];
							}
						)
				  )
				: undefined;

			this.zones = {
				...zones,
				[zone.name]: [{ k: zone.key, e: moment().valueOf() }]
			};

			if (!this._collectionName)
				throw 'Collection name is not specified.';
			if (Object.keys(this).includes('createdBy'))
				this['createdBy'] = userInfo.userKey;
			if (Object.keys(this).includes('createdAt'))
				this['createdAt'] = moment().valueOf();
			const data = {};
			Object.keys(this)
				.filter(
					(item) =>
						item !== '_createdAt' &&
						item !== '_createdBy' &&
						item !== '_collectionName'
				)
				.forEach((item) => (data[item] = this[item]));
			if (!this['_key']) delete data['_key'];
			const collection = db.collection(this._collectionName);

			const inserted = await db.query(aql`
              INSERT ${data}
              IN ${collection}
              RETURN NEW
            `);
			return (await inserted.map((doc) => doc)).reduce(
				(acm, item) => item,
				''
			);
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	async update({ key, data }: { key: string; data: Record<string, any> }) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}
		try {
			const collection = db.collection(this._collectionName);

			const updated = await db.query(aql`
            	UPDATE ${key} WITH ${data}  
				IN ${collection}
				RETURN { before: OLD, after: NEW }
            `);
			return (await updated.map((doc) => doc)).reduce(
				(acm, item) => item,
				''
			);
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	async replace(key: string, data: any) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}
		try {
			const collection = db.collection(this._collectionName);
			const inserted = await db.query(aql`
            	REPLACE ${key} WITH ${data}  
              IN ${collection}
              RETURN { before: OLD, after: NEW }
            `);
			return (await inserted.map((doc) => doc)).reduce(
				(acm, item) => item,
				''
			);
		} catch (e) {
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	async read(
		data: {
			pageSize?: number;
			page?: number;
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			//@ts-ignore
			filter?: FilterMDL[];
			sort?: SortMDL[];
		} = {
			pageSize: 10,
			page: 1,
			filter: [],
			sort: []
		},
		zone: ZoneObj
	) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}

		if (typeof data === 'string') {
			let result;
			try {
				const cursor = await db.query(
					`RETURN DOCUMENT("` +
						this._collectionName +
						`/` +
						data +
						`")`
				);
				result = await cursor.reduce((acm, item) => item, undefined);
			} catch (e) {
				throw {
					message: (e as ArangojsError).message,
					name: (e as ArangojsError).name,
					stack: (e as ArangojsError).stack
				};
			}
			if (result === undefined || result === null) throw 'Not Found';
			else return result;
		} else {
			// LIST REQUESTED
			data = {
				...data,
				pageSize: data.pageSize || 10,
				page: data.page ? data.page - 1 : 0
			};

			try {
				const collection = db.collection(this._collectionName);
				const query1 = `
				let items = (FOR u in ${collection.name}
				${!!data?.sort?.length ? 'SORT' : ''}
				${
					!!data?.sort?.length
						? data?.sort?.reduce(
								(acm, sortItem) =>
									(acm ? acm + ', ' : '') +
									('u.' +
										sortItem.key +
										'' +
										(sortItem.desc ? ' DESC ' : '')),
								''
						  )
						: ''
				}
				FILTER LENGTH(u.zones.${zone.name}[* FILTER CURRENT.k == "${
					zone.key
				}" AND !CURRENT.l])>0

				FILTER ${
					data?.filter
						?.map(({ op, value, key }) =>
							op === 'IN' || op === 'NOT IN'
								? '"' + value + '" ' + op + ' u.' + key
								: 'u.' +
								  key +
								  ' ' +
								  op +
								  (typeof value === 'string'
										? ' "' + value + '"'
										: value)
						)
						.reduce(
							(strFilters, strFilter) =>
								strFilters +
								(strFilters ? ' AND ' : '') +
								strFilter,
							''
						) || ' true '
				}
					LIMIT ${data.page * data.pageSize},${data.pageSize}
					RETURN u)


					let totalCount = LENGTH(FOR u in ${collection.name}
						${!!data?.sort?.length ? 'SORT' : ''}
						${
							!!data?.sort?.length
								? data?.sort?.reduce(
										(acm, sortItem) =>
											(acm ? acm + ', ' : '') +
											('u.' +
												sortItem.key +
												'' +
												(sortItem.desc
													? ' DESC '
													: '')),
										''
								  )
								: ''
						}
						FILTER LENGTH(u.zones.${zone.name}[* FILTER CURRENT.k == "${
					zone.key
				}" AND !CURRENT.l])>0
						FILTER ${
							data?.filter
								?.map(({ op, value, key }) =>
									op === 'IN' || op === 'NOT IN'
										? '"' + value + '" ' + op + ' u.' + key
										: 'u.' +
										  key +
										  ' ' +
										  op +
										  (typeof value === 'string'
												? ' "' + value + '"'
												: value)
								)
								.reduce(
									(strFilters, strFilter) =>
										strFilters +
										(strFilters ? ' AND ' : '') +
										strFilter,
									''
								) || ' true '
						}
							RETURN u)
						RETURN {items, totalCount}


			`;
				const cursor = await db.query(query1);
				const res = await cursor.all();
				if (res.length == 1) {
					return {
						items: res[0].items,
						totalCount: res[0].totalCount
					};
				} else {
					throw 'Error in running read query.';
				}
			} catch (e) {
				return e;
			}
		}
	}

	async remove({ _key }: { _key: string }) {
		if (!this._collectionName) {
			throw 'Collection name is not specified.';
		}
		try {
			const collection = db.collection(this._collectionName);
			const result = await collection.removeByKeys([_key]);

			return result.removed == 1;
		} catch (e) {
			if (
				(e as ArangojsError).message ===
				'AQL: document not found (while executing)'
			) {
				throw 'Not Found';
			}
			throw {
				message: (e as ArangojsError).message,
				name: (e as ArangojsError).name,
				stack: (e as ArangojsError).stack
			};
		}
	}

	static async runAQL(query: string, bindVars: Dict<any>) {
		const cursor = await db.query(query, bindVars);
		return await cursor.all();
	}

	static getDB() {
		return db;
	}

	zones: Partial<Record<ZoneName, ZoneSpecifier[]>>;
}

export default Entity;
