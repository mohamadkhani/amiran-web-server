import { createServer } from 'http';
import { Server, Socket } from 'socket.io';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import UserController from './Controller/UserController';
import Request from './Interface/Request';

export let gIO: Server<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap>; 
class APIServer {
	static socket: Socket;

	constructor(PORT: number, opts: Record<string, any>) {
		const httpServer = createServer();
		const io = new Server(httpServer, opts);
		gIO = io;
		io.on('connection', (socket: Socket) => {
			APIServer.socket = socket;
			socket.on(
				'request',
				(request: any, callback: (resp: any) => void, arg3) => {
					const req = new Request(request, socket);
					req.exec().then((resp) => {
						callback(resp);
					});
				}
			);
			//FIXME An additional one is defined in the UserController but it dosnt trigger
			socket.on('disconnect', () => {
				delete UserController.userInfo[socket.id];
			});
		});

		httpServer.listen(PORT, () => console.log(`listening on *:${PORT}`));
	}
}

export default APIServer;
