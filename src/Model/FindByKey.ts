import { IsDefined, IsString, Length } from 'class-validator';
import Model from '../Base/Model';

export default class FindByKey extends Model {
	@IsDefined()
	@IsString()
	_key: string = null;
}
