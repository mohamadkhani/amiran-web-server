import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class FindPackage extends Model {
	@IsDefined()
	@IsString()
	_key: string;

	@IsDefined()
	@IsString()
	shiftCode: string;
}
