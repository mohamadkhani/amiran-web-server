import { IsArray, IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class SetBreakReasonMDL extends Model {
	@IsDefined()
	@IsString()
	_key: string;

	@IsDefined()
	@IsArray()
	reasons: string[];
}
