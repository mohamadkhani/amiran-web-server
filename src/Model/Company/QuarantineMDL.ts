import { IsArray, IsDefined } from 'class-validator';
import Model from '../../Base/Model';

export default class QuarantineMDL extends Model {
	@IsDefined()
	@IsArray()
	staff: string[] = [];
}
