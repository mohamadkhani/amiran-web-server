import { IsArray, IsDefined, IsObject, IsString } from 'class-validator';
import Model from '../../Base/Model';
import { Role } from '../../Entity/Role';

export class UpdateWorkersRolesMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;

	@IsDefined()
	@IsObject()
	public zoneAccess?: Record<string, string> = {};
}
