import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class QCVerifyMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;

	@IsDefined()
	@IsString()
	public report: string = null;
}
