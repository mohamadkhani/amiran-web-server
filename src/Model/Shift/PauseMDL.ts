import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class PauseMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;
}
