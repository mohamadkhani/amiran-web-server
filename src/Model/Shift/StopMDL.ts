import { IsBoolean, IsDefined, IsOptional, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class StopMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;


	@IsDefined()
	@IsString()
	public report: string = null;

	@IsOptional()
	@IsBoolean()
	public force: boolean = false;

}
