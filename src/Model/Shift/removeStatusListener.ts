import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class RemoveStatusListener extends Model {
	@IsDefined()
	@IsString()
	id: string = null;
}
