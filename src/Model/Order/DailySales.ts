import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class DailySalesMDL extends Model {
	@IsDefined()
	@IsNumber()
	public year: number = null;
	@IsDefined()
	@IsNumber()
	public month: number = null;
}
