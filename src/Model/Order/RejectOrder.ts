import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class RejectOrder extends Model {
	@IsDefined()
	@IsString()
	public _key: string = null;
}
