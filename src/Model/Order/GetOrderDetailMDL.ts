import { IsDefined, IsNumberString, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class GetOrderDetailMDL extends Model {
	@IsDefined()
	@IsString()
    @IsNumberString()
	public orderKey: string = null;
}
