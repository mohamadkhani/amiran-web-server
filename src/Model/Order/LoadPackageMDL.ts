import { IsDefined, IsNumber, IsNumberString, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class LoadPackageMDL extends Model {
	@IsDefined()
	@IsString()
	@IsNumberString()
	public orderKey: string = null;

	@IsDefined()
	@IsString()
	public packageKey: string = null;
}
