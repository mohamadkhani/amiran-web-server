import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class MonthlySalesMDL extends Model {
	@IsDefined()
	@IsNumber()
	public year: number = null;
}
