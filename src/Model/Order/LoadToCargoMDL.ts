import {
	IsDefined,
	IsNumber,
	IsNumberString,
	IsObject,
	IsString
} from 'class-validator';
import Model from '../../Base/Model';

export class LoadToCargoMDL extends Model {
	@IsDefined()
	@IsString()
	@IsNumberString()
	public orderKey: string = null;

	@IsDefined()
	@IsString()
	public _key: string = null;
}
