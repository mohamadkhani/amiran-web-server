import {
	IsDefined,
	IsNumber,
	IsNumberString,
	IsObject,
	IsString
} from 'class-validator';
import Model from '../../Base/Model';

export class SetLadingCodeMDL extends Model {
	@IsDefined()
	@IsString()
	public _key: string = null;

	@IsDefined()
	@IsString()
	public code: string = null;
}
