import {
	IsDefined,
	IsNumber,
	IsNumberString,
	IsObject,
	IsString
} from 'class-validator';
import Model from '../../Base/Model';

export type TransportType = 'busTransfer' | 'cargo';

export interface TransportInfo {
	transportType?: TransportType;
	vehiclePlate?: [number, string, number, number];
	driverFullname: string | null;
	driverMobile: string | null;
	driverLadingCode: string | null;
}

export class UpdateTransportInfoMDL extends Model {
	@IsDefined()
	@IsString()
	@IsNumberString()
	public orderKey: string = null;

	@IsDefined()
	@IsObject()
	public transportInfo: TransportInfo = null;
}
