import { IsDefined, IsString, Length } from 'class-validator';
import Model from '../../Base/Model';

export default class ResetPassMDL extends Model {
	@IsDefined()
	@IsString()
	oldPass!: string;

	@IsDefined()
	@IsString()
	@Length(5, 20)
	newPass!: string;
}
