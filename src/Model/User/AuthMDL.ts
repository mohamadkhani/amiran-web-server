import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class AuthMDL extends Model {
	constructor() {
		super();
	}

	@IsDefined()
	@IsString()
	token!: string;
}
