import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class LoginMDL extends Model {
	constructor() {
		super();
	}

	@IsDefined()
	@IsString()
	username!: string;

	@IsDefined()
	@IsString()
	password!: string;
}
