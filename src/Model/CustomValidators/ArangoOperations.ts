import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export type ArangoOperationsType =
	| '=='
	| '!='
	| '<'
	| '<='
	| '>'
	| '>='
	| 'IN'
	| 'NOT IN'
	| 'LIKE';
const operations = ['==', '!=', '<', '<=', '>', '>=', 'IN', 'NOT IN', 'LIKE'];
export function ArangoOperations(
	excludes: string[] = [],
	validationOptions?: ValidationOptions
) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid operation name'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'ArangoOperations',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: string) {
					let ops = [...operations];
					ops = operations.filter((item) => !excludes.includes(item));
					return ops.includes(value);
				}
			}
		});
	};
}
