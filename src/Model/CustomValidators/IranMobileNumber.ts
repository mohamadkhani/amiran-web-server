import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function IranMobileNumber(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid Mobile Number'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'IranMobileNumber',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: string) {
					return /^(0)?9\d{9}$/.test(value);
				}
			}
		});
	};
}
