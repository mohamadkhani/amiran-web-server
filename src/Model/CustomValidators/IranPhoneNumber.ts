import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function IranPhoneNumber(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid Phone Number'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'IranPhoneNumber',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: string) {
					return /^0[1-8][0-9]{9}$/.test(value);
				}
			}
		});
	};
}
