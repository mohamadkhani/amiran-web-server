import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function Password(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message:
			'Password must contain at least 8 characters, at least ' +
			'one number and both lower and uppercase letters'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'Password',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: any, arg) {
					return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(value);
				}
			}
		});
	};
}
