import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function Gender(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid Gender Type'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'Gender',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: string) {
					return value === 'male' || value === 'female';
				}
			}
		});
	};
}
