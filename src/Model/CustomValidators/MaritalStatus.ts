import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function MaritalStatus(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid Marital Status'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'MaritalStatus',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: any) {
					return value === 'single' || value === 'married';
				}
			}
		});
	};
}
