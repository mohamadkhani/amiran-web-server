import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function NationalCode(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid National Code'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'NationalCode',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: any) {
					if (!/^\d{10}$/.test(value)) return false;

					let check = parseInt(value[9]);
					let sum = 0;
					for (let i = 0; i < 9; ++i) {
						sum += parseInt(value[i]) * (10 - i);
					}
					sum %= 11;

					return (
						(sum < 2 && check == sum) ||
						(sum >= 2 && check + sum == 11)
					);
				}
			}
		});
	};
}
