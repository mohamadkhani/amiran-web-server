import { registerDecorator } from 'class-validator';
import { ValidationOptions } from 'class-validator';

export function Username(validationOptions?: ValidationOptions) {
	validationOptions = {
		...validationOptions,
		message: 'Invalid Username'
	};
	return function (object: Object, propertyName: string) {
		registerDecorator({
			name: 'Username',
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value: string) {
					return !!value && /^[a-z0-9_]{4,16}$/.test(value);
				}
			}
		});
	};
}
