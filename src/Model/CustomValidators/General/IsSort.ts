import {
	ValidateBy,
	ValidationArguments,
	ValidationOptions
} from 'class-validator';
import _ from 'lodash';
import { SortMDL } from '../../Entity/ReadMDL';

export function IsSort(validationOptions?: ValidationOptions) {
	return ValidateBy(
		{
			name: 'IsSort',
			constraints: [],
			async: true,
			validator: {
				validate(
					value: any,
					{ object, targetName }: ValidationArguments
				) {
					if (value === null || value === undefined) return true;
					if (!_.isArray(value)) {
						return false;
					}
					//@ts-ignore
					if (
						!!value.length &&
						(!global.sortables ||
							!_.isArray(
								//@ts-ignore
								global.sortables[object.entity.constructor.name]
							))
					) {
						return false;
					}

					const sorts = value.map(
						(filter) => new SortMDL(filter.key, filter.desc)
					);

					return sorts.reduce<boolean>(
						(acm, sortItem: SortMDL) =>
							acm &&
							//@ts-ignore
							!!global.sortables[
								//@ts-ignore
								object.entity.constructor.name
							].includes(sortItem.key) &&
							sortItem.validate().length === 0,
						true
					);
				},
				defaultMessage: (vArgs) => {
					if (vArgs) {
						const { value, object } = vArgs;
						if (!_.isArray(value)) {
							return (
								'sort structure is invalid. Example: ' +
								'{value: string, desc: boolean}[]'
							);
						}
						//@ts-ignore
						if (!!value.length && (!global.sortables ||
							!_.isArray(
								//@ts-ignore
								global.sortables[object.entity.constructor.name]
							))) {
							return 'Invalid request, Please Check the sort keys';
						}
						const sorts = value.map(
							(filter) => new SortMDL(filter.key, filter.desc)
						);
						const res = sorts.reduce<any[]>(
							(acm, sortItem: SortMDL) => [
								...acm,
								...sortItem.validate()
							],
							[]
						);
						if (res.length > 0) {
							return Object.entries(
								res[0].constraints
							)[0].reduce<string>(
								(acm, item, index) =>
									acm + ((index == 1 ? ' => ' : '') + item),
								''
							);
						} else
							return 'Invalid request, Please Check the sort keys';
					} else {
						return (
							'sort structure is invalid. Example: ' +
							'{value: string, desc: boolean}'
						);
					}
				}
			}
		},
		validationOptions
	);

	/*return (object: any, propertyName: string) => {
        registerDecorator({
            name: 'IsFilter',
            target: object.constructor,
            propertyName,
            constraints: [],
            options: {
                message: 'filters structure is not valid',
                ...validationOptions
            },
            validator: {
                async validate(value: any, args: ValidationArguments) {

                    if (value === null || value === undefined) return true;
                    if (!_.isArray(value)) {
                        return false;
                    }

                    const filters = value.map(
                        (f) => new Filter(f.op, f.key, f.value)
                    );

                    const filterObjValidationResult = await filters.reduce(
                        async (acm, filter) =>
                            (await acm) && (await validate(filter)).length == 0,
                        Promise.resolve(true)
                    );

                    return filters.reduce<boolean>(
                        (acm, filtersItem: Filter) =>
                            acm &&
                            //@ts-ignore
                            !!global.filterables[
                                object.constructor.name
                            ].includes(filtersItem.key),
                        true
                    );
                }
            }
        });
    };*/
}
