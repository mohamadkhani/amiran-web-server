export function Updatable() {
	return (object: any, propertyName: string) => {
		//@ts-ignore
		if (!global.updatables) global.updatables = {};
		//@ts-ignore
		if (!global.updatables[object.constructor.name])
			//@ts-ignore
			global.updatables[object.constructor.name] = [];
		//@ts-ignore
		global.updatables[object.constructor.name].push(propertyName);
	};
}
