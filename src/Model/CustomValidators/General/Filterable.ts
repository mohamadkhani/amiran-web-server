import { ArangoOperationsType } from '../ArangoOperations';

// TODO Arango operations should be applied to the fields
export function Filterable(ops: ArangoOperationsType[] = ['==']) {
	return (object: any, propertyName: string) => {
		//@ts-ignore
		if (!global.filterables) global.filterables = {};
		//@ts-ignore
		if (!global.filterables[object.constructor.name])
			//@ts-ignore
			global.filterables[object.constructor.name] = [];
		//@ts-ignore
		global.filterables[object.constructor.name].push(propertyName);
	};
}
