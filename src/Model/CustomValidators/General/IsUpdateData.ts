import {
	ValidateBy,
	validateSync,
	ValidationArguments,
	ValidationOptions
} from 'class-validator';

export function IsUpdateData(validationOptions?: ValidationOptions) {
	return ValidateBy(
		{
			name: 'IsUpdateData',
			async: true,
			validator: {
				validate(
					data: Record<string, any>,
					{ object }: ValidationArguments
				) {
					if (Object.keys(data).length === 0) return false;

					//@ts-ignore
					const updatables = global.updatables[
						//@ts-ignore
						object.entity.constructor.name
					] as string[];

					const isInUpdateData = Object.keys(data).reduce<boolean>(
						(acm, prop) => acm && updatables.includes(prop),
						true
					);
					if (!isInUpdateData) return false;

					//@ts-ignore

					//@ts-ignore
					const obj = new object.entity.constructor();

					//assigning JSON object props to Model instance props
					Object.entries(data).forEach(
						//@ts-ignore
						([key, value]) => (obj[key] = value)
					);

					// let obj = {};
					// updatables.forEach((item) => {
					// 	if (data[item]) obj[item] = data[item];
					// });
					const validateResult = validateSync(obj, {
						groups: ['update']
					});

					return validateResult.length === 0;
				},
				defaultMessage: (vArgs) => {
					if (vArgs) {
						const { value, object } = vArgs;

						if (Object.keys(value).length === 0)
							return 'data must not be empty';

						//@ts-ignore
						const updatables = global.updatables[
							//@ts-ignore
							object.entity.constructor.name
						] as string[];

						const isInUpdateData = Object.keys(
							value
						).reduce<boolean>(
							(acm, prop) => acm && updatables.includes(prop),
							true
						);
						if (!isInUpdateData)
							return 'data must contain allowed keys';

						//@ts-ignore
						const obj = new object.entity.constructor();

						Object.entries(value).forEach(
							//@ts-ignore
							([key, value]) => (obj[key] = value)
						);
						// let obj = {};
						// updatables.forEach((item) => {
						// 	if (value[item]) obj[item] = value[item];
						// });

						if (
							validateSync(obj, { groups: ['update'] }).length !==
							0
						) {
							return 'update data keys are invalid';
						} else {
							return '';
						}
					} else {
						return (
							'update structure is invalid. Example: ' +
							'{op: operation, value, key}'
						);
					}
				}
			}
		},
		validationOptions
	);
}
