import {
	ValidateBy,
	validateSync,
	ValidationArguments,
	ValidationOptions
} from 'class-validator';
import _ from 'lodash';
import { FilterMDL } from '../../Entity/ReadMDL';

export function IsFilter(validationOptions?: ValidationOptions) {
	return ValidateBy(
		{
			name: 'IsFilter',
			constraints: [],
			async: true,
			validator: {
				validate(
					value: any,
					{ object, targetName }: ValidationArguments
				) {
					if (value === null || value === undefined) return true;
					if (!_.isArray(value)) {
						return false;
					}
					if (
						!!value.length &&
						(!global.filterables ||
							!_.isArray(
								global.filterables[
									//@ts-ignore
									object.entity.constructor.name
								]
							))
					) {
						return false;
					}

					const filters = value.map(
						(filter) =>
							//@ts-ignore
							new FilterMDL(filter.op, filter.key, filter.value)
					);

					return filters.reduce<boolean>(
						//@ts-ignore
						(acm, filtersItem: FilterMDL) =>
							acm &&
							//@ts-ignore
							!!global.filterables[
								//@ts-ignore
								object.entity.constructor.name
							].includes(filtersItem.key) &&
							filtersItem.validate().length === 0,
						true
					);
				},
				defaultMessage: (vArgs) => {
					if (vArgs) {
						const { value, object } = vArgs;
						if (!_.isArray(value)) {
							return (
								'filter structure is invalid. Example: ' +
								'[{op: operation, value, key}]'
							);
						}
						//@ts-ignore
						if (
							!!value.length &&
							(!global.filterables ||
								!_.isArray(
									global.filterables[
										//@ts-ignore
										object.entity.constructor.name
									]
								))
						) {
							return 'Invalid request, Please Check the filter keys';
						}
						const filters = value.map(
							(filter) =>
								new FilterMDL(
									filter.op,
									//@ts-ignore
									filter.key,
									filter.value
								)
						);
						const res = filters.reduce<any[]>(
							//@ts-ignore
							(acm, filtersItem: FilterMDL) => [
								...acm,
								...filtersItem.validate()
							],
							[]
						);
						if (res.length > 0) {
							return Object.entries(
								res[0].constraints
							)[0].reduce<string>(
								(acm, item, index) =>
									acm + ((index == 1 ? ' => ' : '') + item),
								''
							);
						} else
							return 'Invalid request, Please Check the filter keys';
					} else {
						return (
							'filter structure is invalid. Example: ' +
							'{op: operation, value, key}'
						);
					}
				}
			}
		},
		validationOptions
	);

	/*return (object: any, propertyName: string) => {
		registerDecorator({
			name: 'IsFilter',
			target: object.constructor,
			propertyName,
			constraints: [],
			options: {
				message: 'filters structure is not valid',
				...validationOptions
			},
			validator: {
				async validate(value: any, args: ValidationArguments) {

					if (value === null || value === undefined) return true;
					if (!_.isArray(value)) {
						return false;
					}

					const filters = value.map(
						(f) => new Filter(f.op, f.key, f.value)
					);

					const filterObjValidationResult = await filters.reduce(
						async (acm, filter) =>
							(await acm) && (await validate(filter)).length == 0,
						Promise.resolve(true)
					);

					return filters.reduce<boolean>(
						(acm, filtersItem: Filter) =>
							acm &&
							//@ts-ignore
							!!global.filterables[
								object.constructor.name
							].includes(filtersItem.key),
						true
					);
				}
			}
		});
	};*/
}
