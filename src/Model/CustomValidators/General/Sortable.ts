// TODO Arango operations should be applied to the fields
export function Sortable() {
	return (object: any, propertyName: string) => {
		//@ts-ignore
		if (!global.sortables) global.sortables = {};
		//@ts-ignore
		if (!global.sortables[object.constructor.name])
			//@ts-ignore
			global.sortables[object.constructor.name] = [];
		//@ts-ignore
		global.sortables[object.constructor.name].push(propertyName);
	};
}
