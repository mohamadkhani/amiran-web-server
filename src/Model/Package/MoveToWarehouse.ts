import {
	IsArray,
	IsBoolean,
	IsDefined,
	IsOptional,
	IsString,
	Length
} from 'class-validator';
import Model from '../../Base/Model';

export default class MoveToWarehouse extends Model {
	@IsDefined()
	@IsArray()
	packages: string[] = [];

	@IsOptional()
	@IsBoolean()
	force: boolean = false;

	@IsOptional()
	@IsString()
	description: string = '';
}
