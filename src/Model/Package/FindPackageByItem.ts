import {
	IsArray,
	IsBoolean,
	IsDefined,
	IsOptional,
	IsString,
	Length
} from 'class-validator';
import Model from '../../Base/Model';

export default class FindPackageByItem extends Model {
	@IsDefined()
	@IsString()
	_key: string = null;

	@IsDefined()
	@IsString()
	shiftCode: string = null;
}
