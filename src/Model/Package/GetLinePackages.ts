import { IsDefined, IsNumber, IsOptional, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class GetPackageWithItems extends Model {
	@IsDefined()
	@IsString()
	shiftCode: string = null;

	@IsOptional()
	@IsNumber()
	page: number = 0;

	@IsOptional()
	@IsNumber()
	PageSize: number = 0;
}
