import { IsDefined, IsNumber, IsOptional, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class PMPackageListMDL extends Model {
	@IsDefined()
	@IsString()
	shiftKey: string = null;

	@IsOptional()
	@IsString()
	status: string = null;

	@IsOptional()
	@IsNumber()
	page: number = 0;

	@IsOptional()
	@IsNumber()
	PageSize: number = 0;
}
