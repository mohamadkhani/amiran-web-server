import { IsString } from "class-validator";
import Model from "../../Base/Model";

export default class RemoveRegListener extends Model { 
    @IsString()
    id!: string;
}