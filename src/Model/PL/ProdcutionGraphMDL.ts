import { IsArray, IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class ProductionGraphMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;
	
	@IsDefined()
	@IsArray()
	public intervals: {start: number, end: number}[] = [];
}
