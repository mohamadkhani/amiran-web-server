import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class EstimateMDL extends Model {
	@IsDefined()
	@IsString()
	public shiftKey: string = null;
	@IsDefined()
	@IsNumber()
	public period: number = null;
}
