import { IsDefined, IsNumber } from 'class-validator';
import Model from '../Base/Model';

export default class IntervalMDL extends Model {
	@IsDefined()
	@IsNumber()
	start: number = null;
	@IsDefined()
	@IsNumber()
	end: number = null;
}
