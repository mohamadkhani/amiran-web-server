import { IsDefined, IsNumber } from 'class-validator';
import Model from '../../Base/Model';

export default class InactiveForMDL extends Model {
	@IsDefined()
	@IsNumber()
	days: string = null;
}
