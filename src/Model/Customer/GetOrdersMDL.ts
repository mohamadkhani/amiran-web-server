import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class GetOrdersMDL extends Model {
	@IsDefined()
	@IsString()
	_key: string = null;

	@IsDefined()
	@IsNumber()
	page: number = 1;

	@IsDefined()
	@IsNumber()
	pageSize: number = 10;
}
