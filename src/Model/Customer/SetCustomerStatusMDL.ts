import { IsBoolean, IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class SetCustomerStatusMDL extends Model {
	@IsDefined()
	@IsString()
	_key: string = null;

	@IsDefined()
	@IsBoolean()
	active: boolean = false;
}
