import { IsDefined, IsString } from 'class-validator';
import Model from '../../Base/Model';

export class RemoveMDL extends Model {
	@IsDefined()
	@IsString()
	_key?: string;
}
