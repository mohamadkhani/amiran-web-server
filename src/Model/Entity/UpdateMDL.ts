import { IsDefined, IsNotEmpty, IsObject, IsString } from 'class-validator';
import Model from '../../Base/Model';
import { IsUpdateData } from '../CustomValidators/General/IsUpdateData';

export default class UpdateMDL<Ntity> extends Model {
	constructor(public entity: string) {
		super();
	}

	@IsDefined()
	@IsString()
	key?: string;

	@IsDefined()
	@IsObject()
	@IsUpdateData()
	data?: Record<keyof Ntity, any>;
}
