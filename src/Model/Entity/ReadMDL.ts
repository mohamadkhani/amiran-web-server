import {
	IsArray,
	IsBoolean,
	IsDefined,
	IsNumber,
	IsObject,
	IsOptional,
	IsString,
	validateSync
} from 'class-validator';
import {
	ArangoOperations,
	ArangoOperationsType
} from '../CustomValidators/ArangoOperations';
import Model from '../../Base/Model';
import { IsFilter } from '../CustomValidators/General/IsFilter';
import { IsSort } from '../CustomValidators/General/IsSort';

type FilterValue = string | number;

export class SortMDL {
	constructor(key: string, desc: boolean) {
		this.desc = desc;
		this.key = key;
		this.validate();
	}

	@IsBoolean()
	@IsOptional()
	desc: boolean;

	@IsDefined()
	@IsString()
	key: string;

	validate() {
		return validateSync(this);
	}
}

export class FilterMDL<Ntity, PropType> {
	constructor(
		operation: ArangoOperationsType,
		key: keyof Ntity,
		value: PropType
	) {
		this.op = operation;
		this.value = value;
		this.key = key;
		this.validate();
	}

	@IsDefined()
	@IsString()
	@ArangoOperations()
	op: ArangoOperationsType;

	@IsDefined()
	value: PropType;

	@IsDefined()
	@IsString()
	key: keyof Ntity;

	validate() {
		return validateSync(this);
	}
}

export default class ReadMDL<Ntity> extends Model {
	// this modelName is used in IsFilter
	constructor(public entity: string) {
		super();
	}

	@IsDefined()
	@IsArray()
	@IsFilter()
	filter?: { key: keyof Ntity; op: ArangoOperationsType; value: any }[] = [];

	@IsOptional()
	@IsArray()
	@IsSort()
	sort?: { key: keyof Ntity; desc: boolean }[] = [];

	@IsOptional()
	@IsNumber()
	pageSize?: number = 1;

	@IsOptional()
	@IsNumber()
	page?: number = 1;
}
