import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class UpdatePriceMDL extends Model {
	@IsDefined()
	@IsString()
	_key: string;

	@IsDefined()
	@IsNumber()
	price: number = 0;
}
