import { IsDefined, IsNumber, IsString } from 'class-validator';
import Model from '../../Base/Model';

export default class GetMonthlyStatMDL extends Model {
	@IsDefined()
	@IsString()
	rgx: string = null;

	@IsDefined()
	@IsNumber()
	year: number = 0;
}
