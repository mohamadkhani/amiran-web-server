import { IsDefined, IsNumber } from 'class-validator';
import Model from '../../Base/Model';

export default class CriticalInfoMDL extends Model {
	@IsDefined()
	@IsNumber()
	guaranteeDate: number = null;

	@IsDefined()
	@IsNumber()
	days: number = null;
}
