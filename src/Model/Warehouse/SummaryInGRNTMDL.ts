import { IsDefined, IsNumber } from 'class-validator';
import Model from '../../Base/Model';

export default class SummaryInGRNTMDL extends Model {
	@IsDefined()
	@IsNumber()
	guaranteeDate: number = null;
}
