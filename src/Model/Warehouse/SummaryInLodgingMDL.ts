import { IsDefined, IsNumber } from 'class-validator';
import Model from '../../Base/Model';

export default class SummaryInLodgingMDL extends Model {
	@IsDefined()
	@IsNumber()
	days: number = null;
}
